% compare with hotelling T^2

clear
close all


mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

%% Hotelling T

M=200; % M is the size of sliding window

rep=5000; 



%%  offline control chart


output=[];



  for t=1:rep % total number of runs    
    
      Y=normrnd(mu_0,sigma_1,[20,M]); % Y is testing data
      [m,n]=size(Y);
       
      
      %% initialize      
      % sample data
        
       %index=1;
       % compute statistic
       T_2=zeros(1,n);
   for index=1:n-1
    
       data_pre=Y(:, 1:index);
       data_post=Y(:,index+1:n);
       
       mean_pre=mean(data_pre,2);
       mean_post=mean(data_post,2);
       
       
       
       temp_pre=bsxfun(@minus, data_pre, mean_pre);
       est_cov_pre=sum(bsxfun(@times, reshape(temp_pre,m,1,index), reshape(temp_pre, 1,m,index)),3);
       
       temp_post=bsxfun(@minus, data_post, mean_post);
       est_cov_post=sum(bsxfun(@times, reshape(temp_post,m,1,n-index), reshape(temp_post, 1,m,n-index)),3);
       
       est_cov=( est_cov_pre+est_cov_post)/(n-2);
       
       
       S_inv=inv(est_cov);
       
       temp=(mean_pre-mean_post)'*S_inv*(mean_pre-mean_post);

       T_2(index)=(index*(n-index)/n)*temp;
 
    end         

%fprintf(1, '--experiment no %d of %d\n', t, rep);
stat(t)={T_2};
end


         
 