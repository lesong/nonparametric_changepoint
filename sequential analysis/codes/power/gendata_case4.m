% offline 
% generate 100 datasets d=20,50,100
%% case 4: normal --> slope change
% d=20

clear
close all

N=5; 
M=200;
d=100;

 % correspond to the index of the slope change


mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point
slope=0.02;

for i=1:100
%index=rand(d,floor(M/2));
%index= index>ratio; % 
%index=repmat(index,d,1);
C=ceil(20*rand(1,2));
testing=zeros(d,M);
%rng(1);
testing(:,1:floor(M/2))=normrnd(mu_0,sigma_1,[d,floor(M/2)]);
% rng(2);
%testing(:,floor(M/2)+1: M)=(1-index).*randlap(d,floor(M/2))+index.*normrnd(mu_0,sigma_1,[d,floor(M/2)]);
testing(:,floor(M/2)+1: M)=normrnd(mu_0,sigma_1,[d,floor(M/2)]);
testing(C(1),floor(M/2)+1: M)=slope*(1: M-floor(M/2));
testing(C(2),floor(M/2)+1: M)=slope*(1: M-floor(M/2));
%testing(:,floor(M/2)+1: M)=randlap(d,floor(M/2));
data_test(i)={testing};
end
%% we use the same reference data
reference=zeros(d,N*M);
%rng(3);
reference=normrnd(mu_0,sigma_1,[d,N*M]);
%figure
%plot(testing(1,:),'o')
%ylim([-10,10])
%alldata=[reference,testing];


