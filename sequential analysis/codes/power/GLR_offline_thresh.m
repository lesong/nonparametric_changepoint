%% GLR offline line

% for gaussian d=20

% compare with hotelling T^2

clear
close all


mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

%% GLR

M=200; % M

rep=5000; % find the threshold from 5000 samples



%% offline


output=[];



  for t=1:rep % total number of runs    
    
      Y=normrnd(mu_0,sigma_1,[20,M]); % Y is testing data
      [m,n]=size(Y);
       
      %flag=1; % once cross the threshold, set the flag=0
       
      %% initialize      
      % sample data
        
      % index=M-1;
       % compute statistic
       index=0;
       GLR=[];
       
       mean_t=mean(Y,2);
       temp_t=bsxfun(@minus, Y, mean_t);
       est_cov_t=mean(bsxfun(@times, reshape(Y,m,1,n), reshape(Y, 1,m,n)),3);
       temp1=n*log(det(est_cov_t));
   while  (index < n-1) 
       index=index+1;
       
       data_pre=Y(:, 1:index);
       data_post=Y(:,index+1:n);
       
       mean_pre=mean(data_pre,2);
       mean_post=mean(data_post,2);
       
       
       
       temp_pre=bsxfun(@minus, data_pre, mean_pre);
       est_cov_pre=mean(bsxfun(@times, reshape(temp_pre,m,1,index), reshape(temp_pre, 1,m,index)),3);
       
       temp_post=bsxfun(@minus, data_post, mean_post);
       est_cov_post=mean(bsxfun(@times, reshape(temp_post,m,1,n-index), reshape(temp_post, 1,m,n-index)),3);
       
       
       
       GLR(index)=temp1-index*log(det(est_cov_pre))-(n-index)*log(det(est_cov_post));
       
       
 
   end         

    output(t)=max(GLR);
   
%fprintf(1, '--experiment no %d of %d\n', t, rep);
end


hist(output,100)
         
 