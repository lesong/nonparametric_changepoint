clear
close all
 
B=5;
d=10;

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point
mu_1=0.5;
sigma_2=1;


%% generate data
L = 1000;



% estimate bandwith
X_s =normrnd(mu_0,sigma_1,[d,5000]);
[ms,ns]=size(X_s);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,X_s(:,(i+1):ns),X_s(:,i));
    Ds(i,(i+1):ns)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw = median(Ds(Ds~=0));

% estimate variance
sigma_2_sq=hyy(X_s,bandw);
sigma_4_sq=hxxyy(X_s,bandw);

N = 20;
B = 10;

for i = 1: 100
    
reference=normrnd(mu_0,sigma_1,[d,N*B]);

C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
S_var = 2/B/(B-1)*C;
     
testing=zeros(d,L);
testing(:,1:floor(L/2))=normrnd(mu_0,sigma_1,[d,floor(L/2)]);
testing(:,floor(L/2)+1: L)=normrnd(mu_1,sigma_2,[d,floor(L/2)]);
M_stat(i,:) = online_compute_stat (reference, testing, 1000, bandw, S_var, B, N);
end

% plot 
mean_stat = mean(M_stat,1);
std_stat = std(M_stat,0, 1);
x = 1: L;

errorbar(x,mean_stat,std_stat)


