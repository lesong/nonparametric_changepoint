% compare with hotelling T^2

clear
close all

% case 5

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

%% Hotelling T compute threshold

M = 20; % M is the size of sliding window
L = 300;
d =1;
rep = 5000; 
ARL = 5000;

%%  offline control chart
output=[];

for t=1:rep % total number of runs    
    
      Y = normrnd(mu_0,sigma_1,[d,L]); % Y is testing data
      [m,n] = size(Y);
       
      
      %% initialize      
      % sample data
       T2 = [];
   for index=M:L
       
       meanEst = mean(Y(:, index-M+1: index), 2);
       T2 = [T2; M* (meanEst-0)' * (meanEst-0)];
                       
       
   end         

    output = [output; max(T2)];
end

threshold = quantile(output, (1-L/ARL));

% detect
load data_test_case5.mat


for i = 1: 500
    Y = data_test{i};
    
    
    EDD = [];
   for index=M:200
       
       meanEst = mean(Y(:, index-M+1: index), 2);
       T2 = M* (meanEst-0)' * (meanEst-0);
       if T2 >= threshold
           EDD = [EDD; index];
           break;
       end
   end  
   index
end

size(EDD)

mean(EDD)



         