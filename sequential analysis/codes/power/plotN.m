% plot
L = 1000;
subplot(1,2,1)
load M_stat_N1
mean_stat = mean(M_stat,1);
max_stat = max(M_stat, [],1);
min_stat = min(M_stat, [],1);
err_stat = max_stat - min_stat;
x = 1: L;
errorbar(x,mean_stat,err_stat);

subplot(1,2,2)
load M_stat_N10
mean_stat = mean(M_stat,1);
max_stat = max(M_stat, [],1);
min_stat = min(M_stat, [],1);
err_stat = max_stat - min_stat;
x = 1: L;
errorbar(x,mean_stat,err_stat);

figure 
subplot(1,2,1)
load M_stat_N1
mean_stat = mean(M_stat,1);
std_stat = std(M_stat,0, 1);
x = 1: L;
errorbar(x,mean_stat,std_stat);
ylim([-10, 30]);
xlabel('time');
ylabel('scan B-statistic');
title('N=1');


subplot(1,2,2)
load M_stat_N10
mean_stat = mean(M_stat,1);
std_stat = std(M_stat,0, 1);
x = 1: L;
errorbar(x,mean_stat,std_stat);
ylim([-10, 30]);
xlabel('time');
ylabel('scan B-statistic');
title('N=10');
