
clear
clc

% load data
load sequence_1.mat

L = length(sequence);
mu = mean(sequence);
sigma = std(sequence);
sequence = (sequence - mu) ./ sigma;
%sequence = sequence'./100;
seqCut = sequence(1:1500);
reference =datasample(seqCut, 20000);
%reference = datasample(seqCut, 5000);

N = 5;
M = 150;
r = 0.1;
bandw = r * bandw1(reference);
S_var = est_var (reference, bandw, M, N);

outcome = [];

% reference = mvnrnd(mu, sigma, N*M)';
% test = mvnrnd(mu, sigma, 1*M)';

    

%% Estimate bandwidth from reference data

%  r = 1 by default (median trick);
%  You can also try other bandwidth by tuning r in comparision with median trick;

%  bandw1(.) is a function to estimate bandwidth from reference data using median trick



%% Estimate Variance of the statistic (Lemma 1 in our paper)

%  est_var(.) is a function to estimate variacen of statistic under null (using lemma 1)



for k = 1:2000
    
% reference = mvnrnd(mu, sigma, N*M)';
% test = mvnrnd(mu, sigma, 1*M)';


reference = datasample(seqCut, N*M);
test = datasample(seqCut, 1*M);
    

%% Estimate bandwidth from reference data

%  r = 1 by default (median trick);
%  You can also try other bandwidth by tuning r in comparision with median trick;

%  bandw1(.) is a function to estimate bandwidth from reference data using median trick



%% Compute kernel matrix  

%  fKxx(data1, data2, N, M, bandw, flag) is a function to oompute kernel matrix


Kxx_post = fKxx(test, test, N, M, bandw, 1);  
Kxx_pre = fKxx(reference, reference, N, M, bandw, 2);
Kxx_cross = fKxx(reference, test ,N, M, bandw, 3) ; 


%% Compute statistic

% M_stat is your computed statistic
 
M_stat = zeros(1,M);
 
 for B = 2:M  % scan your testing data by tuning the block size
   
         MMD = [];
         
         T = Kxx_post(M-B+1:M, M-B+1:M);
         
         
         for j = 1:N % compute MMD for all blocks and take an average to get the statistic 
           
             A = Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C = Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             
             MMD(j) = 1/B/(B-1)*sum(A(:))+ 1/B/(B-1)*sum(T(:)) - 2/B/(B-1)*sum(C(:));
             
         end 
   
         M_stat(M-B+1) = mean(MMD) ./ sqrt(S_var(B-1)) ;
        
 end

 outcome = [outcome; M_stat(1:M-1)];
 outcomeMax(k) = max(M_stat(1:M-1));
 fprintf('--itermation %d --Max is % f\n ', k,outcomeMax(k))

end

threshold1 = quantile(outcomeMax, 0.90)

threshold2 = quantile(outcomeMax, 0.95)

threshold3 = quantile(outcomeMax, 0.99)






