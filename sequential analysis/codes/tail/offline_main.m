
clear
clc

% generate 

N = 5;
M = 100;

d = 20;
mu = zeros(1,d);
sigma = eye(d);
outcome = [];


for K =1:5
reference = mvnrnd(mu, sigma, N*M)';
test = mvnrnd(mu, sigma, 1*M)';
 
    

%% Estimate bandwidth from reference data

%  r = 1 by default (median trick);
%  You can also try other bandwidth by tuning r in comparision with median trick;

%  bandw1(.) is a function to estimate bandwidth from reference data using median trick

r = 1;
bandw = r * bandw1(reference);

%% Estimate Variance of the statistic (Lemma 1 in our paper)

%  est_var(.) is a function to estimate variacen of statistic under null (using lemma 1)

S_var = est_var (reference, bandw, M, N);

for k = 1:1000
reference = mvnrnd(mu, sigma, N*M)';
test = mvnrnd(mu, sigma, 1*M)';
 
    

%% Estimate bandwidth from reference data

%  r = 1 by default (median trick);
%  You can also try other bandwidth by tuning r in comparision with median trick;

%  bandw1(.) is a function to estimate bandwidth from reference data using median trick



%% Compute kernel matrix  

%  fKxx(data1, data2, N, M, bandw, flag) is a function to oompute kernel matrix


Kxx_post = fKxx(test, test, N, M, bandw, 1);  
Kxx_pre = fKxx(reference, reference, N, M, bandw, 2);
Kxx_cross = fKxx(reference, test ,N, M, bandw, 3) ; 


%% Compute statistic

% M_stat is your computed statistic
 
M_stat = zeros(1,M);
 
 for B = 2:M  % scan your testing data by tuning the block size
   
         MMD = [];
         
         T = Kxx_post(M-B+1:M, M-B+1:M);
         
         
         for j = 1:N % compute MMD for all blocks and take an average to get the statistic 
           
             A = Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C = Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             
             MMD(j) = 1/B/(B-1)*sum(A(:))+ 1/B/(B-1)*sum(T(:)) - 2/B/(B-1)*sum(C(:));
             
         end 
   
         M_stat(M-B+1) = mean(MMD) ./ sqrt(S_var(B-1)) ;
        
 end

 outcome = [outcome; M_stat];
 outcomeMax(k) = max(M_stat);
 fprintf('--itermation %d --Max is % f\n ', k,outcomeMax(k))
end


threshold1(K) = quantile(outcomeMax, 0.90)

threshold2(K) = quantile(outcomeMax, 0.95)

threshold3(K) = quantile(outcomeMax, 0.99)

end

mean(threshold1)
mean(threshold2)
mean(threshold3)


