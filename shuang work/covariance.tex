\documentclass[11 pt]{article}
\usepackage[top=0.8 in, bottom=0.8  in, right=0.8in, left=0.8 in]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsfonts}
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{amssymb, amsmath}% Use packages about maths
\usepackage{amsthm}
\usepackage[english]{babel}
\usepackage{enumerate,bbm}
\usepackage{geometry}
%\usepackage{enumerate}
%\newtheorem{theorem}{Claim}
%\newenvironment{proof}{\noindent {\sc Proof:}}{$\Box$ \medskip}
%\renewcommand{\labelenumi}{(\alph{enumi})}
%\newcommand{\prob}[1]{\mathbf{Pr}(#1)}
%\newcommand{\lcm}{\mbox{lcm}}
%\newcommand{\reals}{\mathbb{R}}
%\newcommand{\tih}{\tilde{H}}
%\newcommand{\ex}{\langle\mbox{EXPR}\rangle}
%\newcommand{\ter}{\langle\mbox{TERM}\rangle}
%\newcommand{\fac}{\langle\mbox{FACTOR}\rangle}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\usepackage{hyperref}
\usepackage{color}
\usepackage{natbib}
\usepackage{amsmath}
\usepackage{indentfirst}
\renewcommand{\baselinestretch}{1.3}
\usepackage{float}
\begin{document}
\section{Background}
Assume observations 
\begin{align*}
& X:=\{x_1, x_2, \dots, x_m\} \stackrel {iid} {\sim} P,\\
& Y:=\{y_1, y_2, \dots, y_m\} \stackrel{iid}{\sim} Q.
\end{align*}
Let $\mathcal{F}$ be a class of functions $f: \mathcal{X} \to \mathbb{R}$ and define the \textit{maximum mean discrepancy} (MMD) as 
\begin{align}
\mbox{MMD} [\mathcal{F}, P, Q] := \sup_{f\in \mathcal{F}} \left( \mathbb{E}_x [f(x)] -\mathbb{E}_y [f(y)]\right).
\end{align}
In the reproducing kernel Hilbert space $\mathcal{H}$, the $\mbox{MMD} $ can be expressed as the distance between mean embeddings in $\mathcal{H}$, and furthermore can be expressed in terms of kernel functions. That is
\begin{align*}
\mbox{MMD}^2 [\mathcal{F}, P, Q ] & =\left[  \sup_{\| f \|_{\mathcal{H} \leqslant 1}     }     ( \mathbb{E}_x [f(x)] -\mathbb{E}_y [f(y)]    )    \right]^2 \\
& = \left[  \sup_{\|f\|_{\mathcal{H} \leqslant 1}}    \langle \mu_P -\mu_Q, f \rangle_{\mathcal{H}}  \right]^2 \\
&=\| \mu_P -\mu_Q  \|^2_{\mathcal{H}} \\
&=\langle \mu_P,\mu_P  \rangle_{\mathcal{H}} + \langle \mu_Q,\mu_Q  \rangle_{\mathcal{H}}  -2 \langle \mu_P,\mu_Q \rangle_{\mathcal{H}}  \\
&= \mathbb{E}_{x,x'} \langle  \phi(x), \phi(x') \rangle_{\mathcal{H}} +\mathbb{E}_{y,y'} \langle  \phi(y), \phi(y') \rangle_{\mathcal{H}} -2 \mathbb{E}_{x,y} \langle  \phi(x), \phi(y) \rangle_{\mathcal{H}} \\
&= \mathbb{E}_{x,x'} \kappa(x,x')+ \mathbb{E}_{y,y'} \kappa(y,y')-2 \mathbb{E}_{x,y} \kappa(x,y)
\end{align*}
Next, let $\mbox{MMD}_u^2$ be the unbiased estimate of $\mbox{MMD}^2 [\mathcal{F}, P, Q ]$ by replacing the population expectations with their corresponding U-statistics and sample averages:
\begin{align}
\mbox{MMD}_u^2 =\frac{1}{m(m-1)} \sum_{i,j=1, i \neq j}^m h(z_i,z_j),
\end{align}
where $z^i :=(x_i,y_i)$ and $h(z_i,z_j)=\kappa(x_i,x_j)+\kappa(y_i,y_j)-\kappa(x_i,y_j)-\kappa (x_j,y_i).$\\

Intuitively, the empirical test statistic $\mbox{MMD}_u^2$ is expected to be small (close to zero) if $P=Q$, and large if $P$ and $Q$ are far apart. And it will cost $O(m^2)$ time to compute the statistic, given both sample sizes of $\{x_i\}$ and $\{y_i\}$ are $m$.\\

Under $H_0 \, (P=Q)$, the U-statistic is degenerate, which means $\mathbb{E}_{z'} h(z,z')=0$. In this case, $\mbox{MMD}_u^2$ converges in distribution according to 
\begin{align}
m\mbox{MMD}^2_u \stackrel{D} \to \sum_{l=1}^{\infty} \lambda_{l} [z_l^2 -2],
\end{align}
where $z_l \sim N(0,2)$ and $\lambda_i$ are the solutions to the eigenvalue equation
\begin{align}
\int_{\mathcal{X}} \hat{\kappa} (x,x') \phi_i (x) d \mbox{Pr} (x)= \lambda_i \phi_i (x'),
\end{align}
and $\hat{\kappa} (x_i, x_j) := \kappa (x_i, x_j) -\mathbb{E}_x \kappa(x_i, x)-\mathbb{E}_x \kappa (x,x_j) +\mathbb{E}_{x,x'} \kappa (x,x')$ is the centered RKHS kernel.\\

The main idea of B-test is to split the data into blocks of size B, compute the quadratic-time $\mbox{MMD}_u^2$ on each block, and then average the resulting statistics.
\section{B test}
Given observations $D_t=\{x_1,x_2,\dots, x_t\}$, which will be split into $(n+1)$ blocks, each with size $B$, we compute the B-test statistic:
\begin{align*}
s_t &=\max_{B \in \{2,3,\dots, m\}} \left\{\frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_i(B)\right\}\\
&=\max_{B\in \{2,3,\dots, m\}} \left\{ \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2 (X_i,Y; B) \right \} \\
&=\max_{B\in \{2,3,\dots,m\}} \left \{ \frac{1}{n} \sum_{i=1}^n \frac{1}{B(B-1)} \sum_{j,l=1, j \neq l}^B h( x_i^j, x_i^l, y^j, y^l)  \right\} \\
&= \max_{B \in \{2,3,\dots, m\} } \left \{ \frac{1}{n} \sum_{i=1}^n \frac{1}{B(B-1)} \sum_{j,l=1, j \neq l}^B 
\kappa(x_i^j, x_i^l) +\kappa(y^j,y^l)-\kappa(x_i^j,y^l)-\kappa (x_i^l,y^j)
  \right\} \\
\end{align*}
\begin{figure}[H]
        \centering
                \includegraphics[trim = 5mm 50mm 5mm 18mm, clip, width=\textwidth]{methodgraph}
                \caption{Model}
 \end{figure}
 
 \newpage
 \subsection{The Asymptotic Distribution}
 Recall the $\mbox{MMD}_i^2(B)$ is 
 \begin{align}
 \mbox{MMD}_i^2(B) &= {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  ),
 \end{align}
 where $h(x_i^l, x_i^j, y^l, y^j  ) =\kappa(x_i^l,x_i^j)+\kappa(y^l,y^j)-\kappa(x_i^l,y^j)-\kappa(x_i^j,y^l)$.

 
 In preparation for the next step analysis, we compute the following variance and covariance:
 \begin{itemize}
 \item $\mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right), \,\, i=1,2,\dots, n$;
 \item $\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right), \,\, s=(1-i), (2-i), \dots, (n-i),   s \neq 0  $;
  \item $\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right), \,\, s=(1-i), (2-i), \dots, (n-i),   s \neq 0, \,\, k=2-B, 3-B, \dots, m-B, k \neq 0$.
 \end{itemize}
 
\subsubsection{$  \mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right) $} 
For $i=1,2,\dots, n$,
\begin{align}
& \mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right) \\
 =& \mbox{Var} \left(     {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  )  \right)  \\
 =&    {\binom{B}{2} }^{-2}  \left[  \binom{B}{2} \binom{2}{1} \binom{B-2}{2-1} \sigma_{xy}^2  +                             \binom{B}{2} \binom{2}{2} \binom{B-2}{2-2} \sigma_{xx'yy'}^2  \right],\,\,\mbox{where}
\end{align}
\begin{align}
 \sigma_{xy}^2= \mbox{Var} \left(     \mathbb{E}_{xy} [h(x,x',y,y')]  \right)=\mbox{Var}(0)=0 ,\\
 \sigma_{xx'yy'}^2 =\mbox{Var}  \left(  h(x,x',y,y')   \right)=\mathbb{E} [h^2(x,x',y,y')].
\end{align}
Thus, 
\begin{align}
\mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right)=\frac{2}{B(B-1)} \mathbb{E} [h^2(x,x',y,y')].
\end{align}


\subsubsection{$    \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)  $}
For $i=1,2,\dots, n$, and $s=(1-i), (2-i), \dots, (n-i),   s \neq 0$,
\begin{align}
   & \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right) \\
= & \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B}{2} }^{-1}  \sum_{p < q}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
=&  {\binom{B}{2} }^{-2} \left[      \binom{B}{2} \binom{2}{1} \binom{B-2}{2-1} \sigma_{y}^2  +                             \binom{B}{2} \binom{2}{2} \binom{B-2}{2-2} \sigma_{yy'}^2                   \right],\,\, \mbox{where}
\end{align}
\begin{align}
 \sigma_{y}^2 =\mbox{Var} \left(   \mathbb{E}_{y} [h(x,x',y,y')]   \right)=\mathbb{E} [  \mathbb{E}^2_{y} [h(x,x',y,y') ] \\
 \sigma_{yy'}^2=\mbox{Var} \left(   \mathbb{E}_{yy'} [h(x,x',y,y')]   \right)=\mathbb{E}[  \mathbb{E}^2_{yy'} [h(x,x',y,y') ]
\end{align}
Thus, 
\begin{align}
\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)
=\frac{4 (B-2)}{B(B-1)} \mathbb{E} [  \mathbb{E}^2_{y} [h(x,x',y,y') ] + \frac{2}{B(B-1)} \mathbb{E}[  \mathbb{E}^2_{yy'} [h(x,x',y,y') ]
\end{align}

\subsubsection{$   \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right)     $}
For $i=1,2,\dots, n$, $s=(1-i), (2-i), \dots, (n-i),   s \neq 0$, and $ k=2-B, 3-B, \dots, m-B, k \neq 0$, our objective is to compute
\begin{align}
 \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right)  
\end{align}
 
 Given the observed data $\{d\}$, let's first define the following sets:
 \begin{align}
 & A := \{d| d \in X_i (B) \mbox{ and } d \in X_{i+s} (B+k) \} \\
 & T := \{ d| d \in X_i(B)  \mbox{ and } d \in Y(B+k)            \} \\
 & C := \{ d| d \in Y(B) \mbox{ and } d \in X_{i+s} (B+k)          \} \\
 & D :=\{  d| d \in Y(B) \mbox{ and } d \in Y(B+k )\}
 \end{align}
 Correspondingly, the cardinality of the above sets are denoted as: $|A|, |T|, |C|, |D|$.\\
 
 Given $k > 0$, we will definitely have $C=\emptyset$ and $|C|=0$. Similarly, given $k <0$, we will definitely have $B =\emptyset$ and $|T|=0$.\\
 
 Without loss of generality, we let $k>0$. Then
 \begin{align}
  &  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right)  \\
  =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B+k}{2} }^{-1}  \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}   \underbrace{   \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right) , }_{\mbox{I}} \mbox{ where} 
 \end{align}
\begin{align}
 \mbox{I} : & = R_1\sigma^2_0 + R_2 \sigma_{1(x,x)}^2+R_3 \sigma_{1(y,y)}^2 +R_4 \sigma^2 _{1(x,y)} +R_5 \sigma^2_{2(xx,xx)} +R_6 \sigma^2_{2(yy,yy)}+
 R_7 \sigma^2_{2(xx,yy)}  \dots   \nonumber \\
 & + R_8 \sigma^2_{2(xx,xy)}  
 + R_9 \sigma^2_{2(xy,xy)} 
  +R_{10} \sigma^2_{2(xy,yy)}  
+R_{11} \sigma^2_{3(xxy,xxy)} \dots \nonumber \\
 & +R_{12}\sigma^2_{3(xyy,xyy)}+R_{13} \sigma^2_{3(xxy,xyy)} +R_{14} \sigma^2_{4(xxyy,xxyy)}
 \end{align}


Note that given $k>0$, in the most general case: 
\begin{itemize}
\item for any elements $x_i \in X_i$, we have:
\begin{align}
x_i \in \{A\} \mbox{ or } x_i \in \{T\} \mbox{ or } x_i \in X_i -\{A\}-\{T\};
\end{align}
\item for any elements $y \in Y(B)$, we have:
\begin{align}
y \in \{D\};
\end{align}
(given $k>0$, we will have $|D|=B$)
\item for any elements $x_{i+s} \in X_{i+s}$, we have:
\begin{align}
x_{i+s} \in \{A\} \mbox{ or } x_{i+s} \in X_{i+s}-\{A\}; 
\end{align}
\item for any elements $y \in Y(B+k)$, we have:
\begin{align}
y \in \{T\}, \mbox{ or } y \in \{D\} \mbox{ or } y \in Y(B+k) - \{T\}-\{D\}.
\end{align}
\end{itemize}
 
 Now, define $\{e_i\}$ the common elements shared by $h(x_i^l, x_i^j, y^l, y^j  )$ and $h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )$. Next, we focus on computing the coefficients $R_1, R_2, \dots, R_3$.\\ 
 \begin{enumerate}
 \item Suppose $\mathbf{card} (e)=0$, then
     $$\sigma^2_0=0.$$
 In this case, we can skip computing $R_1.$
 \item Suppose $\mathbf{card}(e)=1$,  $e_1 \in \{A\}$, then
   \begin{align}
& \mbox{Cov} (h(e_1, x^j, y^l, y^j), h(e_1, x^q, y^p, y^q) ):=\sigma^2_{1(x,x)} \\
 R_2& =\binom{|A|}{1}  \binom{|A|-1}{1}  \binom{|X_{i+s}|-2 }{1} \binom{|D|}{2} \binom{|Y(B+k)|-2} {2}  \nonumber \\
&\,\,\, \,\,+\binom{|A|}{1}  \binom{|X_i|-|A|-|T|}{1} \binom{|X_{i+s}|-1} {1} \binom{|D|}{2} \binom{|Y(B+k)|-2}{2}  \nonumber \\
&\,\,\,\,\, +\binom{|A|}{1}  \binom{|T|}{1} \binom{|X_{i+s}|-1}{1} \binom{|D|}{2} \binom{|Y(B+k)|-3}{2}  \\
&=|A| (|A|-1) (B+k-2) \left( \frac{B(B-1)}{2}   \right) \left( \frac{(B+k-2)(B+k-3)}{2} \right) \nonumber \\
&\,\,\,\,\,+|A| (B-|A|-|T|) (B+k-1) \left( \frac{B(B-1)}{2}   \right) \left( \frac{(B+k-2)(B+k-3)}{2} \right) \nonumber \\
&\,\, \,\,\,+|A| |T|(B+k-1) \left( \frac{B(B-1)}{2}   \right) \left( \frac{(B+k-3)(B+k-4)}{2} \right)
   \end{align}
\item Suppose $\mathbf{card}(e)=1$,  $e_1 \in \{D\}$, then
  \begin{align}
  & \mbox{Cov} (h(x^l, x^j, e_1, y^j), h(x^p, x^q, e_1, y^q)):=\sigma^2_{1(y,y)}  \\
  R_3 & =\binom{|D|}{1} \binom{|D|-1}{1} \binom{|A|}{2} \binom{|X_{i+s}|-2}{2} \binom{|Y(B+k)|-2}{1}  \nonumber \\
        & \,\,\,\,\, +\binom{|D|}{1} \binom{|D|-1}{1} \binom{|X_i|-|A|-|T|}{2} \binom{|X_{i+s}| }{2} \binom{|Y(B+k)|-2}{1}  \nonumber \\
        & \,\,\,\,\, +\binom{|D|}{1} \binom{|D|-1}{1} \binom{|T|}{2} \binom{|X_{i+s}| }{2} \binom{|Y(B+k)|-4}{1}  \nonumber \\
          & \,\,\,\,\, +\binom{|D|}{1} \binom{|D|-1}{1} \binom{|A|}{1}\binom{|X_i|-|A|-|T|}{1} \binom{|X_{i+s}| -1}{2} \binom{|Y(B+k)|-2}{1}  \nonumber \\
          & \,\,\,\,\, +\binom{|D|}{1} \binom{|D|-1}{1} \binom{|A|}{1}\binom{|T|}{1} \binom{|X_{i+s}| -1}{2} \binom{|Y(B+k)|-3}{1}  \nonumber \\
           & \,\,\,\,\, +\binom{|D|}{1} \binom{|D|-1}{1} \binom{|X_i|-|A|-|T|}{1}\binom{|T|}{1} \binom{|X_{i+s}| }{2} \binom{|Y(B+k)|-3}{1}  \nonumber \\
 &=B(B-1)\left(  \frac{|A|(|A|-1)}{2}     \right)  \left(  \frac{(B+k-2)(B+k-3)}{2}   \right) (B+k-2) \nonumber \\
           & \,\,\,\,\, +B(B-1)\left(  \frac{(B-|A|-|T|) (B-|A|-|T|-1)}{2}     \right)  \left(  \frac{(B+k)(B+k-1)}{2}   \right) (B+k-2) \nonumber \\
           & \,\,\,\,\, +B(B-1)\left(  \frac{|T| (|T|-1)}{2}     \right)  \left(  \frac{(B+k)(B+k-1)}{2}   \right) (B+k-4) \nonumber \\
           & \,\,\,\,\, +B(B-1)|A|(B-|A|-|T|) \left(  \frac{(B+k-1)(B+k-2)}{2}   \right) (B+k-2) \nonumber \\
            & \,\,\,\,\, +B(B-1)|A||T|\left(  \frac{(B+k-1)(B+k-2)}{2}   \right) (B+k-3) \nonumber \\
              & \,\,\,\,\, +B(B-1)(B-|A|-|T|)|T| \left(  \frac{(B+k)(B+k-1)}{2}   \right) (B+k-3) 
  \end{align}
\item Suppose $\mathbf{card}(e)=1$, $e_1 \in \{T\}$, then
\begin{align}
 & \mbox{Cov} (h(x^l, e_1, y^l, y^j), h(x^p, x^q, e_1, y^q) ):=\sigma^2_{1(x,y)}  \\
 R_4 &= \binom{|T|}{1}   \binom{|T|-1}{1}    \binom{|X_{i+s}|}{2}     \binom{|D|}{2} \binom{|Y(B+k)|-4}{1}  \nonumber \\
   & \,\,\,\,\,   + \binom{|T|}{1}   \binom{|A|}{1}    \binom{|X_{i+s}|-1}{2}     \binom{|D|}{2} \binom{|Y(B+k)|-3}{1}  \nonumber \\
    & \,\,\,\,\,   + \binom{|T|}{1}   \binom{|X_i|-|A|-|T|}{1}    \binom{|X_{i+s}|}{2}     \binom{|D|}{2} \binom{|Y(B+k)|-3}{1}  \\
    &=|T|(|T|-1) \left(  \frac{(B+k)(B+k-1)}{2}   \right) \left(  \frac{B(B-1)}{2}   \right) (B+k-4) \nonumber \\
    & \,\,\,\,\, +|T||A| \left(  \frac{(B+k-1)(B+k-2)}{2}   \right) \left(  \frac{B(B-1)}{2}   \right) (B+k-3) \nonumber \\
    & \,\,\,\,\,+|T|(B-|A|-|T|) \left(  \frac{(B+k)(B+k-1)}{2}   \right) \left(  \frac{B(B-1)}{2}   \right) (B+k-3) 
\end{align}
\item  Suppose $\mathbf{card}(e)=2$, $e_1,\,e_2 \in \{A\}$, then
\begin{align}
 & \mbox{Cov} (h(e_1, e_2, y^l, y^j), h(e_1, e_2, y^p, y^q) ):=\sigma^2_{2(xx,xx)}  \\
 R_5 &= \binom{|A|}{2}  \binom{|D|}{2} \binom{|Y(B+k)|-2}{2}  \nonumber \\
 &=\left(  \frac{|A|(|A|-1)}{2}   \right) \left(  \frac{B(B-1)}{2}   \right) \left(  \frac{(B+k-2)(B+k-3)}{2}   \right) 
\end{align}
\item  Suppose $\mathbf{card}(e)=2$, $e_1,\,e_2 \in \{D\}$, then
\begin{align}
 & \mbox{Cov} (h(x^l, x^j, e_1, e_2), h(x^p, x^q, e_1, e_2) ):=\sigma^2_{2(yy,yy)}  \\
 R_6 &= \binom{|D|}{2} \binom{|A|}{2}  \binom{|X_{i+s}|-2}{2} +\binom{|D|}{2} \binom{|X_i|-|A|-|T|}{2}  \binom{|X_{i+s}|}{2} \nonumber \\
 & \,\,\,\,\, +   \binom{|D|}{2} \binom{|T|}{2}  \binom{|X_{i+s}|}{2} + \binom{|D|}{2} \binom{|A|}{1}  \binom{|X_i|-|A|-|T|}{1}\binom{|X_{i+s}|-1}{2} \nonumber \\
 & \,\,\,\,\, +  \binom{|D|}{2} \binom{|A|}{1}  \binom{|T|}{1}\binom{|X_{i+s}|-1}{2} +  \binom{|D|}{2}  \binom{|X_i|-|A|-|T|}{1} \binom{|T|}{1}\binom{|X_{i+s}|}{2} \\
 &=\left(  \frac{B(B-1)}{2}   \right) \left(  \frac{|A|(|A|-1)}{2}   \right) \left(  \frac{(B+k-2)(B+k-3)}{2}   \right)  \nonumber \\
 &\,\,\,\,\, + \left(  \frac{B(B-1)}{2}   \right) \left(  \frac{(B-|A|-|T|) (B-|A|-|T|-1)}{2}   \right) \left(  \frac{(B+k)(B+k-1)}{2}   \right)  \nonumber  \\
 &\,\,\,\,\, + \left(  \frac{B(B-1)}{2}   \right) \left(  \frac{|T| (|T|-1)}{2}   \right) \left(  \frac{(B+k)(B+k-1)}{2}   \right)  \nonumber  \\
 &\,\,\,\,\, + \left(  \frac{B(B-1)}{2}   \right) |A| (B-|A|-|T|)   \left(  \frac{(B+k-1)(B+k-2)}{2}   \right)  \nonumber  \\
 &\,\,\,\,\, + \left(  \frac{B(B-1)}{2}   \right) |A| |T|   \left(  \frac{(B+k-1)(B+k-2)}{2}   \right)  \nonumber  \\
  &\,\,\,\,\, + \left(  \frac{B(B-1)}{2}   \right) (B-|A|- |T| )|T|  \left(  \frac{(B+k)(B+k-1)}{2}   \right)   
\end{align}
\item  Suppose $\mathbf{card}(e)=2$, $e_1,\,e_2 \in \{T\}$, then
\begin{align}
& \mbox{Cov} (h(e_1, e_2, y^l, y^j), h(x^p, x^q, e_1, e_2) ):=\sigma^2_{2(xx,yy)}  \\
 R_7 &= \binom{|T|}{2}  \binom{|X_{i+s}|}{2} \binom{|D|}{2}  \nonumber \\
 &=\left(  \frac{|T|(|T|-1)}{2}   \right) \left(  \frac{(B+k)(B+k-1)}{2}   \right) \left(  \frac{B(B-1)}{2}   \right) 
 \end{align}
 \item  Suppose $\mathbf{card}(e)=2$, $e_1\in \{A\},\,e_2 \in \{T\}$, then
\begin{align}
& \mbox{Cov} (h(e_1, e_2, y^l, y^j), h(e_1, x^q, e_2, y^q) ):=\sigma^2_{2(xx,xy)}  \\
 R_8 &= \binom{|A|}{1} \binom{|T|}{1}  \binom{|X_{i+s}|-1}{1} \binom{|D|}{2} \binom{|Y(B+k)|-3}{1}   \nonumber \\
 &=|A||T| (B+k-1)\left(  \frac{B(B-1)}{2}   \right) (B+k-3) 
 \end{align}
 \item  Suppose $\mathbf{card}(e)=2$, $e_1\in \{A\},\,e_2 \in \{D\}$, then
\begin{align}
& \mbox{Cov} (h(e_1, x^j, e_2, y^j), h(e_1, x^q, e_2, y^q) ):=\sigma^2_{2(xy,xy)}  \\
 R_9 &= \binom{|A|}{1} \binom{|D|}{1}  \binom{|A|-1}{1} \binom{|X_{i+s}|-2|}{1} \binom{|D|-1}{1}   \binom{|Y(B+k)|-2}{1}  \nonumber \\
 &\,\,\,\,\,+     \binom{|A|}{1} \binom{|D|}{1}  \binom{|X_i|-|A|-|T|}{1} \binom{|X_{i+s}|-1|}{1} \binom{|D|-1}{1}   \binom{|Y(B+k)|-2}{1}  \nonumber \\
  &\,\,\,\,\,+     \binom{|A|}{1} \binom{|D|}{1}  \binom{|T|}{1} \binom{|X_{i+s}|-1|}{1} \binom{|D|-1}{1}   \binom{|Y(B+k)|-3}{1}  \nonumber \\
  &= |A|B(|A|-1)(B+k-2) (B-1)(B+k-2) \nonumber \\
  & \,\,\,\,\, + |A|B(B-|A|-|T|)(B+k-1) (B-1)(B+k-2)   \nonumber \\
  & \,\,\,\,\, + |A|B|T|(B+k-1) (B-1)(B+k-3)   
 \end{align}
 \item  Suppose $\mathbf{card}(e)=2$, $e_1\in \{T\},\,e_2 \in \{D\}$, then
 \begin{align}
& \mbox{Cov} (h(x^l,e_1, y^l, e_2), h(x^p, x^q, e_1, e_2)):=\sigma^2_{2(xy,yy)}  \\
 R_{10} &= \binom{|T|}{1} \binom{|D|}{1}  \binom{|A|}{1} \binom{|X_{i+s}|-1}{2} \binom{|D|-1}{1}    \nonumber \\
 &\,\,\,\,\,+    \binom{|T|}{1} \binom{|D|}{1}  \binom{|X_i|-|A|-1}{1} \binom{|X_{i+s}|}{2} \binom{|D|-1}{1}    \nonumber \\
  &= |T|B|A|           \left(  \frac{(B+k-1)(B+k-2)}{2}   \right)     (B-1)      \nonumber \\
  & \,\,\,\,\, + |T|B(B-|A|-1)         \left(  \frac{(B+k)(B+k-1)}{2}   \right)    (B-1) 
 \end{align}
  \item  Suppose $\mathbf{card}(e)=3$, $e_1,e_2 \in \{A\},\,e_3 \in \{D\}$, then
  \begin{align}
 & \mbox{Cov} (h(e_1, e_2, y^l, e_3), h(e_1, e_2, y^p, e_3) ):=\sigma^3_{2(xxy,xxy)}  \\
 R_{11} &= \binom{|A|}{2} \binom{|D|}{1}  \binom{|Y(B+k)|-1}{1}  \\
 & =  \left(  \frac{|A|(|A|-1)}{2}   \right)     B (B+k-1)
 \end{align}
   \item  Suppose $\mathbf{card}(e)=3$, $e_1 \in \{A\},\,e_2,e_3 \in \{D\}$, then
  \begin{align}
 & \mbox{Cov} (h(e_1, x^j, e_2, e_3), h(e_1, x^q, e_2, e_3) ):=\sigma^3_{2(xyy,xyy)}  \\
 R_{12} &= \binom{|A|}{1} \binom{|D|}{2} \binom{|A|-1}{1}  \binom{|X_{i+s}|-2}{1}  \nonumber  \\
 & \,\,\,\,\,+ \binom{|A|}{1} \binom{|D|}{2} \binom{|X_i|-|A|}{1}  \binom{|X_{i+s}|-1}{1}  \nonumber  \\
 &=|A| (|A|-1)  \left(  \frac{B(B-1)}{2}  \right)  (B+k-2) +|A|(B-|A|)\left(  \frac{B(B-1)}{2}  \right) (B+k-1)
 \end{align}
  \item  Suppose $\mathbf{card}(e)=3$, $e_1 \in \{A\},\,e_2 \in \{T\},\, e_3 \in \{D\}$, then
  \begin{align}
 & \mbox{Cov} (h(e_1, e_2,y^l, e_3), h(e_1, x^1, e_2, e_3) ):=\sigma^3_{2(xxy,xyy)}  \\
 R_{13} &= \binom{|A|}{1} \binom{|T|}{1} \binom{|D|}{1} \binom{|D|-1}{1} \binom{|X_{i+s}|-1}{1}  \nonumber  \\
 &=|A||T|B(B-1) (B+k-1) 
 \end{align}
  \item  Suppose $\mathbf{card}(e)=4$, $e_1,e_2 \in \{A\},\,e_3,e_4 \in \{D\}$, then
  \begin{align}
 & \mbox{Cov} (h(e_1, e_2,e_3, e_4), h(e_1, e_2, e_3, e_4) ):=\sigma^2_{4(xxyy,xxyy)}  \\
 R_{14} &= \binom{|A|}{2} \binom{|D|}{2}   \nonumber  \\
 &=  \left(  \frac{|A|(|A|-1)}{2}  \right)      \left(  \frac{B(B-1)}{2}  \right)    
 \end{align}
 \end{enumerate}
 
 Given the specific locations of the blocks, we can obtain the explicit expressions of $|A|$ and $|T|$ in terms of $i,s,B,k$.
 That is:
 \begin{align}
 & |A|=\max \left\{ 0,  1+\min \{ (i+1)B,(i+s+1)(B+k)  \} -\max \{ iB+1, (i+s)(B+k)+1  \}                 \right\} \\
 & |T|=\max \left\{ 0,1+(B+k)-(iB+1) \right\}.
 \end{align}
 More specificly, 
 \begin{align}
&  |A|=\left\{\begin{array}{ll}
 ik+(s+1)(B+k) ,& \mbox{ if } \left\lceil    \frac{iB+1}{B+k}           \right\rceil-i-1 \leqslant  s\leqslant    \left\lfloor   \frac{(i+1)B}{B+k}    \right\rfloor -i-1 ;  \\
B, & \mbox{ if }  \left\lceil    \frac{(i+1)B}{B+k}           \right\rceil-i-1 \leqslant  s\leqslant    \left\lfloor   \frac{iB}{B+k}    \right\rfloor -i ;\\
B-ik-s(B+k),  & \mbox{ if }  \left\lceil    \frac{iB}{B+k}           \right\rceil-i \leqslant  s\leqslant    \left\lfloor   \frac{(i+1)B-1}{B+k}    \right\rfloor -i; \\
0, & \mbox{ otherwise. }
 \end{array} \right. \\
 & |T|=\left\{ \begin{array}{ll}
 k-(i-1)B & \mbox{ if }    \left\lceil    \frac{B+k}{B}           \right\rceil-1 \leqslant i \leqslant \left\lfloor \frac{B+k-1}{B} \right\rfloor ;\\
 B, &  \mbox{ if }    i \leqslant \left\lfloor \frac{B+k}{B} \right\rfloor-1 ;\\
 0,& \mbox{ otherwise}.
 \end{array} \right.
 \end{align}
 \newpage
 
 Let 
 \begin{align}
& X_1^i,X_2^i, \dots, X_B^i \sim P(x) \\
& X_1^j, X_2^j, \dots, X_B^j \sim P(x) \\
& \mbox{and }Y_1, Y_2, \dots, Y_B \sim Q(x)
 \end{align}
 and $i \neq j, i, j= 1,2,\dots, n$.\\
 
 Recall our propoosed statisitic is
 \begin{align}
 s_t =\max_{B \in \{2,3,\dots\}} \frac{1}{n} \sum_{i=1}^n \mbox{MMD}_i^2 (B)
 \end{align}
 
 We need to compute $$\mbox{VAR} \left(  \mbox{MMD}_i^2 (B) \right)$$
 and $$\mbox{COV}   \left(  \mbox{MMD}_i^2 (B) ,\mbox{ MMD}_j^2(B+k)             \right)       $$
 where if $k<0$,  $k=-1,-2,\dots, -B+2$ or if $k>0$, $k=1,2,3,\dots$
 
 Review the construction of $\mbox{MMD}^2(B)$, which is unbiased estimator.
  Let 
 \begin{align}
& X_1,X_2, \dots, X_B \sim P(x) \\
& \mbox{and }Y_1, Y_2, \dots, Y_B \sim Q(x)
 \end{align}
 We construct paired data $z_i=(x_i,y_i)$. Then the $\mbox{MMD}^2(B)$ is 
 \begin{align}
 \mbox{MMD}^2(B)=\frac{1}{B(B-1)} \sum_{i \neq j}^B h(z_i,z_j)=\frac{2}{B(B-1)} \sum_{C_{2,B}}h(z_i,z_j)
 \end{align}
 where $h(z_i,z_j)=\kappa(x_i,x_j)+\kappa(y_i,y_j)-\kappa(x_i,y_j)-\kappa(x_j,y_i)$.
 
\subsubsection{Compute $\mbox{VAR} \left(  \mbox{MMD}_i^2 (B) \right)$   } 
Suppse $i=1,2,\dots, n$. For any fixed $i$
\begin{align}
\mbox{VAR} \left( \mbox{MMD}_i^2(B)  \right)=CB^{-2}
 \end{align}
 where $C=2 \sum_{l=1}^{\infty} \lambda_l^2 $ and $\lambda_l$ is the eigenvalues of the kernel matrix.
More details can be found in "tech report 2008 "

\subsubsection{Compute $ \mbox{COV}   \left(  \mbox{MMD}_i^2 (B) ,\mbox{ MMD}_j^2(B)             \right)       $ }
Suppose $i \neq j, i,j=1,2,\dots, n$. For any $i, j$
 \begin{align}
 & \mbox{COV} \left(  \mbox{MMD}_i^2 (B),    \mbox{MMD}_j^2 (B)  \right) \\
 &=\mbox{COV} \left(  \binom{B}{2} ^{-1}    \sum_{i \in C_{2,B}} h(z_{i1},z_{i2}),   \binom{B}{2} ^{-1}    \sum_{j \in C_{2,B}} h(z_{j1},z_{j2})       \right) \\
 &= \binom{B}{2}^{-2} \sum_{i \in C_{2,B}} \sum_{j \in C_{2,B}} \mbox{COV}\left(  h(z_{i1},z_{i2}), h( z_{j1},z_{j2}    )          \right)\\
 &= \binom{B}{2}^{-2} \sum_{i \in C_{2,B}} \sum_{j \in C_{2,B}} \mbox{COV}\left(  h(x_{i1}, y_{i1}, x_{i2}, y_{i2}), h( x_{j1},y_{j1}, x_{j2}, y_{j2} )          \right)\\
 &= \binom{B}{2}^{-2}  \sum_{c=0}^2 \binom{B}{2} \binom{2}{c} \binom {B-2}{2-c}  \sigma_c^2  
 \end{align}
One concern here is when $B$ is a small number, could we still use Stirling's approximation, that is:
\begin{align}
n! \sim \sqrt{2 \pi n} \left( \frac{n}{e} \right)^n
\end{align}
When $B$ is a large number, we will have 
\begin{align}
\mbox{COV} \left(  \mbox{MMD}_i^2 (B),    \mbox{MMD}_j^2 (B)  \right) \approx \frac{4}{B} \sigma_1^2
\end{align}
We need to compute $\sigma_1^2$
\begin{align}
\sigma_1^2 &=\mbox{VAR}\left(h(Z,z_j)\right) \\
& =\mbox{VAR}\left(h(X, Y, x_j, y_j)\right)\\
&= \mbox{VAR} \left(   \kappa(X,x_j) +\kappa(Y, y_j) -\kappa(X, y_j)+\kappa(x_j, Y)  \right)
\end{align}

\begin{align}
h(X, Y, x_j, y_j) &= \kappa(X,x_j) +\kappa(Y, y_j) -\kappa(X, y_j)+\kappa(x_j, Y) 
\end{align}
Next, we can write kernel $\kappa(x_i, x_j)$ interms of eigenfunctions $\phi_i(x)$.
\begin{align}
\kappa (x,x')=\sum_{l=1}^{\infty} \lambda_l \phi_l (x) \phi_l(x')
\end{align}
where \begin{align}
\int  \kappa(x,x')\phi_i (x)d \mbox{Pr} (x)=\lambda_i \phi_i(x')
\end{align}
\subsubsection{Compute $ \mbox{COV}   \left(  \mbox{MMD}_i^2 (B) ,\mbox{ MMD}_j^2(B+k)             \right)       $}
Suppose, $k=-1,-2,\dots, -B+2$ or  $k=1,2,3,\dots $ \\
\begin{align}
 & \mbox{COV} \left(  \mbox{MMD}_i^2 (B),    \mbox{MMD}_j^2 (B+k)  \right) \\
 &=\mbox{COV} \left(  \binom{B}{2} ^{-1}    \sum_{i \in C_{2,B}} h(z_{i1},z_{i2}),   \binom{B+k}{2} ^{-1}    \sum_{j \in C_{2,B+k}} h(z_{j1},z_{j2})       \right) \\
 &= \binom{B}{2}^{-1}   \binom{B+k}{2}^{-1} \sum_{i \in C_{2,B}} \sum_{j \in C_{2,B+k}} \mbox{COV}\left(  h(z_{i1},z_{i2}), h( z_{j1},z_{j2}    )          \right)\\
 &= \binom{B}{2}^{-1}    \binom{B+k}{2}^{-1}    \sum_{i \in C_{2,B}} \sum_{j \in C_{2,B+k}} \mbox{COV}\left(  h(x_{i1}, y_{i1}, x_{i2}, y_{i2}), h( x_{j1},y_{j1}, x_{j2}, y_{j2} )          \right)\\
 &= \binom{B}{2}^{-1}  \binom{B+k}{2}^{-1}      \sum_{c=0}^2 \binom{B}{2} \binom{2}{c} \binom {B+k-2}{2-c}  \sigma_c^2  
\end{align}




\end{document}



