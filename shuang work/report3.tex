\documentclass[11 pt]{article}
\usepackage[top=0.8 in, bottom=0.8  in, right=0.8in, left=0.8 in]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsfonts}
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{amssymb, amsmath}% Use packages about maths
\usepackage{amsthm}
\usepackage[english]{babel}
\usepackage{enumerate,bbm}
\usepackage{geometry}
%\usepackage{enumerate}
%\newtheorem{theorem}{Claim}
%\newenvironment{proof}{\noindent {\sc Proof:}}{$\Box$ \medskip}
%\renewcommand{\labelenumi}{(\alph{enumi})}
%\newcommand{\prob}[1]{\mathbf{Pr}(#1)}
%\newcommand{\lcm}{\mbox{lcm}}
%\newcommand{\reals}{\mathbb{R}}
%\newcommand{\tih}{\tilde{H}}
%\newcommand{\ex}{\langle\mbox{EXPR}\rangle}
%\newcommand{\ter}{\langle\mbox{TERM}\rangle}
%\newcommand{\fac}{\langle\mbox{FACTOR}\rangle}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\usepackage{hyperref}
\usepackage{color}
\usepackage{natbib}
\usepackage{amsmath}
\usepackage{indentfirst}
\renewcommand{\baselinestretch}{1.3}
\usepackage{float}
\begin{document}
\section{Background}
Assume observations 
\begin{align*}
& X:=\{x_1, x_2, \dots, x_m\} \stackrel {iid} {\sim} P,\\
& Y:=\{y_1, y_2, \dots, y_m\} \stackrel{iid}{\sim} Q.
\end{align*}
Let $\mathcal{F}$ be a class of functions $f: \mathcal{X} \to \mathbb{R}$ and define the \textit{maximum mean discrepancy} (MMD) as 
\begin{align}
\mbox{MMD} [\mathcal{F}, P, Q] := \sup_{f\in \mathcal{F}} \left( \mathbb{E}_x [f(x)] -\mathbb{E}_y [f(y)]\right).
\end{align}
In the reproducing kernel Hilbert space $\mathcal{H}$, the $\mbox{MMD} $ can be expressed as the distance between mean embeddings in $\mathcal{H}$, and furthermore can be expressed in terms of kernel functions. That is
\begin{align*}
\mbox{MMD}^2 [\mathcal{F}, P, Q ] & =\left[  \sup_{\| f \|_{\mathcal{H} \leqslant 1}     }     ( \mathbb{E}_x [f(x)] -\mathbb{E}_y [f(y)]    )    \right]^2 \\
& = \left[  \sup_{\|f\|_{\mathcal{H} \leqslant 1}}    \langle \mu_P -\mu_Q, f \rangle_{\mathcal{H}}  \right]^2 \\
&=\| \mu_P -\mu_Q  \|^2_{\mathcal{H}} \\
&=\langle \mu_P,\mu_P  \rangle_{\mathcal{H}} + \langle \mu_Q,\mu_Q  \rangle_{\mathcal{H}}  -2 \langle \mu_P,\mu_Q \rangle_{\mathcal{H}}  \\
&= \mathbb{E}_{x,x'} \langle  \phi(x), \phi(x') \rangle_{\mathcal{H}} +\mathbb{E}_{y,y'} \langle  \phi(y), \phi(y') \rangle_{\mathcal{H}} -2 \mathbb{E}_{x,y} \langle  \phi(x), \phi(y) \rangle_{\mathcal{H}} \\
&= \mathbb{E}_{x,x'} \kappa(x,x')+ \mathbb{E}_{y,y'} \kappa(y,y')-2 \mathbb{E}_{x,y} \kappa(x,y)
\end{align*}
Next, let $\mbox{MMD}_u^2$ be the unbiased estimate of $\mbox{MMD}^2 [\mathcal{F}, P, Q ]$ by replacing the population expectations with their corresponding U-statistics and sample averages:
\begin{align}
\mbox{MMD}_u^2 =\frac{1}{m(m-1)} \sum_{i,j=1, i \neq j}^m h(z_i,z_j),
\end{align}
where $z^i :=(x_i,y_i)$ and $h(z_i,z_j)=\kappa(x_i,x_j)+\kappa(y_i,y_j)-\kappa(x_i,y_j)-\kappa (x_j,y_i).$\\

Intuitively, the empirical test statistic $\mbox{MMD}_u^2$ is expected to be small (close to zero) if $P=Q$, and large if $P$ and $Q$ are far apart. And it will cost $O(m^2)$ time to compute the statistic, given both sample sizes of $\{x_i\}$ and $\{y_i\}$ are $m$.\\

Under $H_0 \, (P=Q)$, the U-statistic is degenerate, which means $\mathbb{E}_{z'} h(z,z')=0$. In this case, $\mbox{MMD}_u^2$ converges in distribution according to 
\begin{align}
m\mbox{MMD}^2_u \stackrel{D} \to \sum_{l=1}^{\infty} \lambda_{l} [z_l^2 -2],
\end{align}
where $z_l \sim N(0,2)$ and $\lambda_i$ are the solutions to the eigenvalue equation
\begin{align}
\int_{\mathcal{X}} \hat{\kappa} (x,x') \phi_i (x) d \mbox{Pr} (x)= \lambda_i \phi_i (x'),
\end{align}
and $\hat{\kappa} (x_i, x_j) := \kappa (x_i, x_j) -\mathbb{E}_x \kappa(x_i, x)-\mathbb{E}_x \kappa (x,x_j) +\mathbb{E}_{x,x'} \kappa (x,x')$ is the centered RKHS kernel.\\

The main idea of B-test is to split the data into blocks of size B, compute the quadratic-time $\mbox{MMD}_u^2$ on each block, and then average the resulting statistics.
\section{B test}
Given observations $D_t=\{x_1,x_2,\dots, x_t\}$, which will be split into $(n+1)$ blocks, each with size $B$, we compute the B-test statistic:
\begin{align*}
s_t &=\max_{B \in \{2,3,\dots, m\}} \left\{\frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_i(B)\right\}\\
&=\max_{B\in \{2,3,\dots, m\}} \left\{ \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2 (X_i,Y; B) \right \} \\
&=\max_{B\in \{2,3,\dots,m\}} \left \{ \frac{1}{n} \sum_{i=1}^n \frac{1}{B(B-1)} \sum_{j,l=1, j \neq l}^B h( x_i^j, x_i^l, y^j, y^l)  \right\} \\
&= \max_{B \in \{2,3,\dots, m\} } \left \{ \frac{1}{n} \sum_{i=1}^n \frac{1}{B(B-1)} \sum_{j,l=1, j \neq l}^B 
\kappa(x_i^j, x_i^l) +\kappa(y^j,y^l)-\kappa(x_i^j,y^l)-\kappa (x_i^l,y^j)
  \right\} \\
\end{align*}

 
 \newpage
 \subsection{The Asymptotic Distribution}
 Recall the $\mbox{MMD}_i^2(B)$ is 
 \begin{align}
 \mbox{MMD}_i^2(B) &= {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  ),
 \end{align}
 where $h(x_i^l, x_i^j, y^l, y^j  ) =\kappa(x_i^l,x_i^j)+\kappa(y^l,y^j)-\kappa(x_i^l,y^j)-\kappa(x_i^j,y^l)$.

 
 In preparation for the next step analysis, we compute the following variance and covariance:
 \begin{itemize}
 \item $\mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right), \,\, i=1,2,\dots, n$;
 \item $\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right), \,\, s=(1-i), (2-i), \dots, (n-i),   s \neq 0  $;
  \item $\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right), \,\, s=(1-i), (2-i), \dots, (n-i),   s \neq 0, \,\, k=2-B, 3-B, \dots, m-B, k \neq 0$.
 \end{itemize}
 
\subsubsection{$  \mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right) $} 
For $i=1,2,\dots, n$,
\begin{align}
& \mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right) \\
 =& \mbox{Var} \left(     {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  )  \right)  \\
 =&    {\binom{B}{2} }^{-2}  \left[  \binom{B}{2} \binom{2}{1} \binom{B-2}{2-1} \sigma_{xy}^2  +                             \binom{B}{2} \binom{2}{2} \binom{B-2}{2-2} \sigma_{xx'yy'}^2  \right],\,\,\mbox{where}
\end{align}
\begin{align}
 \sigma_{xy}^2:= \mbox{Var} \left(     \mathbb{E}_{xy} [h(x,x',y,y')]  \right)=\mbox{Var}(0)=0 ,\\
 \sigma_{xx'yy'}^2 :=\mbox{Var}  \left(  h(x,x',y,y')   \right)=\mathbb{E} [h^2(x,x',y,y')].
\end{align}
Thus, 
\begin{align}
\mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right)=\frac{2}{B(B-1)} \mathbb{E} [h^2(x,x',y,y')].
\end{align}
\begin{itemize}
\item Evaluation See  MMDvar.m
\end{itemize}

\subsubsection{$    \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)  $}
For $i=1,2,\dots, n$, and $s=(1-i), (2-i), \dots, (n-i),   s \neq 0$,
\begin{align}
   & \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right) \\
= & \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B}{2} }^{-1}  \sum_{p < q}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
=&  {\binom{B}{2} }^{-2} \left[      \binom{B}{2} \binom{2}{1} \binom{B-2}{2-1} \sigma_{y}^2  +                             \binom{B}{2} \binom{2}{2} \binom{B-2}{2-2} \sigma_{yy'}^2                   \right],\,\, \mbox{where}
\end{align}
\begin{align}
 \sigma_{y}^2 :&=\mbox{Cov} \left(  h(x_i^l,x_i^j,y,y^j),h(x_{i+s}^p,x_{i+s}^q, y, y^q)   \right)\\ 
 &=\int \mbox{Pr}( x_i^l,x_i^j,y,y^j,x_{i+s}^p,x_{i+s}^q, y, y^q  )  h(x_i^l,x_i^j,y,y^j) h(x_{i+s}^p,x_{i+s}^q, y, y^q) \\
 &=\int \mbox{Pr}(x_i^l,y) \mbox{Pr}(x_{i+s}^p,y)  \int \mbox{Pr} (x_i^j,y^j)  h(x_i^l,x_i^j,y,y^j) \int \mbox{Pr} (x_{i+s}^q, y^q) h(x_{i+s}^p,x_{i+s}^q, y, y^q) \\
 &=0\\
 \sigma_{yy'}^2:&=\mbox{Cov} \left(  h(x_i^l,x_i^j,y,y'),h(x_{i+s}^p,x_{i+s}^q, y, y')   \right)
\end{align}
Thus, 
\begin{align}
\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)
=\frac{2}{B(B-1)} \mbox{Cov} \left(  h(x_i^l,x_i^j,y,y'),h(x_{i+s}^p,x_{i+s}^q, y, y')   \right)
\end{align}
\begin{itemize}
\item Evaluation See  MMDcov1.m
\end{itemize}
\subsubsection{$   \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right)     $}
For $i=1,2,\dots, n$, $s=(1-i), (2-i), \dots, (n-i),   s \neq 0$, and $ k=2-B, 3-B, \dots, m-B, k \neq 0$, our objective is to compute
 \begin{align}
    \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i}^2 (B+k)    \right)  
  \end{align}
  Suppose $s=0$,
  \begin{align}
    &  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i}^2 (B+k)    \right)  \\
  =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B+k}{2} }^{-1}  \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}     \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B+k}{2} }^{-1}  \sigma_{xx'yy'}^2
 \end{align}
 \begin{align}
    &  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i}^2 (B-k)    \right)  \\
  =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B-k}{2} }^{-1}  \sum_{p < q}^{B-k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B-k}{2} }^{-1}     \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B-k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}  \sigma_{xx'yy'}^2
 \end{align}
 
 In summary, 
 \begin{align}
   \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i}^2 (T)    \right) ={\binom{B \vee T}{2} }^{-1}  \sigma_{xx'yy'}^2
 \end{align}
 Suppose $s\neq 0$,
  \begin{align}
    &  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right)  \\
  =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B+k}{2} }^{-1}  \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}     \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B+k}{2} }^{-1}  \sigma_{yy'}^2
 \end{align}
 \begin{align}
    &  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B-k)    \right)  \\
  =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B-k}{2} }^{-1}  \sum_{p < q}^{B-k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B-k}{2} }^{-1}     \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B-k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}  \sigma_{yy'}^2
 \end{align}
 In summary, 
 \begin{align}
 \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (T)    \right) ={\binom{B \vee T}{2} }^{-1}  \sigma_{yy'}^2
 \end{align}
\begin{itemize}
\item Evaluation: see MATLAB codes
\end{itemize}if :
\newpage
\subsection{Asymptotic Distribution}
Recall that:
\begin{align}
s_t=\max_{B \in \{2,3,\dots, m\}} \frac{1}{n} \sum_{i=1}^n \mbox{MMD}_i^2(B)
\end{align}
Define $Z_B:= \frac{1}{n} \sum_{i=1}^n \mbox{MMD}_i^2(B)     $.

\subsubsection{$\mbox{Var}(Z_B)$}
\begin{align}
\mbox{Var}(Z_B) &= \mbox{Var}\left(\frac{1}{n} \sum_{i=1}^n  \mbox{MMD}^2_i (B) \right) \\
                         &= \frac{1}{n^2}
\end{align}
\subsubsection{$\mbox{Cov}(Z_B,Z_T)$}
\begin{align}
& \mbox{Cov} \left(  \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_i(B), \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_{i+s} (B+k)   \right) \\
&=\frac{1}{n^2} \sum_{i=1}^n \left(     {\binom{B+k}{2} }^{-1} \sigma^2_{xx'yy'}       +(n-1)   {\binom{B+k}{2} }^{-1}      
\sigma^2_{yy'} \right)  \\
&=\frac{1}{n} \frac{2}{(B+k)(B+k-1)}  \sigma^2_{xx'yy'}  +\frac{n-1}{n}  \frac{2}{(B+k)(B+k-1)}  \sigma^2_{yy'}  \\
&=\frac{2}{n}B^{-1}(B-1)^{-1} \sigma^2_{xx'yy'}\left( 1+\frac{k}{B}    \right)^{-1} \left( 1+\frac{k}{B-1}    \right)^{-1}+ \\
&   \,\,\, \,\,\,\,   \frac{2(n-1)}{n}B^{-1}(B-1)^{-1} \sigma^2_{yy'}\left( 1+\frac{k}{B}    \right)^{-1} \left( 1+\frac{k}{B-1}    \right)^{-1} \\
&\approx  \frac{2}{n}B^{-1}(B-1)^{-1} \sigma^2_{xx'yy'}\left( 1-\frac{k}{B}    \right) \left( 1-\frac{k}{B-1}    \right)+
\\
&   \,\,\, \,\,\,\,   \frac{2(n-1)}{n}B^{-1}(B-1)^{-1} \sigma^2_{yy'}\left( 1-\frac{k}{B}    \right) \left( 1-\frac{k}{B-1}    \right) 
\end{align}

\begin{align}
& \mbox{Cov} \left(  \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_i(B), \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_{i+s} (B-k)   \right) \\
&=\frac{1}{n^2} \sum_{i=1}^n \left(     {\binom{B}{2} }^{-1} \sigma^2_{xx'yy'}       +(n-1)   {\binom{B}{2} }^{-1}      
\sigma^2_{yy'} \right)  \\
&=\frac{1}{n} \frac{2}{B(B-1)}  \sigma^2_{xx'yy'}  +\frac{n-1}{n}  \frac{2}{B(B-1)}  \sigma^2_{yy'}  
\end{align}



 We would like to study $\mbox{Pr}_{H_0} (s_t >b)$ for given $t$:
\begin{align}
&  \mbox{Pr} \left( \max_{2 \leqslant B \leqslant m} Z_B >b       \right) \\
=& \mathbb{E} \left(  \frac{\sum_{B}   e^{\xi Z_B -\psi(\xi)}   }{\sum_l   e^{\xi Z_l -\psi(\xi)}}   ; \max_{2 \leqslant B\leqslant m} Z_B >b    \right) \\
=& \sum_B \mathbb{E}  \left(     \frac{e^{\xi Z_B -\psi(\xi)}   }{\sum_l   e^{\xi Z_l -\psi(\xi)}}   ; \max_{2 \leqslant B\leqslant m} Z_B >b       \right) \\
=& \sum_B \mathbb{E}_B \left(     \frac{1  }{\sum_l   e^{\xi Z_l -\psi(\xi)}}   ; \max_{2 \leqslant B\leqslant m} Z_B >b       \right)\\
=& \sum_B  e^{\psi(\xi_B) -\xi_B b}   \mathbb{E}_B \left(     \frac{e^{\xi_B \max_l [Z(l)-Z(B)]} e^{-\xi [Z_B-b+\max_l [Z_l-Z_B] ]}  }{\sum_l   e^{\xi Z_l -\psi(\xi)}}   ; Z_B-b+ \max_l [Z_l-Z_B] \geq 0       \right) \\
= & \sum_B  \underbrace{ e^{\psi(\xi_B) -\xi_B b} }_{\mbox{I}}  
   \underbrace{  \mathbb{E}_B \left( \frac{M_B}{S_B}    e^{-\xi_B[Z_B-b+\frac{\mbox{log} M_B}{\xi_B}]}   ; Z_B-b+ \frac{\mbox{log} M_B}{\xi_B} \geq 0       \right)    }_{\mbox{II}}  \\
\end{align}

\subsubsection{Gaussian Approximation}
 First, we approximate the distribution of $Z_B$ by Gaussian with the same mean and variiance. \\
That is 
\begin{align}
Z_B \sim Gaussian(0, \delta^2_B)
\end{align}
where 
\begin{align}
& \delta^2_B:= \mbox{VAR}(Z_B)=\frac{1}{n} \frac{2}{B(B-1)}\sigma^2_{xx'yy'} +\frac{n-1}{n} \frac{2}{B(B-1)} \sigma^2_{yy'} 
\end{align}
and $\mathbb{E}(Z_B)=0$.\\
Evaluation by Q-Q plot:
\begin{figure}[H]
                \centering
                \includegraphics[trim = 15mm 60mm 20mm 60mm, clip, scale=0.45]{normality}
                \caption{Normality of $Z_B$}
\end{figure}

\subsubsection{Large Deviation}
Next, we compute $\mbox{I}$, the large deviation component. \\
We need to compute the function:
\begin{align}
\dot {\psi} (\xi_B) =b
\end{align}
where $\psi(\xi)= \mbox{log} \mathbb{E}(e^{\xi Z_B})$. Specifically, for $Z_B \sim Gaussian(0, \delta^2_B)$, we will have
\begin{align}
\psi(\xi)= \mbox{log} e^{\xi \mu +\frac{1}{2} \delta^2_B \xi^2  }=\frac{1}{2} \delta^2_B \xi^2 . 
\end{align}
Thus, $ \dot {\psi} (\xi_B) =b \Rightarrow \xi_B= \frac{b}{\delta^2_B}$. Furthermore,
\begin{align}
\psi(\xi_B)=\frac{1}{2} \frac{b^2}{\delta^2_B}
\end{align}
And 
\begin{align}
  \boxed{    \mbox{I}: =e^{\psi(\xi_B) -\xi_B b} =e^ {-\frac{1}{2}  \frac{b^2}{\delta^2_B}   }.  }
\end{align}

\subsubsection{Local Field}
 We analyze the local field $\{l_T-l_B\}$, where
\begin{align}
l_T :=\xi_T Z_T-\psi(\xi_T) \\
l_B :=\xi_B Z_B-\psi(\xi_B) 
\end{align}
We can write:
\begin{align}
l_T :=\xi_T Z_T-\psi(\xi_T) =\frac{b}{\delta_T}  X_T-\frac{1}{2} \frac{b^2}{\delta^2_T}\\
l_B :=\xi_B Z_B-\psi(\xi_B) =\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}
\end{align}
where $X_T,X_B$ follow standard normal distribution.
\begin{align}
\mathbb{E}_B \{ l_T-l_B    \} 
& =\mathbb{E}_B \left\{   \left(  \frac{b}{\delta_T}  X_T-\frac{1}{2} \frac{b^2}{\delta^2_T}    \right)-
\left(         \frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}          \right)  \right\} \\
&=\mathbb{E}  \left\{  \left(  \left(  \frac{b}{\delta_T}  X_T-\frac{1}{2} \frac{b^2}{\delta^2_T}    \right)-
\left(         \frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}          \right)  \right) e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}} \right\} \\
&=\mathbb{E}  \left\{  \left(  \left(  \frac{b}{\delta_T}  (  \sqrt{1-r^2} W+r X_B     )-\frac{1}{2} \frac{b^2}{\delta^2_T}    \right)-
\left(         \frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}          \right)  \right) e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}} \right\} . 
\end{align}
The above representation results from the regression of $X_T$ on $X_B$ with $W$ being the standardized residual of the regression. And $r$ is defined as:
\begin{align}
r& =\mbox{COV} (X_T,X_B)=\mbox{COV} ( \frac{Z_T}{\delta_T}  ,  \frac{Z_B}{\delta_B}  ) \\
& =\frac{1}{\delta_T \delta_B}   \frac{1}{n}\left[ { \binom{B \vee T}{2} }^{-1} \sigma^2_{xx'yy'} +(n-1) { \binom{B \vee T}{2} }^{-1} \sigma^2_{yy'}            \right] \\
&= \frac{1}{\delta_T \delta_B}  { \binom{B \vee T}{2} } ^{-1}   \left[  \frac{1}{n}  \sigma^2_{xx'yy'} +\frac{n-1}{n}  \sigma^2_{yy'}            \right] .
\end{align}
Recall: 
\begin{align}
& \delta_T =  \sqrt{   { \binom{T}{2} } ^{-1}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'} \right]  } \\
& \delta_B =  \sqrt{   { \binom{B}{2} } ^{-1}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'} \right]  }
\end{align}
Thus,
\begin{align}
r=\frac{  { \binom{B \vee T}{2} } ^{-1}  }{    \sqrt{   { \binom{T}{2} } ^{-1}      { \binom{B}{2} } ^{-1}     }  } 
=\frac{      \sqrt{   { \binom{T}{2} }     { \binom{B}{2} }    }                 }{    { \binom{B \vee T}{2} }      }
\end{align}


Now, we have
\begin{align}
\mathbb{E}_B \{ l_T-l_B    \} = & \mathbb{E}_B  \left\{   \left(  \frac{br }{\delta_T}  X_B    
-\frac{1}{2} \frac{b^2}{\delta^2_T}    \right)-
\left(         \frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}          \right)   \right\} \\
= &\mathbb{E}_B  \left\{    \frac{br }{\delta_T}  X_B    
-      \frac{b}{\delta_B}  X_B \right\}  +
\mathbb{E}_B  \left\{   \frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}      \right\} 
\end{align}
For the first term, we have: 
\begin{align}
& \mathbb{E}_B  \left\{    \frac{br }{\delta_T}  X_B    
-      \frac{b}{\delta_B}  X_B \right\} \\
= &\mathbb{E}  \left\{   \left(  \frac{br }{\delta_T}  X_B    
-      \frac{b}{\delta_B}  X_B \right) e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}} \right\}  \\
= & \left(    \frac{br}{\delta_T}-\frac{b}{\delta_B}   \right) \mathbb{E} \left\{     X_B     e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}}                \right\}  \\
=& \frac{b^2 r}{\delta_T \delta_B}-\frac{b^2}{\delta^2_B} 
\end{align}
where
\begin{align}
&\mathbb{E} \left\{     X_B     e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}}                \right\}  \\
=& \frac{1}{\sqrt{2 \pi}} \int X_B e^{-\frac{1}{2} \left(X_B  -\frac{b}{\delta_B} \right)^2} dX_B \\
=& \frac{1}{\sqrt{2 \pi}} \int \left(X_B- \frac{b}{\delta_B} \right) e^{-\frac{1}{2} (X_B  -\frac{b}{\delta_B} )^2} dX_B 
+  \frac{1}{\sqrt{2 \pi}} \int \frac{b}{\delta_B} e^{-\frac{1}{2} (X_B  -\frac{b}{\delta_B} )^2} dX_B \\
= & \frac{b}{\delta_B},
\end{align}

Similarly, for the second term, we have:
\begin{align}
& \mathbb{E}_B  \left\{   \frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}      \right\} \\
= &\mathbb{E}  \left\{   \left(  \frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}          \right)   e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}} \right\}  \\
=& \frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}  
\end{align}
where
\begin{align}
&  \mathbb{E} \left\{      e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}}                \right\}  \\
=&  \frac{1}{\sqrt{2 \pi}} \int  e^{-\frac{1}{2} \left(X_B  -\frac{b}{\delta_B} \right)^2} dX_B \\
=& 1
\end{align}
Finally, we get: 
\begin{align}
\boxed{ \mathbb{E}_B \{ l_T-l_B    \} = \left(  \frac{b^2 r}{\delta_T \delta_B}-\frac{b^2}{\delta^2_B}  \right)        +\left(  \frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}    \right)   = \frac{b^2 r}{\delta_T \delta_B} -\frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}        }
\end{align}

Similarly, 
\begin{align}
\mbox{Var}_B \{ l_T-l_B    \} 
& =\mbox{Var}_B \left\{   \left(  \frac{b}{\delta_T}  X_T-\frac{1}{2} \frac{b^2}{\delta^2_T}    \right)-
\left(         \frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}          \right)  \right\} \\
& =\mbox{Var}_B \left\{   \frac{b}{\delta_T}   \sqrt{1-r^2} W+ \left(  \frac{br}{\delta_T}  X_B   - \frac{b}{\delta_B}  X_B \right) +\left( \frac{1}{2} \frac{b^2}{\delta^2_B} -\frac{1}{2} \frac{b^2}{\delta^2_T} \right)  \right\}  \\
& =\mbox{Var}_B \left\{   \frac{b}{\delta_T}   \sqrt{1-r^2} W+ \left(  \frac{br}{\delta_T}  X_B   - \frac{b}{\delta_B}  X_B \right)  \right\}  
\end{align}
where the first term:
\begin{align}
 & \mbox{Var}_B \left\{   \frac{b}{\delta_T}   \sqrt{1-r^2} W \right\} \\
= & \mathbb{E}_B \left\{  \frac{b^2}{\delta_T^2}   (1-r^2)W^2   \right\} -  \mathbb{E}_B \left\{  \frac{b}{\delta_T}   \sqrt{1-r^2} W   \right\}^2  \\
=& \mathbb{E}_B \left\{  \frac{b^2}{\delta_T^2}   (1-r^2)W^2   \right\} \\
=& \mathbb{E } \left\{   \frac{b^2}{\delta_T^2}   (1-r^2)W^2  e^{\frac{b}{\delta_B}  X_B-\frac{1}{2} \frac{b^2}{\delta^2_B}}    \right\} \\
=& \frac{b^2}{\delta_T^2}   (1-r^2) .
\end{align}
The second term:
\begin{align}
& \mbox{Var}_B \left\{    \frac{br}{\delta_T}  X_B   - \frac{b}{\delta_B}  X_B \right\} \\
= & \left(     \frac{br}{\delta_T} -\frac{b}{\delta_B}\right)^2 
\end{align}
We can prove that $\mbox{Var}_B(X_B)=1$, which keeps unchanged under the change of measure.

Combining the two terms, we have:
\begin{align}
& \mbox{Var}_B \{ l_T-l_B    \} \\
&=\frac{b^2}{\delta_T^2}   (1-r^2) + \left(     \frac{br}{\delta_T} -\frac{b}{\delta_B}\right)^2 \\
&= -\frac{2b^2 r}{\delta_T\delta_B} +   \frac{b^2}{\delta^2_B}+  \frac{b^2}{\delta^2_T}\\
& =-2 \left[    \frac{b^2 r}{\delta_T \delta_B} -\frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}           \right] \\
&= -2 \mathbb{E}_B \{l_T-l_B\}
\end{align}
That is:
\begin{align}
\boxed{   \mbox{Var}_B \{ l_T-l_B    \} =     -2 \mathbb{E}_B \{l_T-l_B\}   }
\end{align}




\subsubsection{Approximate $   \mathbb{E}_B \{ l_T-l_B  \}$ in terms of the difference in block size}

Recall:
\begin{align}
\mathbb{E}_B \{ l_T-l_B    \} = \frac{b^2 r}{\delta_T \delta_B} -\frac{1}{2} \frac{b^2}{\delta^2_B}   -\frac{1}{2} \frac{b^2}{\delta^2_T}  
\end{align}

where
\begin{align}
& r=\frac{      \sqrt{   { \binom{T}{2} }     { \binom{B}{2} }    }                 }{    { \binom{B \vee T}{2} }      }   \\
& \delta_T =  \sqrt{   { \binom{T}{2} } ^{-1}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'} \right]  } \\
& \delta_B =  \sqrt{   { \binom{B}{2} } ^{-1}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'} \right]  }
\end{align}

Define $k$ as the difference in block size:
\begin{align}
k:=T-B
\end{align}

\begin{itemize}
\item Suppose $k>0$ \\
We can rewrite $\mathbb{E}_B\{l_T=l_B\}$ as:
\begin{align}
\mathbb{E}_B\{l_T-l_B\} &= \frac{b^2}{\delta_B} \left[  \frac{r}{\delta_T}-\frac{1}{\delta_B}    \right] +\frac{1}{2} \frac{b^2}{\delta^2_B} -\frac{1}{2} \frac{b^2}{\delta^2_T} \\
&=\frac{b^2}{\delta_B} \left[    \frac{      \sqrt{   { \binom{T}{2} }     { \binom{B}{2} }    }                 }{    { \binom{T}{2} }      }                 
\frac{\sqrt{ \binom{T}{2}    }}{\sqrt{   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   }}
-\frac{\sqrt{ \binom{B}{2}    }}{\sqrt{   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   }}
\right]
 +\frac{1}{2} \frac{b^2}{\delta^2_B} -\frac{1}{2} \frac{b^2}{\delta^2_T} \\
& =0+\frac{1}{2} \frac{b^2}{\delta^2_B} -\frac{1}{2} \frac{b^2}{\delta^2_T},
\end{align}
where $  \frac{1}{2} \frac{b^2}{\delta^2_B} -\frac{1}{2} \frac{b^2}{\delta^2_T}   $ can be approximated as (Taylor expansion):
\begin{align}
\frac{1}{2} \frac{b^2}{\delta^2_B} -\frac{1}{2} \frac{b^2}{\delta^2_T}  
\approx - \frac{  kb^2 (2B-1)}{4}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   \right]^{-1}
\end{align}
Therefore, for $k> 0$, we have:
\begin{align}
\mathbb{E}_B\{l_T=l_B\} \approx - \frac{  kb^2 (2B-1)}{4}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   \right]^{-1}
\end{align}
\item Suppose $k<0$ \\
We can rewrite $\mathbb{E}_B\{l_T=l_B\}$ as:
\begin{align}
\mathbb{E}_B\{l_T-l_B\} &= \frac{b^2}{\delta_T} \left[  \frac{r}{\delta_B}-\frac{1}{\delta_T}    \right] +\frac{1}{2} \frac{b^2}{\delta^2_T} -\frac{1}{2} \frac{b^2}{\delta^2_B} \\
&=\frac{b^2}{\delta_T} \left[    \frac{      \sqrt{   { \binom{T}{2} }     { \binom{B}{2} }    }                 }{    { \binom{B}{2} }      }                 
\frac{\sqrt{ \binom{B}{2}    }}{\sqrt{   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   }}
-\frac{\sqrt{ \binom{T}{2}    }}{\sqrt{   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   }}
\right]
 +\frac{1}{2} \frac{b^2}{\delta^2_T} -\frac{1}{2} \frac{b^2}{\delta^2_B} \\
& =0+\frac{1}{2} \frac{b^2}{\delta^2_T} -\frac{1}{2} \frac{b^2}{\delta^2_B},
\end{align}
where $  \frac{1}{2} \frac{b^2}{\delta^2_T} -\frac{1}{2} \frac{b^2}{\delta^2_B}   $ can be approximated as (Taylor expansion):
\begin{align}
\frac{1}{2} \frac{b^2}{\delta^2_T} -\frac{1}{2} \frac{b^2}{\delta^2_B}  
\approx  \frac{  kb^2 (2B-1)}{4}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   \right]^{-1}
\end{align}
Therefore, for $k< 0$, we have:
\begin{align}
\mathbb{E}_B\{l_T=l_B\} \approx  \frac{  kb^2 (2B-1)}{4}  \left[   \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}   \right]^{-1}
\end{align}
\end{itemize}

Finally, combining the two cases, we have:
\begin{align}
 \boxed{     \mathbb{E}_B \{ l_T-l_B    \} \approx -\frac{|k|   b^2   (2B-1) }{4} \left[  \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}        \right]^{-1}      } 
\end{align}
Given the relation between the mean and variance proven as before, we further have:
\begin{align}
 \boxed{     \mbox{Var}_B \{ l_T-l_B    \} \approx \frac{|k|   b^2   (2B-1) }{2} \left[  \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}        \right]^{-1}      } 
\end{align}
\subsubsection{From Local to Global}
Define:
\begin{align}
\mu^2:= \frac{  b^2   (2B-1) }{2} \left[  \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}        \right]^{-1}   
\end{align}
That is:
\begin{align}
\mu=b \sqrt{\frac{2B-1}{2}} \left[  \frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}        \right]^{-\frac{1}{2}}  
\end{align}
We have:
\begin{align}
\mathbb{E} \left(   \frac{\mathcal{M}}{\mathcal{S}}    \right) =\frac{\mu^2}{2} \nu(\mu)
\end{align}
The overshoot function $\nu$ can be computed as:
\begin{align}
\nu (\mu) \approx \frac{(2/\mu) (\Phi(\mu/2)-0.5) }{(\mu /2)\Phi(\mu/2)+ \phi (\mu/2)}
\end{align}
where $\Phi(x)$ is the cumulative distribution function of the standard normal distribution and $\phi$ is the probability density function of the standard normal distribution.

\begin{align}
\mbox{II} & =\mathbb{E}_B \left( \frac{M_B}{S_B}    e^{-\xi_B[Z_B-b+\frac{\mbox{log} M_B}{\xi_B}]}   ; Z_B-b+ \frac{\mbox{log} M_B}{\xi_B} \geq 0       \right)    \\
 & \approx \frac{\mu^2}{2} \nu(\mu) \frac{1}{\sqrt{2\pi \ddot{\psi}(\xi_B)}} =\frac{\mu^2   \nu(\mu)    }{2 \sqrt{2\pi \delta^2_B}} 
\end{align}

\subsubsection{Tail Probability}
In summary, we approximate the tail probability as:
\begin{align}
\mbox{Pr} \left( \max_{2 \leqslant B \leqslant m} Z_B >b       \right) 
& \approx   \sum_{2 \leqslant B \leqslant m} e^ {-\frac{1}{2}  \frac{b^2}{\delta^2_B}   }  \times 
\frac{b^2 (2B-1) \nu (   \frac{  b^2   (2B-1) }{2   \sqrt{C_n}}    )  }{4 C_n \sqrt{2\pi \delta^2_B}  } \\
& =  \sum_{2 \leqslant B \leqslant m} e^ {-\frac{b^2 B(B-1)}{4 C_n}   } \times 
\frac{b^2 (2B-1) \nu (   \frac{  b^2   (2B-1) }{2   \sqrt{C_n}}    )  }{8 C_n \sqrt{  \frac{\pi C_n}{B(B-1)}   }  }
\end{align}
where 
\begin{align}
&C_n :=\frac{1}{n}   \sigma^2_{xx'yy'} +\frac{n-1}{n} \sigma^2_{yy'}     
\end{align}
\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Approximation of r
Let $T=B+k$,
if $k>0$, then
\begin{align}
r=&  \sqrt{ \frac{B(B-1)}{(B+|k|)(B+|k|-1)}  } =\sqrt{\left(1+\frac{|k|}{B}   \right)^{-1}  \left(1+\frac{|k|}{B-1} \right)^{-1}   }    \\
  \approx & \sqrt{\left(1-\frac{|k|}{B}\right)\left(1-\frac{|k|}{B-1}\right)}
\end{align}
if $k<0$, then
\begin{align}
r=&\sqrt{ \frac{(B-|k|)(B-|k|-1)}{B(B-1)}  } =\sqrt{\left(1-\frac{|k|}{B}\right)\left(1-\frac{|k|}{B-1}\right)} \\
\end{align}
Thus, we can approximate:
\begin{align}
r\approx \sqrt{\left(1-\frac{|k|}{B}\right)\left(1-\frac{|k|}{B-1}\right)} \approx 1-\frac{1}{2} \frac{2B-1}{B(B-1)} |k|
\end{align}


