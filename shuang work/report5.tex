\documentclass[11 pt]{article}
\usepackage[top=0.8 in, bottom=0.8  in, right=0.8in, left=0.8 in]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsfonts}
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{amssymb, amsmath}% Use packages about maths
\usepackage{amsthm}
\usepackage[english]{babel}
\usepackage{enumerate,bbm}
\usepackage{geometry}
%\usepackage{enumerate}

%\newenvironment{proof}{\noindent {\sc Proof:}}{$\Box$ \medskip}
%\renewcommand{\labelenumi}{(\alph{enumi})}
%\newcommand{\prob}[1]{\mathbf{Pr}(#1)}
%\newcommand{\lcm}{\mbox{lcm}}
%\newcommand{\reals}{\mathbb{R}}
%\newcommand{\tih}{\tilde{H}}
%\newcommand{\ex}{\langle\mbox{EXPR}\rangle}
%\newcommand{\ter}{\langle\mbox{TERM}\rangle}
%\newcommand{\fac}{\langle\mbox{FACTOR}\rangle}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\usepackage{hyperref}
\usepackage{color}
\usepackage{natbib}
\usepackage{amsmath}
\usepackage{indentfirst}
\renewcommand{\baselinestretch}{1.3}
\usepackage{float}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem*{remark}{Remark}

\begin{document}

\section*{Background}
Assume observations (new)
\begin{align*}
& X:=\{x_1, x_2, \dots, x_m\} \stackrel {iid} {\sim} P,\\
& Y:=\{y_1, y_2, \dots, y_m\} \stackrel{iid}{\sim} Q.
\end{align*}
Let $\mathcal{F}$ be a class of functions $f: \mathcal{X} \to \mathbb{R}$ and define the \textit{maximum mean discrepancy} (MMD) as 
\begin{align}
\mbox{MMD} [\mathcal{F}, P, Q] := \sup_{f\in \mathcal{F}} \left( \mathbb{E}_x [f(x)] -\mathbb{E}_y [f(y)]\right).
\end{align}
In the reproducing kernel Hilbert space $\mathcal{H}$, the $\mbox{MMD} $ can be expressed as the distance between mean embeddings in $\mathcal{H}$, and furthermore can be expressed in terms of kernel functions. That is
\begin{align*}
\mbox{MMD}^2 [\mathcal{F}, P, Q ] & =\left[  \sup_{\| f \|_{\mathcal{H} \leqslant 1}     }     ( \mathbb{E}_x [f(x)] -\mathbb{E}_y [f(y)]    )    \right]^2 \\
& = \left[  \sup_{\|f\|_{\mathcal{H} \leqslant 1}}    \langle \mu_P -\mu_Q, f \rangle_{\mathcal{H}}  \right]^2 \\
&=\| \mu_P -\mu_Q  \|^2_{\mathcal{H}} \\
&=\langle \mu_P,\mu_P  \rangle_{\mathcal{H}} + \langle \mu_Q,\mu_Q  \rangle_{\mathcal{H}}  -2 \langle \mu_P,\mu_Q \rangle_{\mathcal{H}}  \\
&= \mathbb{E}_{x,x'} \langle  \phi(x), \phi(x') \rangle_{\mathcal{H}} +\mathbb{E}_{y,y'} \langle  \phi(y), \phi(y') \rangle_{\mathcal{H}} -2 \mathbb{E}_{x,y} \langle  \phi(x), \phi(y) \rangle_{\mathcal{H}} \\
&= \mathbb{E}_{x,x'} \kappa(x,x')+ \mathbb{E}_{y,y'} \kappa(y,y')-2 \mathbb{E}_{x,y} \kappa(x,y)
\end{align*}
Next, let $\mbox{MMD}_u^2$ be the unbiased estimate of $\mbox{MMD}^2 [\mathcal{F}, P, Q ]$ by replacing the population expectations with their corresponding U-statistics and sample averages:
\begin{align}
\mbox{MMD}_u^2 =\frac{1}{m(m-1)} \sum_{i,j=1, i \neq j}^m h(z_i,z_j),
\end{align}
where $z^i :=(x_i,y_i)$ and $h(z_i,z_j)=\kappa(x_i,x_j)+\kappa(y_i,y_j)-\kappa(x_i,y_j)-\kappa (x_j,y_i).$\\

Intuitively, the empirical test statistic $\mbox{MMD}_u^2$ is expected to be small (close to zero) if $P=Q$, and large if $P$ and $Q$ are far apart. And it will cost $O(m^2)$ time to compute the statistic, given both sample sizes of $\{x_i\}$ and $\{y_i\}$ are $m$.\\

Under $H_0 \, (P=Q)$, the U-statistic is degenerate, which means $\mathbb{E}_{z'} h(z,z')=0$. In this case, $\mbox{MMD}_u^2$ converges in distribution according to 
\begin{align}
m\mbox{MMD}^2_u \stackrel{D} \to \sum_{l=1}^{\infty} \lambda_{l} [z_l^2 -2],
\end{align}
where $z_l \sim N(0,2)$ and $\lambda_i$ are the solutions to the eigenvalue equation
\begin{align}
\int_{\mathcal{X}} \hat{\kappa} (x,x') \phi_i (x) d \mbox{Pr} (x)= \lambda_i \phi_i (x'),
\end{align}
and $\hat{\kappa} (x_i, x_j) := \kappa (x_i, x_j) -\mathbb{E}_x \kappa(x_i, x)-\mathbb{E}_x \kappa (x,x_j) +\mathbb{E}_{x,x'} \kappa (x,x')$ is the centered RKHS kernel.\\

The main idea of B-test is to split the data into blocks of size B, compute the quadratic-time $\mbox{MMD}_u^2$ on each block, and then average the resulting statistics.

\newpage
\section*{B test}
Given observations $D_t=\{x_1,x_2,\dots, x_t\}$, which will be split into $(n+1)$ blocks, each with size $B$, we compute the B-test statistic, defined as $Z_B$:
\begin{align}
Z_B: & =\frac{1}{n} \sum_{i=1}^n \mbox{MMD}_i^2(B) \\
       &= \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2 (X_i,Y; B) \\
       &=  \frac{1}{n} \sum_{i=1}^n \frac{1}{B(B-1)} \sum_{j,l=1, j \neq l}^B h( x_i^j, x_i^l, y^j, y^l)   \\
       & =\frac{1}{n}\sum_{i=1}^n \frac{1}{B(B-1)} \sum_{j,l=1, j \neq l}^B 
\kappa(x_i^j, x_i^l) +\kappa(y^j,y^l)-\kappa(x_i^j,y^l)-\kappa (x_i^l,y^j)    
\end{align}
Under null, we have $\mathbb{E} (Z_B)=0$. 

\begin{definition}
The normalized scan statistic is defined as:
\begin{align}
\max_{B \in \{2,3,\dots, m\}} \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} 
\end{align}
We would like to study the tail probability given threshold $b$:
\begin{align}
\mbox{Pr} \left(  \max_{  B \in \{2,3,\dots, m\} } \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} > b \right) 
\end{align}
\end{definition}



 \newpage
 \section*{Estimate $\mbox{Var} (Z_B)$ under Null Distribution }
 In this section, we discuss how to estimate $\mbox{Var} (Z_B)$ given any block size $B$.
 \begin{lemma}
We split reference data into $n$ blocks each with size $B$, and define $i$ as the block index for reference data. Under null hypothesis, for any $i=1,2,\dots, n$, we have
\begin{align}
\mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right)={\binom{B}{2}}^{-1}\mathbb{E} [h^2(x_i,x'_i,y,y')].
\end{align}
 \end{lemma}
 \begin{proof}
 For any $i=1,2,\dots, n$, by definition of U-statistic, we have
\begin{align}
 \mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right) 
 &= \mbox{Var} \left(     {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  )  \right)   \nonumber \\
 &=   {\binom{B}{2} }^{-2}  \left[  \binom{B}{2} \binom{2}{1} \binom{B-2}{2-1}    \mbox{Var} \left(     \mathbb{E}_{x_iy} [h(x_i,x'_i,y,y')]  \right)
  +                             \binom{B}{2} \binom{2}{2} \binom{B-2}{2-2}  \mbox{Var}  \left(  h(x_i,x'_i,y,y')   \right)
    \right],   \nonumber 
\end{align}
where under null distribution, we have $\mathbb{E}_{x_i y} [h(x_i,x'_i,y,y')]=0$. Thus,
\begin{align}
& \mbox{Var} \left(     \mathbb{E}_{x_iy} [h(x_i,x'_i,y,y')]  \right)=\mbox{Var}(0)=0 ,  \nonumber   \\
& \mbox{Var}  \left(  h(x_i,x'_i,y,y')   \right)=\mathbb{E} [h^2(x_i,x'_i,y,y')]-  \mathbb{E} [h(x_i, x'_i,y,y')]^2 
        =\mathbb{E} [h^2(x_i,x'_i,y,y')].  \nonumber 
\end{align}
Here, $  \mathbb{E} [h^2(x_i,x'_i,y,y')]$ can be estimated from the data. Furthermore, since under null hypothesis all data are assumed to independently follow the same distribution , we can safely drop the block index. In summary, we have:
\begin{align}
\mbox{Var} \left(  \mbox{MMD}_i^2(B)  \right)={\binom{B}{2}}^{-1}\mathbb{E} [h^2(x,x',y,y')], \nonumber 
\end{align}
where under null hypothesis, $x,\, x',\, y\,$ and $y'$ are from the same null distribution.
 \end{proof}
 
 
 \begin{lemma}
 For any distinct blocks $i$ and $i+s$, under null hypothesis we have
 \begin{align}
\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)
={\binom{B}{2}}^{-1}\mbox{Cov} \left(  h(x_i,x'_i,y,y'),h(x_{i+s},x'_{i+s}, y, y')   \right).
\end{align}
 \end{lemma}
 \begin{proof}
 For $i=1,2,\dots, n$, and $s=(1-i), (2-i), \dots, (n-i),   s \neq 0$,
\begin{align*}
   & \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right) \\
= & \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B}{2} }^{-1}  \sum_{p < q}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
=&  {\binom{B}{2} }^{-2}      \binom{B}{2} \binom{2}{1} \binom{B-2}{2-1} \mbox{Cov} \left(  h(x_i^l,x_i^j,y,y^j),h(x_{i+s}^p,x_{i+s}^q, y, y^q)   \right) \\
&  +          {\binom{B}{2} }^{-2}                     \binom{B}{2} \binom{2}{2} \binom{B-2}{2-2} \mbox{Cov} \left(  h(x_i^l,x_i^j,y,y'),h(x_{i+s}^p,x_{i+s}^q, y, y')   \right) ,
\end{align*}
where under null distribution,
\begin{align*}
& \mbox{Cov} \left(  h(x_i^l,x_i^j,y,y^j),h(x_{i+s}^p,x_{i+s}^q, y, y^q)   \right)\\ 
 =& \int \mbox{Pr}( x_i^l,x_i^j,y,y^j,x_{i+s}^p,x_{i+s}^q, y, y^q  )  h(x_i^l,x_i^j,y,y^j) h(x_{i+s}^p,x_{i+s}^q, y, y^q) \\
 =& \int \mbox{Pr}(x_i^l,y) \mbox{Pr}(x_{i+s}^p,y)  \int \mbox{Pr} (x_i^j,y^j)  h(x_i^l,x_i^j,y,y^j) \int \mbox{Pr} (x_{i+s}^q, y^q) h(x_{i+s}^p,x_{i+s}^q, y, y^q) \\
 =& 0
\end{align*}
Finally, we have:
\begin{align*}
\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)
={\binom{B}{2}}^{-1} \mbox{Cov} \left(  h(x_i,x'_i,y,y'),h(x_{i+s},x'_{i+s}, y, y')   \right).
\end{align*}
Under null hypothesis, $x_i,\, x'_i,\, x_{i+s},\, x'_{i+s},\, y\,$ and $y'$ independently follow the same null distribution. Furthermore, we drop the block index and obtain:
\begin{align*}
\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B)    \right)
={\binom{B}{2}}^{-1}\mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right).
\end{align*}
 \end{proof}

\begin{theorem}
Given any fixed block size $B$, under null hypothesis we can estimate $\mbox{Var} (Z_B)$ by:
\begin{align}
\mbox{Var}(Z_B)={\binom{B}{2}}^{-1}\left[   \frac{1}{n} \mathbb{E} [h^2(x, x',y,y')]  +\frac{n-1}{n} \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right)  \right],
\end{align}
where $x,\, x',\,x'',\,x''',\, y,$ and $y'$ independently follow the same null distribution.
\end{theorem}
\begin{proof}
\begin{align*}
\mbox{Var}(Z_B) &=\mbox{Var} \left( \frac{1}{n} \sum_{i=1}^n \mbox{MMD}_i^2(B) \right) \\
  &=\frac{1}{n^2}\left[ n \mbox{Var}(\mbox{MMD}_i^2(B)) +\sum_{i \neq j} \mbox{Cov} \left(  \mbox{MMD}_i^2(B), \mbox{MMD}_j^2(B)   \right)  \right] \\
  &= \frac{1}{n}  {\binom{B}{2}}^{-1}\mathbb{E} [h^2(x_i,x'_i,y,y')]         +  \frac{1}{n^2}\sum_{i \neq j}  {\binom{B}{2}}^{-1}\mbox{Cov} \left(  h(x_i,x'_i,y,y'),h(x_{j},x'_{j}, y, y')   \right)
  \end{align*}
Plugging in Lemma 1 and Lemma 2, we get:
  \begin{align*}
\mbox{Var}(Z_B) = {\binom{B}{2}}^{-1}\left[   \frac{1}{n} \mathbb{E} [h^2(x, x',y,y')]  +\frac{n-1}{n} \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right)  \right]
\end{align*}
\end{proof}
 
 
 
 \newpage
 \section*{Compute $r:=\mbox{Cov} \left(  \frac{Z_B}{\sqrt{Var(Z_B)}}, \frac{Z_{B+k}}{\sqrt{Var(Z_{B+k})}}\right)$}
 In this section, we add perturbation $k$ ($k=\pm 1,\,\pm 2,\,\dots$) to the block size $B$, and compute the covariance of statistic under different block sizes.
 
 \begin{lemma}
 For blocks with the same index $i$ but with distinct block sizes, under null hypothesis we have the covariance:
 \begin{align}
 \mbox{Cov} \left( \mbox{MMD}_i^2(B), \mbox{MMD}_i^2(B+k) \right)={ \binom {B \vee (B+k)} {2} }^{-1} \mathbb{E} [h^2(x, x',y,y')] 
 \end{align}
 \end{lemma}
 \begin{proof}
 \begin{align*}
   & \mbox{Cov} \left( \mbox{MMD}_i^2(B), \mbox{MMD}_i^2(B+k) \right) \\
   =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B+k}{2} }^{-1}  \sum_{p < q}^{B+k}  h(x_{i}^p, x_{i}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}     \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B+k}  h(x_{i}^p, x_{i}^q, y^p, y^q  )           \right)  \\
    =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}    \binom{B \wedge (B+k)}{2}   \mbox{Var} ( h(x_i,x'_i,y,y'))  \\
   =&  {\binom{B \vee (B+k)}{2} }^{-1}  \mathbb{E} [h^2(x,x',y,y')]
 \end{align*}
 \end{proof}
 
 \begin{lemma}
For blocks with distinct index as well as distinct block sizes, under null hypothesis we have the covariance:
\begin{align}
\mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right) ={\binom{B \vee (B+k)}{2} }^{-1}  \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right) 
\end{align}
 \end{lemma}
 \begin{proof}
 \begin{align*}
   &  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{ MMD}_{i+s}^2 (B+k)    \right)  \\
  =&  \mbox{Cov}   \left(    {\binom{B}{2} }^{-1}  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),   {\binom{B+k}{2} }^{-1}  \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
   =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}     \mbox{Cov}   \left(  \sum_{l < j}^{B}  h(x_i^l, x_i^j, y^l, y^j  ),     \sum_{p < q}^{B+k}  h(x_{i+s}^p, x_{i+s}^q, y^p, y^q  )           \right)  \\
      =&  {\binom{B}{2} }^{-1}   {\binom{B+k}{2} }^{-1}   \binom{B \wedge (B+k)}{2}    \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right)    \\
   =&  {\binom{B \vee (B+k)}{2} }^{-1}  \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right) 
  \end{align*}
 \end{proof}
 
 \begin{theorem}
 Under null hypothesis, we have:
 \begin{align}
 r:=  \mbox{Cov} \left(  \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}, \frac{Z_{B+k}}{\sqrt{\mbox{Var}(Z_{B+k})}}\right) =\frac{\sqrt{{\binom{B}{2}}{ \binom{B+k}{2}  }}}{\binom{B \vee (B+k) }{2}}
 \end{align}
 \end{theorem}
 \begin{proof}
 \begin{align*}
r:=  \mbox{Cov} \left(  \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}, \frac{Z_{B+k}}{\sqrt{\mbox{Var}(Z_{B+k})}}\right)=\frac{1}{\sqrt{\mbox{Var}(Z_B)}} \frac{1}{\sqrt{\mbox{Var}(Z_{B+k})}} \mbox{Cov} \left(  Z_B, Z_{B+k}  \right),
 \end{align*}
 where \begin{align*}
 \mbox{Cov} \left(  Z_B, Z_{B+k}  \right) & = \mbox{Cov} \left(  \frac{1}{n} \sum_{i=1}^n \mbox{MMD}^2_i(B), \frac{1}{n} \sum_{j=1}^n \mbox{MMD}^2_{j} (B+k)   \right) \\
&=\frac{1}{n^2} \left[n \mbox{Cov} \left(  \mbox{MMD}_i^2(B), \mbox{MMD}_i^2(B+k)      \right)    +\sum_{i \neq j}  \mbox{Cov} \left(   \mbox{MMD}_i^2(B), \mbox{MMD}_j^2(B+k)    \right)      \right]   
\end{align*}
Plugging in Lemma 3 and 4, we have:
\begin{align*}
 \mbox{Cov} \left(  Z_B, Z_{B+k}  \right)&  =\frac{1}{n}    {\binom{B \vee (B+k)}{2} }^{-1}  \mathbb{E} [h^2(x,x',y,y')]   \\
&   +\frac{n-1}{n} {\binom{B \vee (B+k)}{2} }^{-1}  \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right) \\
&={\binom{B \vee (B+k)}{2} }^{-1} \left[ \frac{1}{n} \mathbb{E} [h^2(x,x',y,y')]+\frac{n-1}{n}   \mbox{Cov} \left(  h(x,x',y,y'),h(x'',x''', y, y')   \right)\right]
\end{align*}
Finally, plugging in the expressions for $\mbox{Var} (Z_B)$ and $\mbox{Var}(B+k)$, we have:
\begin{align*}
r:=\mbox{Cov} \left(  \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}, \frac{Z_{B+k}}{\sqrt{\mbox{Var}(Z_{B+k})}}\right)=\frac{\sqrt{{\binom{B}{2}}{ \binom{B+k}{2}  }}}{\binom{B \vee (B+k) }{2}}
\end{align*}
 \end{proof}
 (Note: Add more intuitive explanation of correlation/ covariance here. For example, when $k=0$, r =1. Otherwise, as the absolute value of $k$ increases, r decreases and is getting closer to 0.)


\newpage
\section*{Asymptotic Tail Probability}
 We would like to study $\mbox{Pr} \left(  \max_{  2 \leqslant   B \leqslant m  } \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} > b \right) $ under null hypothesis.
\begin{align*}
&  \mbox{Pr} \left( \max_{2 \leqslant B \leqslant m}     \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} >b       \right) \\
=& \mathbb{E} \left(  \frac{\sum_{B}   e^{\xi   \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} -\psi(\xi)}   }{\sum_l   e^{\xi \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }}  -\psi(\xi)}}   ; \max_{2 \leqslant B\leqslant m}  \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}  >b    \right) \\
=& \sum_{2 \leqslant B \leqslant m} \mathbb{E}  \left(     \frac{e^{\xi   \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} -\psi(\xi)}   }{\sum_l   e^{\xi   \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }} -\psi(\xi)}}   ; \max_{2 \leqslant B\leqslant m} \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}  >b       \right) \\
=& \sum_{2 \leqslant B \leqslant m} \mathbb{E}_B \left(     \frac{1  }{\sum_l   e^{\xi \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }} -\psi(\xi)}}   ; \max_{2 \leqslant B\leqslant m}  \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} >b       \right)\\
=& \sum_{2 \leqslant B \leqslant m}  e^{\psi(\xi_B) -\xi_B b}   \mathbb{E}_B  \Bigg(    \frac{e^{\xi_B \max_l \left[  \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }} -   \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}  \right]} e^{-\xi_B \left[  \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} -b+\max_l \left[   \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }} -  \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}  \right] \right]}  }{\sum_l   e^{\xi  \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }} -\psi(\xi)}}   ; \\
& \qquad  \qquad  \qquad \qquad \qquad  \qquad  \qquad  \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }} -b+ \max_l \left[  \frac{ Z_l  }  {\sqrt{\mbox{Var} (Z_l)  }} -   \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}  \right] \geq 0       \Bigg)\\
= & \sum_{2 \leqslant B  \leqslant m}  \underbrace{ e^{\psi(\xi_B) -\xi_B b} }_{\mbox{I}}  
   \underbrace{  \mathbb{E}_B \left( \frac{M_B}{S_B}    e^{-\xi_B \left[ \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}   -b+\frac{\mbox{log} M_B}{\xi_B}\right]}   ;  \frac{ Z_B  }  {\sqrt{\mbox{Var} (Z_B)  }}   -b+ \frac{\mbox{log} M_B}{\xi_B} \geq 0       \right)    }_{\mbox{II}} 
\end{align*}
where 
\begin{align*}
&\psi(\xi_B):= \mbox{log} \mathbb{E}   \left[ e^{\xi  \frac{ Z_B  }  {   \sqrt{\mbox{Var} (Z_B)  }}    }  \right] \\
& M_B :=   \\
& S_B :=  
\end{align*}
(Note: there are some mistakes in the derivation. I need to return this part later. Add some background about Gaussian random field.)
\subsubsection{Gaussian Approximation}
 First, under null hypothesis we approximate the distribution of $Z_B$ by Gaussian with the same mean and variiance. In this way, $\frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}$ would follow standard normal, and furthermore we have:
 \begin{align}
 \psi(\xi_B):= \mbox{log} \mathbb{E}   \left[ e^{\xi  \frac{ Z_B  }  {   \sqrt{\mbox{Var} (Z_B)  }}    }  \right] =\frac{1}{2} \xi^2
 \end{align}

\subsubsection{Large Deviation}
Next, we compute $\mbox{I}$, the large deviation component. \\
We need to compute the function:
\begin{align}
\dot {\psi} (\xi_B) =b  \Rightarrow \xi_B= b
\end{align}
 Therefore,
\begin{align}
\psi(\xi_B)=\frac{1}{2} b^2
\end{align}
And 
\begin{align}
  \boxed{    \mbox{I}: =e^{\psi(\xi_B) -\xi_B b} =e^ {-\frac{1}{2}  b^2 }.  }
\end{align}

\subsubsection{Local Field}
 We analyze the local field $\{l_{B+k}-l_B\}$, where
\begin{align}
& l_{B+k} :=\xi_{B+k} \frac{Z_{B+k}}{\sqrt{\mbox{Var}(Z_{B+k})}}-\psi(\xi_{B+k}) \\
& l_B :=\xi_B \frac{Z_B}{\sqrt{\mbox{Var}{Z_B}}}-\psi(\xi_B) 
\end{align}
Using Gaussian approximation, we can write:
\begin{align*}
& l_{B+k} :=\frac{b Z_{B+k}}{ \sqrt{\mbox{Var}{Z_{B+k}}}}  -\frac{1}{2} b^2\\
& l_B :=\frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2
\end{align*}
Then,
\begin{align*}
\mathbb{E}_B \{ l_{B+k}-l_B    \} 
& =\mathbb{E}_B \left\{   \left(  \frac{b Z_{B+k}}{ \sqrt{\mbox{Var}{Z_{B+k}}}}  -\frac{1}{2} b^2     \right)-
\left(   \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2              \right)  \right\} \\
&=\mathbb{E}  \left\{  \left(     \frac{b Z_{B+k}}{ \sqrt{\mbox{Var}{Z_{B+k}}}} -    \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}   \right) e^{  \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2   } \right\} \\
&=\mathbb{E}  \left\{  \left( b \left[   \sqrt{1-r^2} W+r   \frac{Z_B}{\sqrt{\mbox{Var}{Z_B}}}    \right]- \frac{bZ_B}{\sqrt{\mbox{Var}{Z_B}}}       \right) e^{    \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2   } \right\} . 
\end{align*}
The above representation results from the regression of $     \frac{Z_{B+k}}{ \sqrt{\mbox{Var}{Z_{B+k}}}} $ on $ \frac{ Z_B}{\sqrt{\mbox{Var}(Z_B)}}   $ with $W$ being the standardized residual of the regression. We furthermore have:
\begin{align*}
\mathbb{E}_B \{ l_{B+k}-l_B    \} =-b(1-r) \mathbb{E} \left\{  \frac{Z_B}{\sqrt{\mbox{Var}{Z_B}}}  e^{    \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2   }     \right\}=-b^2(1-r),
\end{align*}
where under the Gaussian assumption we can easily verify that:
\begin{align*}
\mathbb{E} \left\{  \frac{Z_B}{\sqrt{\mbox{Var}{Z_B}}}  e^{    \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2   }     \right\}=b
\end{align*}

Similarly, 
\begin{align*}
\mbox{Var}_B \{ l_{B+k}-l_B    \} 
& =\mbox{Var}_B \left\{     \left(  \frac{b Z_{B+k}}{ \sqrt{\mbox{Var}{Z_{B+k}}}}  -\frac{1}{2} b^2     \right)-
\left(   \frac{b Z_B}{\sqrt{\mbox{Var}(Z_B)}}  -\frac{1}{2} b^2      \right)    \right\} \\
&=\mbox{Var}_B \left\{    b \left[   \sqrt{1-r^2} W+r   \frac{Z_B}{\sqrt{\mbox{Var}{Z_B}}}    \right]- \frac{bZ_B}{\sqrt{\mbox{Var}{Z_B}}}         \right\} \\
&=\mbox{Var}_B \left\{  b\sqrt{1-r^2} W     \right\} 
+ \mbox{Var}_B  \left\{    b(r-1)   \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}      \right\}                    \\
&=\mathbb{E}_B \{ b^2 (1-r^2) W^2\}-\mathbb{E}_B\{ b \sqrt{1-r^2} W \}^2 \\
&\qquad +\mathbb{E}_B \left\{   b^2(r-1)^2 \left(
 \frac{Z_B}{\sqrt{\mbox{Var}{Z_B}}}    \right)^2 \right\}- \mathbb{E}_B \left\{    b(r-1)   \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}       \right\}^2 \\
&=b^2(1-r^2)+b^2(r-1)^2 \\
&=2b^2(1-r)
\end{align*}
(Some detailed computation has been omitted here. See it in old version notes.)

Hence, we have:
\begin{align}
\boxed{   \mbox{Var}_B \{ l_{B+k}-l_B    \} =     -2 \mathbb{E}_B \{l_{B+k}-l_B\}   }
\end{align}

\subsubsection{Approximate $   \mathbb{E}_B \{ l_T-l_B  \}$ in terms of the difference in block size}
\begin{align*}
\mathbb{E}_B \{ l_{B+k}-l_B\}=-b^2(1-r)
\end{align*}

And as discussed before, $r$ is:
\begin{align}
r& :=\mbox{Cov} \left(    \frac{ Z_B}{\sqrt{\mbox{Var}(Z_B)}} , \frac{Z_{B+k}}{ \sqrt{\mbox{Var}{Z_{B+k}}}}   \right)=\frac{\sqrt{{\binom{B}{2}}{ \binom{B+k}{2}  }}}{\binom{B \vee (B+k) }{2}}
\end{align}

Suppose $k>0$, then
\begin{align}
r=&  \sqrt{ \frac{B(B-1)}{(B+k)(B+k-1)}  } =\sqrt{\left(1+\frac{k}{B}   \right)^{-1}  \left(1+\frac{k}{B-1} \right)^{-1}   }    
  \approx & \sqrt{\left(1-\frac{k}{B}\right)\left(1-\frac{k}{B-1}\right)}
\end{align}

Suppose $k<0$, then
\begin{align}
r=&\sqrt{ \frac{(B+k)(B+k-1)}{B(B-1)}  } =\sqrt{\left(1+\frac{k}{B}\right)\left(1+\frac{k}{B-1}\right)} \\
\end{align}

Finally, we can approximate:
\begin{align}
r\approx \sqrt{\left(1-\frac{|k|}{B}\right)\left(1-\frac{|k|}{B-1}\right)} \approx 1-\frac{1}{2} \frac{2B-1}{B(B-1)} |k| +o(|k|)
\end{align}

Then
\begin{align}
\mathbb{E}_B \{ l_{B+k}-l_B    \} \approx -\frac{b^2}{2} \frac{2B-1}{B(B-1)} |k|
\end{align}

Given the relation between the mean and variance which has been proved, we further have:
\begin{align}
  \mbox{Var}_B \{ l_{B+k}-l_B    \} \approx b^2 \frac{2B-1}{B(B-1)}|k|
\end{align}

\subsubsection{From Local to Global}
Define:
\begin{align}
\mu^2:=   b^2 \frac{2B-1}{B(B-1)}
\end{align}

We have:
\begin{align}
\mathbb{E} \left(   \frac{\mathcal{M}}{\mathcal{S}}    \right) =\frac{\mu^2}{2} \nu(\mu)
\end{align}
The overshoot function $\nu$ can be computed as:
\begin{align}
\nu (\mu) \approx \frac{(2/\mu) (\Phi(\mu/2)-0.5) }{(\mu /2)\Phi(\mu/2)+ \phi (\mu/2)}
\end{align}
where $\Phi(x)$ is the cumulative distribution function of the standard normal distribution and $\phi$ is the probability density function of the standard normal distribution.

\begin{align}
\mbox{II} & =\mathbb{E}_B \left( \frac{M_B}{S_B}    e^{-\xi_B \left[  \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}} -b+\frac{\mbox{log} M_B}{\xi_B} \right]}   ;   \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}}   -b+ \frac{\mbox{log} M_B}{\xi_B} \geq 0       \right)    \\
 & \approx \frac{\mu^2}{2} \nu(\mu) \frac{1}{\sqrt{2\pi \ddot{\psi}(\xi_B)}} =\frac{\mu^2   \nu(\mu)    }{2 \sqrt{2\pi }} 
\end{align}

\subsubsection{Tail Probability}
In summary, we approximate the tail probability as:
\begin{align}
\mbox{Pr} \left( \max_{2 \leqslant B \leqslant m}    \frac{Z_B}{\sqrt{\mbox{Var}(Z_B)}} >b       \right) 
& \approx   \sum_{2 \leqslant B \leqslant m} e^ {-\frac{1}{2}  b^2 }  \times 
\frac{b^2 (2B-1)\nu \left(  b \sqrt{\frac{2B-1}{B(B-1)}} \right) }{2B(B-1)\sqrt{2 \pi} }
\end{align}

\newpage
\section*{Evaluation Tail Probability}
\subsection{Tuning the maximum block size }
Compare the approximated tail probability with the empirical distribution of statistic obtained from 5000 samples.
\begin{enumerate}
\item $m=10$
\begin{center}
  \begin{tabular}{c|c|c}
  \hline
  $\alpha$ & $b$: Sampling & $b$: Formula \\ \hline
  0.20   & 1.7854 & 1.9944 \\ \hline
  0.15 & 2.0177 & 2.1800 \\ \hline
  0.10 & 2.2904 & 2.4018 \\ \hline
  0.05 & 2.7258 & 2.7198 \\ \hline
  0.01 & 3.7437 & 3.3080 \\
  \hline
  \end{tabular}
\end{center}
\begin{figure}[H]
  \caption{m=10}
  \centering
    \includegraphics[trim = 20mm 70mm 20mm 70mm, clip, width=0.5\textwidth]{fig_sampling_formula_m10}
\end{figure}




\item $m=20$
\begin{center}
  \begin{tabular}{c|c|c}
  \hline
  $\alpha$ & $b$: Sampling & $b$: Formula \\ \hline
  0.20   & 1.9697  & 2.2492 \\ \hline
  0.15 & 2.1767& 2.4077 \\ \hline
  0.10 & 2.4699 & 2.6058 \\ \hline
  0.05 & 2.8792 & 2.8999 \\ \hline
  0.01 & 3.6840 & 3.4605 \\
  \hline
  \end{tabular}
\end{center}


\begin{figure}[H]
  \caption{m=20}
  \centering
    \includegraphics[trim = 20mm 70mm 20mm 70mm, clip, width=0.5\textwidth]{fig_sampling_formula_m20}
\end{figure}



\item $m=50$
\begin{center}
  \begin{tabular}{c|c|c}
  \hline
  $\alpha$ & $b$: Sampling & $b$: Formula \\ \hline
  0.20   & 2.2114  & 2.4826 \\ \hline
  0.15 & 2.4425 & 2.6236 \\ \hline
  0.10 & 2.7007 & 2.8044 \\ \hline
  0.05 & 3.1471 & 3.0794 \\ \hline
  0.01 & 4.0769  & 3.6155 \\
  \hline
  \end{tabular}
\end{center}

\begin{figure}[H]
  \caption{m=50}
  \centering
    \includegraphics[trim = 20mm 70mm 20mm 70mm, clip, width=0.5\textwidth]{fig_sampling_formula_m50}
\end{figure}


\end{enumerate}

\newpage

\section*{Sequential Analysis}
We would like to approximate:
\begin{align}
\mbox{Pr} \left( \max_{ \begin{subarray}{c} B \leqslant t \leqslant T \\ k=t-B \end{subarray}}  \frac{Z_{k}^t(B) }{\sqrt{\mbox{Var} \left( Z_{k}^t (B)\right)}}>b \right)
\end{align}
According to previous analysis, we have:
\begin{align}
& \mbox{Var}( Z_{k}^t)=\mbox{Var} (Z_{k+s}^{t+s} )={ \binom{B}{2} }^{-1} \left[  \frac{1}{n} \mbox{Var}(h(x,x',y,y'))+\frac{n-1}{n} \mbox{Cov} (h(x,x',y,y'),h(x'',x''',y,y'))  \right] 
\end{align}
We would like to analyze:
\begin{align}
r:=\mbox{Cov} \left(     \frac{Z_{k}^t }{ \sqrt{ \mbox{Var} \left(  Z_{k}^t  \right)  } }, \frac{Z_{k+s}^{t+s}}{  \sqrt{  \mbox{Var}\left(  Z_{k+s}^{t+s}   \right)  }  } \right)  
\end{align}
Without loss of generality, we assume $s >0$. \\
\begin{align}
&  \mbox{Cov} \left( \mbox{MMD}_i^2(D_t; B), \mbox{MMD}_i^2 (D_{t+s}; B) \right) \\
= & {\binom{B}{2} }^{-2}   \binom{B-s}{2}  \mbox{Var} (h(x,x',y,y'))      
\end{align}
\begin{align}
&  \mbox{Cov} \left( \mbox{MMD}_i^2(D_t; B), \mbox{MMD}_j^2 (D_{t+s}; B) \right) \\
= & {\binom{B}{2} }^{-2} \binom{B-s}{2}    \mbox{Cov} (h(x,x',y,y'), h(x'',x''',y,y'))     \\
\end{align}
Thus, 
\begin{align}
\mbox{Cov} \left( Z_{k}^t, Z_{k+s}^{t+s}  \right) 
&= \mbox{Cov} \left(   \frac{1}{n} \sum_{i=1}^n \mbox{MMD}_i^2 (D_t; B) , \frac{1}{n} \sum_{j=1}^n \mbox{MMD}_j^2(D_{t+s}; B+k+s)  \right) \\
&=   {\binom{B}{2} }^{-2} \binom{B-s}{2}   \left[  \frac{1}{n} \mbox{Var}(h(x,x',y,y'))+\frac{n-1}{n} \mbox{Cov} (h(x,x',y,y'),h(x'',x''',y,y'))  \right] 
\end{align}
We have:
\begin{align}
r= \frac{   \binom{B-s}{2} } { \binom{B}{2} }  = \left(1-\frac{s}{B} \right)  \left(  1-\frac{s}{B-1} \right)    \approx 1-\frac{2B-1}{B(B-1)} s
\end{align}
In the following description, we write $Z_{k}^{t}$ as $Z^t$ for simplicity.
Similar to previous analysis, we analyze the local field $\{l_{t+s} - l_t \}$ where
\begin{align}
l_t:=b \frac{Z^t}{  \sqrt{ \mbox{Var} (Z^t)} }-\frac{1}{2} b^2 \\
l_{t+s}:= b \frac{Z^{t+s} }{  \sqrt{ \mbox{Var} (Z^{t+s} )} } -\frac{1}{2} b^2 
\end{align}
Then, for this sequential testing problem, after change of measure, we obtain the representation:
\begin{align}
& \mbox{Pr} \left( \max_{ \begin{subarray}{c} B \leqslant t \leqslant T \\ k=t-B \end{subarray}}  \frac{Z_{k}^t(B) }{\sqrt{\mbox{Var} \left( Z_{k}^t (B)\right)}}>b \right)\\
= & e^{-\frac{1}{2}  b^2 } \sum_{t=B}^{T} \mathbb{E}_t \left( \frac{\mathcal{M}_t}{\mathcal{S}_t} e^{\tilde{l}_t+m_t}; \tilde{l}_t+m_t \geqslant 0 \right)
\end{align}
where 
\begin{align}
& \mathcal{M}_t=\max_{B \leqslant m \leqslant T} e^{l_m-l_t} \\
& \mathcal{S}_t=\sum_{B \leqslant m \leqslant T} e^{l_m-l_t} \\
& \tilde{l}_t=l_t-b \\
& m_t=\mbox{log} {\mathcal{M}_t}
\end{align}
We can also show that:
\begin{align}
\mathbb{E}_t \{l_{t+s}-l_t \} & =-b^2(1-r)=-b^2 \frac{2B-1}{B(B-1)} |s| \\
\mbox{Var}_t \{ l_{t+s}-l_t\} &= b^2 \frac{2(2B-1)}{B(B-1)} |s|
\end{align}
Define:
\begin{align}
\mu^2=b^2 \frac{2(2B-1)}{B(B-1)} 
\end{align}
Then we have:
\begin{align}
\mbox{Pr} _{\infty} \left( t \leqslant T \right)& =\mbox{Pr} \left( \max_{ \begin{subarray}{c} B \leqslant t \leqslant T \\ k=t-B \end{subarray}}  \frac{Z_{k}^t(B) }{\sqrt{\mbox{Var} \left( Z_{k}^t (B)\right)}}>b \right)\\ 
 & \approx  \frac{1}{\sqrt{2 \pi}}    e^ {-\frac{1}{2}  b^2 }   \sum_{t=B}^T 
\frac{b^2 (2B-1)\nu \left(  b \sqrt{\frac{2(2B-1)}{B(B-1)}} \right) }{B(B-1)}
\end{align}


\newpage
\section*{Evaluate Tail Probability}
\subsection*{Exp 1: M=2, N=10, T=100, rep=1000}
The estimated tail probability:
\begin{center}
  \begin{tabular}{c|c|c}
  \hline
                  & MC& Theory \\ \hline
b=3.0 & 0.2060  & 0.4344   \\ \hline
b=3.1 &  0.1650  & 0.3209  \\ \hline
b=3.2 &  0.1600  &  0.2346 \\ \hline
b=3.3 & 0.0950   &  0.1698 \\ \hline
b=3.4 &  0.1020  &0.1216   \\ \hline
b=3.5 &  0.0460  &  0.0818 \\ \hline
b=3.6 &  0.0410  & 0.0605  \\ \hline
b=3.7 & 0.0430   & 0.0420  \\ \hline
b=3.8 & 0.0290   & 0.0289  \\ \hline
b=3.9 &  0.0320  &  0.0197 \\ \hline
b=4.0 & 0.0160   &  0.01324 \\ \hline
b=4.1 &  0.0080  & 0.0088  \\ \hline
b=4.2 &  0.0070  &  0.0058 \\ \hline
b=4.3 & 0.0020   & 0.0038  \\ \hline
b=4.4 &0.0050    & 0.0025  \\ \hline
b=4.5 & 0.0030   &  0.0016 \\ \hline
  \end{tabular}
\end{center}

\begin{figure}[H]
  \caption{M=2,T=100}
  \centering
    \includegraphics[trim = 20mm 70mm 20mm 70mm, clip, width=0.5\textwidth]{exp1}
\end{figure}

\newpage
\subsection*{Exp 2: M=5, N=10, T=100, rep=1000}
The estimated tail probability:
\begin{center}
  \begin{tabular}{c|c|c}
  \hline
                  & MC& Theory \\ \hline
b=3.0 &   0.3230&  0.3510  \\ \hline
b=3.1 & 0.2720   & 0.2636   \\ \hline
b=3.2 & 0.2160   &  0.1957 \\ \hline
b=3.3 &  0.1900  &  0.1436 \\ \hline
b=3.4 & 0.1540   &  0.1042 \\ \hline
b=3.5 &  0.1150  & 0.0747  \\ \hline
b=3.6 & 0.0930   & 0.0530  \\ \hline
b=3.7 &  0.0850  & 0.0372  \\ \hline
b=3.8 &  0.0750  & 0.0258  \\ \hline
b=3.9 & 0.0580   & 0.0177  \\ \hline
b=4.0 & 0.0490   &  0.0120 \\ \hline
b=4.1 &  0.0460  &  0.0080 \\ \hline
b=4.2 &0.0380    & 0.0053  \\ \hline
b=4.3 & 0.0200   & 0.0035  \\ \hline
b=4.4 &  0.0140  & 0.0023  \\ \hline
b=4.5 & 0.0140   &0.0015   \\ \hline
  \end{tabular}
\end{center}

\begin{figure}[H]
  \caption{M=5,T=100}
  \centering
    \includegraphics[trim = 20mm 70mm 20mm 70mm, clip, width=0.5\textwidth]{exp2}
\end{figure}


The estimated tail probability:
\begin{center}
  \begin{tabular}{c|c|c}
  \hline
                  & MC& Theory \\ \hline
b=3.0 &   &    \\ \hline
b=3.1 &    &   \\ \hline
b=3.2 &    &   \\ \hline
b=3.3 &    &   \\ \hline
b=3.4 &    &   \\ \hline
b=3.5 &    &   \\ \hline
b=3.6 &    &   \\ \hline
b=3.7 &    &   \\ \hline
b=3.8 &    &   \\ \hline
b=3.9 &    &   \\ \hline
b=4.0 &    &   \\ \hline
b=4.1 &    &   \\ \hline
b=4.2 &    &   \\ \hline
b=4.3 &    &   \\ \hline
b=4.4 &    &   \\ \hline
b=4.5 &    &   \\ \hline
  \end{tabular}
\end{center}






\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Approximation of r
