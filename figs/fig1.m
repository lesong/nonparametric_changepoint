% fig

ARL=0.1:0.1:1;

M_2=[3.37,3.62, 3.73, 3.80, 3.85, 4.00, 4.02, 4.04, 4.05, 4.07];
T_2=[3.46, 3.66, 3.76, 3.84, 3.90, 3.94, 3.98, 4.02, 4.05, 4.07];

M_5=[3.61, 3.99, 4.05, 4.10, 4.36, 4.43, 4.47, 4.61, 4.65, 4.69];
T_5=[3.43, 3.63, 3.74, 3.82, 3.88, 3.93, 3.97, 4.00, 4.03, 4.06];

M_10=[3.58, 3.96, 4.09, 4.31, 4.36, 4.48, 4.61, 4.63, 4.65, 4.67];
T_10=[3.35, 3.56, 3.68, 3.76, 3.83, 3.88, 3.92, 3.95, 3.99, 4.01];

M_20=[3.36, 3.76, 3.99, 4.11, 4.17, 4.30, 4.33, 4.36, 4.39, 4.43];
T_20=[3.22, 3.46, 3.58, 3.67, 3.73, 3.79, 3.83, 3.87, 3.90, 3.93];


M_50=[3.00, 3.38, 3.58, 3.78, 3.93, 3.97, 4.11, 4.14, 4.16, 4.18];
T_50=[2.99, 3.25, 3.39, 3.48, 3.56, 3.61, 3.66, 3.70, 3.74, 3.77];


figure;
%subplot(2,2,1);

%plot(ARL,M_2,'b-*'); hold on
%plot(ARL,T_2,'r-o'); 
%ylim([2,6]);
%xlim([0,1.1]);
%xlabel('ARL(10^4)')
%ylabel('b')
%legend('MC (B=2)','Theory (B=2)','Location','northwest');


subplot(2,2,1);
plot(ARL,M_5,'b-*','linewidth',1.2); hold on
plot(ARL,T_5,'r-o','linewidth',1.2); 
ylim([2,6]);
xlim([0,1.1]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Simulation (B=5)','Theory (B=5)','Location','northwest');

subplot(2,2,2);

plot(ARL,M_10,'b-*','linewidth',1.2); hold on
plot(ARL,T_10,'r-o','linewidth',1.2); 
ylim([2,6]);
xlim([0,1.1]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Simulation (B=10)','Theory (B=10)','Location','northwest');

subplot(2,2,3);

plot(ARL,M_20,'b-*','linewidth',1.2); hold on
plot(ARL,T_20,'r-o','linewidth',1.2); 
ylim([2,6]);
xlim([0,1.1]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Simulation (B=20)','Theory (B=20)','Location','northwest')

subplot(2,2,4);

plot(ARL,M_50,'b-*','linewidth',1.2); hold on
plot(ARL,T_50,'r-o','linewidth',1.2); 
ylim([2,6]);
xlim([0,1.1]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Simulation (B=50)','Theory (B=50)','Location','northwest')






