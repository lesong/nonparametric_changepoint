% M=100, N=10, rep=5000 T=10,3,\dots, 109

Gauss=[2.0, 0.2368;
       2.3, 0.1590;
       2.6, 0.1094;
       2.9, 0.0698;
       3.2, 0.0432;
       3.5, 0.0212;
       3.8, 0.0166;
       4.1, 0.0094;
       4.4, 0.0038;
       4.7, 0.0038;
       5.0, 0.0012];
 
 Exp=[2.0, 0.2646;
     2.3, 0.1904;
     2.6, 0.1302;
     2.9, 0.0950;
     3.2, 0.0520;
     3.5, 0.0366;
     3.8, 0.0226;
     4.1, 0.0130;
     4.4, 0.0068;
     4.7, 0.0046;
     5.0, 0.0016];

 Graph=[2.0, 0.2284;
       2.3, 0.1614;
       2.6, 0.0912;
       2.9, 0.0642;
       3.2, 0.0336;
       3.5, 0.0188;
       3.8, 0.0116;
       4.1, 0.0062 ];
   
   
 Lap=[2.0, 0.2356;
     2.3, 0.1614;
     2.6, 0.1084;
     2.9,0.066;
     3.2, 0.0462;
     3.5, 0.0244;
     3.8, 0.0134;
     4.1, 0.0080;
     4.4, 0.0028;
     4.7, 0.0028;
     5.0, 0.0010];


b_sim=zeros(10,4); % threshold for ARL=0.1,..., 1 

b1=2:0.01:5.0;


y=interp1(Gauss(:,1),100./Gauss(:,2),b1);
b_sim(:,1)=b1(findthre(y));

b2=2:0.01:5.0;

y=interp1(Exp(:,1),100./Exp(:,2),b2);
b_sim(:,2)=b2(findthre(y));

b3=2:0.01:4.1;

y=interp1(Graph(:,1),100./Graph(:,2),b3);
b_sim(:,3)=b3(findthre(y));

b4=2:0.01:5.0;
y=interp1(Lap(:,1),100./Lap(:,2),b4);
b_sim(:,4)=b4(findthre(y));

M=100;
T=199;
j=1;
for b=1.5:0.01:5
    
Pr=0;

 temp1=exp(-0.5*(b^2));
 temp2=1/sqrt(2* pi);
 for t=M:1:T
     
   
    temp3=b*sqrt(2*(2*M-1)/M/(M-1));
    temp4=fv(temp3);
    temp5=(b^2)*(2*M-1)/M/(M-1);
    temp6=temp4*temp5;
    
    Pr=Pr+temp6;
 end
 Pr=Pr*temp1*temp2;

output_1(j)=b;

output_2(j)=100./Pr;
j=j+1;

end



b_est=output_1(findthre(output_2));


ARL=0.1:0.1:1;
figure;

plot(ARL,b_sim(:,1),'k--*','linewidth',1.5); hold on
plot(ARL,b_sim(:,2),'b--*','linewidth',1.5); hold on

plot(ARL,b_sim(:,3),'m--*','linewidth',1.5); hold on
plot(ARL,b_sim(:,4),'g--*','linewidth',1.5); hold on


plot(ARL,b_est,'r-o','linewidth',1.2); hold on


xlim([0,1.1]);
ylim([1,7]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Gaussian(0,I)','Exp(1)','Random Graph (Node=10, p=0.2)',...
'Laplace(0,1)','Theory ','Location','northwest');

% errorbar


