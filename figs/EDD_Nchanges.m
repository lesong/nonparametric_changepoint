% EDD

% evluate the influnce of block size


EDD_N2=[185.146;
        19.810;
        10.234;
        10;
        10];
    
EDD_N10=[ 104.354;
          12.5780;
          10.0220;
          10;
          10];
     
EDD_N20=[ 89.924;
          12.476;
          10.0006;
          10;
          10];
    
     
    mu=0.2:0.2:1.0;
    
    figure;
    
plot(mu,(EDD_N2),'b-*','linewidth',1.2); hold on
plot(mu,(EDD_N10),'r-*','linewidth',1.2); hold on
plot(mu,(EDD_N20),'k-*','linewidth',1.2); hold on

xlim([0.2,0.8])
xlabel('mu')
ylabel('EDD')
grid on

legend('N=2 (b=3.83)','N=10 (b=3.83)','N=20 (b=3.83)');


    
    