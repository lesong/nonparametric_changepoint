% EDD

% evluate the influnce of block size


EDD_M2=[1519.712;
        264.326;
        41.626;
        9.532;
        4.028;
        2.500;
        2.074;
        2.008;
        2.0040;
        2];
    
EDD_M5=[ 241.4100;
         21.794;
         6.5460;
         5.0800;
         5;
         5;
         5;
         5;
         5;
         5];
     
EDD_M10=[ 104.354;
          12.5780;
          10.0220;
          10;
          10;
         10;
         10;
         10;
          10;
          10];
    
      
 EDD_M20=[51.07;
     20.116;
     20.000;
      20.000;
       20.000;
        20.000;
         20.000;
          20.000;
           20.000;
            20.000];
    mu=0.2:0.2:2.0;
    
    figure;
    
plot(mu,(EDD_M2),'b-*','linewidth',1.2); hold on
plot(mu,(EDD_M5),'c-*','linewidth',1.2); hold on
plot(mu,(EDD_M10),'k-*','linewidth',1.2); hold on
plot(mu,(EDD_M20),'r-*','linewidth',1.2); hold on

xlim([0.2,1])
xlabel('mu')
ylabel('EDD')
grid on

legend('B=2 (b=3.90)','B=5 (b=3.88)','B=10 (b=3.83)','B=20 (b=3.73)');


    
    