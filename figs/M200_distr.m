% M=100, N=10, rep=5000 T=10,3,\dots, 109

Gauss=[1.5, 0.2996;
       1.8, 0.2144;
       2.1, 0.1438;
       2.4, 0.0970;
       2.7, 0.0568;
       3.0, 0.0380;
       3.3, 0.0204;
       3.6, 0.0146;
       3.9, 0.0092];
 
 Exp=[1.5, 0.3136;
     1.8, 0.2258;
     2.1, 0.1534;
     2.4, 0.1080;
     2.7, 0.0678;
     3.0, 0.0442;
     3.3, 0.0316;
     3.6, 0.0194;
     3.9, 0.0092];

 Graph=[1.5, 0.2798;
     1.8, 0.2014;
     2.1, 0.1324;
     2.4, 0.0838;
     2.7, 0.0522;
     3.0, 0.0294;
     3.3, 0.0172;
     3.6, 0.0080;
     3.9, 0.0024];
   
   
 Lap=[1.5, 0.2920;
     1.8, 0.2084;
     2.1, 0.1560;
     2.4, 0.0940;
     2.7, 0.0654;
     3.0, 0.0458;
     3.3, 0.0252;
     3.6, 0.0154;
     3.9, 0.0074];


b_sim=zeros(10,4); % threshold for ARL=0.1,..., 1 

b1=1.5:0.01:3.9;


y=interp1(Gauss(:,1),100./Gauss(:,2),b1);
b_sim(:,1)=b1(findthre(y));

b2=1.5:0.01:3.9;
y=interp1(Exp(:,1),100./Exp(:,2),b2);
b_sim(:,2)=b2(findthre(y));

b3=1.5:0.01:3.9;
y=interp1(Graph(:,1),100./Graph(:,2),b3);
b_sim(:,3)=b3(findthre(y));

b4=1.5:0.01:3.9;
y=interp1(Lap(:,1),100./Lap(:,2),b4);
b_sim(:,4)=b4(findthre(y));

M=200;
T=299;
j=1;
for b=1.2:0.01:4.5
    
Pr=0;

 temp1=exp(-0.5*(b^2));
 temp2=1/sqrt(2* pi);
 for t=M:1:T
     
   
    temp3=b*sqrt(2*(2*M-1)/M/(M-1));
    temp4=fv(temp3);
    temp5=(b^2)*(2*M-1)/M/(M-1);
    temp6=temp4*temp5;
    
    Pr=Pr+temp6;
 end
 Pr=Pr*temp1*temp2;

output_1(j)=b;

output_2(j)=100./Pr;
j=j+1;

end



b_est=output_1(findthre(output_2));


ARL=0.1:0.1:1;
figure;

plot(ARL,b_sim(:,1),'k--*','linewidth',1.5); hold on
plot(ARL,b_sim(:,2),'b--*','linewidth',1.5); hold on

plot(ARL,b_sim(:,3),'m--*','linewidth',1.5); hold on
plot(ARL,b_sim(:,4),'g--*','linewidth',1.5); hold on


plot(ARL,b_est,'r-o','linewidth',1.2); hold on


xlim([0,1.1]);
ylim([1,7]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Gaussian(0,I)','Exp(1)','Random Graph (Node=10, p=0.2)',...
'Laplace(0,1)','Theory ','Location','northwest');

% errorbar


