% M=10, N=10, rep=5000 T=10,3,\dots, 109

Gauss=[2.5, 0.2016;
    2.6,0.1758;
    2.7, 0.1626;
    2.8,0.1294;
    2.9, 0.1044;
    3.0,0.1002;
    3.1,0.0810;
    3.2,0.0784;
    3.3,0.0622;
    3.4,0.0472;
    3.5,0.0408;
    3.6,0.0320;
    3.7,0.0322;
    3.8,0.0234;
    3.9,0.0226;
    4.0,0.0152;
    4.1,0.0154;
    4.2,0.0094];
 
 Exp=[
     2.5, 0.2476;
     2.8, 0.1688;
     3.1, 0.1106;
     3.4, 0.0750;
     3.7, 0.0442;
     4.0, 0.0280;
     4.3, 0.0182;
     4.6, 0.0060;
     4.9, 0.0032;
     5.2, 0.0020];

 Graph=[2.5, 0.1966;
     2.8, 0.1326;
     3.1, 0.0768;
     3.4, 0.0380;
     3.7, 0.0242;
     4.0, 0.0142;
     4.3, 0.0060];
   
   
 Lap=[2.5, 0.2188;
     2.8, 0.1422;
     3.1, 0.0910;
     3.4, 0.0518;
     3.7, 0.0390;
     4.0, 0.0196;
     4.3, 0.0126;
     4.6, 0.0046];


b_sim=zeros(10,4); % threshold for ARL=0.1,..., 1 



b1=2.5:0.01:4.2;
y=interp1(Gauss(:,1),100./Gauss(:,2),b1);
b_sim(:,1)=b1(findthre(y));

b2= 2.5:0.01:5.2;
y=interp1(Exp(:,1),100./Exp(:,2),b2);
b_sim(:,2)=b2(findthre(y));

b3=2.5:0.01:4.3;
y=interp1(Graph(:,1),100./Graph(:,2),b3);
b_sim(:,3)=b3(findthre(y));

b4=2.5:0.01:4.6;
y=interp1(Lap(:,1),100./Lap(:,2),b4);
b_sim(:,4)=b4(findthre(y));



b_est=[2.99, 3.25, 3.39, 3.48, 3.56, 3.61, 3.66, 3.70, 3.74, 3.77];

ARL=0.1:0.1:1;
figure;

plot(ARL,b_sim(:,1),'k--*','linewidth',1.5); hold on
plot(ARL,b_sim(:,2),'b--*','linewidth',1.5); hold on

plot(ARL,b_sim(:,3),'m--*','linewidth',1.5); hold on
plot(ARL,b_sim(:,4),'g--*','linewidth',1.5); hold on


plot(ARL,b_est,'r-o','linewidth',1.2); hold on


xlim([0,1.1]);
ylim([1,7]);
xlabel('ARL(10^4)')
ylabel('b')
grid on
legend('Gaussian(0,I)','Exp(1)','Random Graph (Node=10, p=0.2)',...
'Laplace(0,1)','Theory ','Location','northwest');

% errorbar


