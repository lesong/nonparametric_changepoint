%% analytic computation
function [b1,ARL]=tail_est(M)


%% 
%M=150; %M is the maximum block size 
% T is the time


j=1;

for b=1.5:0.01:5

 temp1=exp(-0.5*(b^2));
 temp2=1/sqrt(2* pi);
 
    
   
    temp3=b*sqrt(2*(2*M-1)/M/(M-1));
    temp4=fv(temp3);
    temp5=(b^2)*(2*M-1)/M/(M-1);
    temp6=temp4*temp5;
 
 Pr=temp6*temp1*temp2;

output(j,1)=b;

output(j,2)=1/Pr;
j=j+1;

end

b1=output(:,1);
ARL=output(:,2);
end

%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2