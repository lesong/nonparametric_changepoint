% offline 


clear
close all

N=5; 
M=50;
L=500;

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

testing=zeros(1,L);
reference=zeros(1,N*M);

rng(1);
testing(1:floor(L/2))=normrnd(mu_0,sigma_1,[1,floor(L/2)]);
rng(2);
testing(floor(L/2)+1: L)=randlap(1,floor(L/2));
%testing(floor(M/2)+1: M)=normrnd(mu_0,sigma_1,[1,floor(M/2)]);
rng(3);
reference=normrnd(mu_0,sigma_1,[1,N*M]);


alldata=reference;
%% estimate bandwidth

[ms,ns]=size(alldata);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,alldata(:,(i+1):ns),alldata(:,i));
    Ds(i,(i+1):ns)=temp.*temp;
end
% use rule of thumb to determine the bandwidth
bandw=median(Ds(Ds~=0));

%% estimate variance
X_s=zeros(1,8000);
X_s(:,[1:8000])=normrnd(mu_0,sigma_1,[1,8000]);

sigma_2_sq=hyy(X_s,bandw);
sigma_4_sq=hxxyy(X_s,bandw);
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;

S_var=C*2/M/(M-1);



 [b_1,ARL]=tail_est(M); % theory
  b=b_1(findARL5000(ARL));

 %% initialize      
 n=length(testing);
 
     %  sample data
       pool=reference;

       index=M;  
       
       X=reference(1:N*M); % X is reference data
       X_sample=datasample(pool,N*M, 'Replace',false ); % X is reference data      
       Y=testing;
 
       Kxx_post=fKxx1(Y(:, index-M+1:index), Y(:, index-M+1:index),M,bandw,1); % M by M
       
       Kxx_pre=[];
       Kxx_cross=[];
       
       for j=1:N       
        Kxx_pre=[Kxx_pre; fKxx1(X(:,(j-1)*M+1: j*M),X(:,(j-1)*M+1: j*M),M,bandw,1)]; %  N*M by M 
        Kxx_cross=[Kxx_cross; fKxx1(X(:,(j-1)*M+1: (j-1)*M+M), Y(:, index-M+1: index),M,bandw,2)]; % N*M by M
       end
       
       B_stat(1:M-1)=0; 
      
       
  for t=M:n-1
    
      
         
          MMD=[];
             T=Kxx_post;
             temp1=1/M/(M-1)*sum(T(:));
         for j=1:N  
             A=Kxx_pre( j*M-M+1:j*M,  1:M );
             C=Kxx_cross( j*M-M+1:j*M,  1:M);
             MMD(j)= 1/M/(M-1)*sum(A(:))+ temp1 - 2/M/(M-1)*sum(C(:));
         end 
         S=mean(MMD);
   
       
         B_stat(t)=(S./sqrt(S_var)) ;
     
        %% update
            pool=[pool,Y(index-M+1)];
            
            index=index+1; % index is the end of sliding window
          
           
           Kxx_post(1:M-1,1:M-1)=Kxx_post(2:M,2:M);
           temp=fKxx1(Y(:,index-M+1:index),Y(:, index),M,bandw,3); % M by 1
           Kxx_post(:,M)=temp;
           Kxx_post(M,:)=temp';
           
     
           % given new data, we need to update Kxx_post, Kxx_pre, and Kxx_cross
          
           r=mod(index,M); % if r==0, we need to sample the reference data blocks
           
           
           
           if r==0
               
               
                for j=1:N
                  
               Kxx_pre((j-1)*M+1:(j-1)*M+M-1, 1:M-1)=Kxx_pre((j-1)*M+2:(j-1)*M+M, 2:M);
               temp=fKxx1( X_sample(:,(j-1)*M+1:(j-1)*M+M), X_sample(:, (j-1)*M+M),M,bandw,3);
               Kxx_pre((j-1)*M+1:(j-1)*M+M, M)=temp;
               Kxx_pre((j-1)*M+M,:)=temp';
                                
           % Kxx_cross
                     
               Kxx_cross((j-1)*M+1:(j-1)*M+M-1, 1:M-1)=Kxx_cross((j-1)*M+2:(j-1)*M+M, 2:M);
               temp1=fKxx1(X_sample(:,(j-1)*M+1:(j-1)*M+M), Y(:,index),M,bandw,3);
               temp2=fKxx1( Y( :,index-M+1:index), X_sample(:,(j-1)*M+M),M,bandw,3);
               Kxx_cross((j-1)*M+1:(j-1)*M+M, M)=temp1;
               Kxx_cross((j-1)*M+M,:)=temp2';
                end
                
                X=X_sample;   
               X_sample=datasample(pool,N*M, 'Replace',false ); % X is reference data   
           elseif r~=0
           
               for j=1:N
                  
               Kxx_pre((j-1)*M+1:(j-1)*M+M-1, 1:M-1)=Kxx_pre((j-1)*M+2:(j-1)*M+M, 2:M);
               temp=fKxx1( [X(:, j*M-(M-r)+1:j*M), X_sample(:,(j-1)*M+1:(j-1)*M+r)], X_sample(:, (j-1)*M+r),M,bandw,3);
               Kxx_pre((j-1)*M+1:(j-1)*M+M, M)=temp;
               Kxx_pre((j-1)*M+M,:)=temp';
                                
           % Kxx_cross
                     
               Kxx_cross((j-1)*M+1:(j-1)*M+M-1, 1:M-1)=Kxx_cross((j-1)*M+2:(j-1)*M+M, 2:M);
               temp1=fKxx1([X(:, j*M-(M-r)+1:j*M), X_sample(:,(j-1)*M+1:(j-1)*M+r)] , Y(:,index),M,bandw,3);
               temp2=fKxx1( Y( :,index-M+1:index), X_sample(:,(j-1)*M+r),M,bandw,3);
               
              % temp2=fKxx1( X( :,index-(j+2)*M+1:index-(j+2)*M+M), X(:,index-(j+2)*M+M),M,bandw,3);
               Kxx_cross((j-1)*M+1:(j-1)*M+M, M)=temp1;
               Kxx_cross((j-1)*M+M,:)=temp2';
                           
              end   
                             
               
           end
  
 
    end         



figure
subplot(2,1,1)
plot(testing,'o');
ylim([-10,10])
xlabel('Location')
ylabel('Original Signal')
subplot(2,1,2)
plot(B_stat); hold on
ylim([-1,8]);
plot(1:0.01:500,b); hold on
for i=2:500
    if B_stat(i-1)<b && B_stat(i)>=b
        loc=i;
        break
    end
end

plot(loc, -1:0.01:8)
xlabel('Location')
ylabel('Statistic')
%% loc=268














