%figplot
clear
close all
load testing.mat
load B_statMed
XMed=B_stat;

load B_statMed100
XMed2=B_stat;

load B_statMed01
XMed3=B_stat;

b=3.55;

subplot(2,1,1)
plot(testing);
xlim([1,2000]);
xlabel('Time');
ylabel('Seismic Signal')
subplot(2,1,2)
plot(XMed,'b'); hold on
plot(XMed2,'r'); hold on
plot(XMed3,'k'); hold on
xlim([1,2000]);
plot(1:0.1:2000,b,'g');
legend('Bandwidth=Median','Bandwidth=100Median','Bandwidth=0.1Median')
xlabel('Time');
ylabel('Statistic')