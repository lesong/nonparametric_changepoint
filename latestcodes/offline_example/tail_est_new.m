%% analytic computation

clear
close all
format long

%% 
M=500;
b=3.3:0.01:3.4;
L=length(b);
output=zeros(L,2);
j=1;
for b=3.3:0.01:3.4;

Pr=0;

for B=2:M
    
    temp1=exp(-0.5*(b^2));
    temp2=b*sqrt((2*B-1)/B/(B-1));
    temp2=fv(temp2);
    temp3=(b^2)*(2*B-1)/2/B/(B-1)/sqrt(2* pi);
    Pr=Pr+temp1*temp2*temp3;
    
end

output(j,1)=b;
output(j,2)=Pr;
j=j+1;

end
output

%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2
