clear
close all
load B_stat.mat

load testing.mat

M=500;
b=3.34; % threshold
figure
subplot(2,1,1)
plot(testing,'o');

ylim([-10,10])
subplot(2,1,2)
plot(B_stat(end:-1:1)); hold on

ylim([-1,8]);
plot(1:0.1:500,b,'r'); hold on

loc=find(B_stat==max(B_stat));
plot(M-loc, -1:0.1:8,'g');
