% offline 


clear
close all

N=5; 
M=500;

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

testing=zeros(1,M);
reference=zeros(1,N*M);

rng(1);
testing(1:floor(M/2))=normrnd(mu_0,sigma_1,[1,floor(M/2)]);
rng(2);
testing(floor(M/2)+1: M)=randlap(1,floor(M/2));
%testing(floor(M/2)+1: M)=normrnd(mu_0,sigma_1,[1,floor(M/2)]);
rng(3);
reference=normrnd(mu_0,sigma_1,[1,N*M]);


alldata=[reference,testing];
%% estimate bandwidth

[ms,ns]=size(alldata);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,alldata(:,(i+1):ns),alldata(:,i));
    Ds(i,(i+1):ns)=temp.*temp;
end
% use rule of thumb to determine the bandwidth
bandw=median(Ds(Ds~=0));

%% estimate variance
X_s=zeros(1,8000);
X_s(:,[1:8000])=normrnd(mu_0,sigma_1,[1,8000]);

sigma_2_sq=hyy(X_s,bandw);
sigma_4_sq=hxxyy(X_s,bandw);
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
B=2:1:M;
S_var=C*2./B./(B-1);


 %% compute kernel matrix    
 
 Kxx_post=fKxx(testing, testing,N,M,bandw,1);  % M by M
 Kxx_pre=fKxx(reference, reference,N,M,bandw,2);  %  N*M by M 
 Kxx_cross=fKxx(reference, testing ,N,M,bandw,3) ; % N*M by M


%% compute statistic
 
 B_stat=zeros(1,M-1);
 for B=2:M
     MMD=[];
             T=Kxx_post(M-B+1:M, M-B+1:M);
             temp1=1/B/(B-1)*sum(T(:));
         for j=1:N  
             A=Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C=Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             MMD(j)= 1/B/(B-1)*sum(A(:))+ temp1 - 2/B/(B-1)*sum(C(:));
         end 
         S=mean(MMD);
       
         B_stat(B-1)=S./sqrt(S_var(B-1)) ;
         %B_stat(B-1)=S;
 end

b=3.34; % threshold
figure
subplot(2,1,1)
plot(testing,'o');
ylim([-10,10])
subplot(2,1,2)
plot(B_stat(end:-1:1)); hold on
%ylim([-1,8]);
plot(1:0.01:500,b); hold on
loc=find(B_stat==max(B_stat));
plot(M-loc, -1:0.01:8)

%% threshold=3.34














