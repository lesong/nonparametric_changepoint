\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{cite}
\usepackage[margin=1in]{geometry}

\title{Response to Reviewers}
\author{T-SP-20573-2016: Sketching for Sequential Change-Point Detection}
%\date{submitted to IEEE Signal Processing Letters}

%\usepackage{natbib}

\usepackage{color,graphicx,amsmath,amsthm,amssymb,enumerate,comment,hyperref,cite}

\newcommand{\yx}[1]{{\color{red}{#1}}}

\newcommand{\yc}[1]{{\color{blue}{#1}}}

\begin{document}

\maketitle

{\setlength{\parindent}{0cm}Dear Editor and Reviewers,\\}

We appreciate the reviewers' thorough reading and thoughtful comments, for providing many insights and great suggestions, which helps us greatly improve the quality of the paper. We have done our best to address the comments and provide appropriate corrections, amendments, and clarifications.

\vspace{.1in}
In this letter, we provide our response on a point-by-point basis. Reviewers' comments are italic. Our response follows the reviews after each bullet point.

\vspace{.1in}
While addressing reviewers' comments, we have particularly made the following revisions

\begin{itemize}

\item We have added a broader list of related references from the statistical process control (SPC) literature and others, as well as commented on the difference from our method.

\item We have highlighted the novelty of our method and our contributions in the introduction.

\item We have added more numerical examples to have a more complete demonstration of our method. In particular, we have:
\begin{itemize}
\item added more instances as suggested by reviewer 1.
\item increased the number of repetitions to $10^4$ for all of our examples.
\item added the standard deviation of our numerical results.
\item  added a new example to compare the performance of random Gaussian and expander graph projections, when the post change mean vector has both positive and negative entries.
\item compared the performance of our method with a classic method (suggested by reviewer 2), multivariate CUSUM applied to the sketches, and demonstrated the benefits of our method.
\end{itemize}

\item Further explained how threshold is determined and why ARL is set to be 5000;

\item Clarified that the EDD's are compared under a constant (same) ARL value.  This leads to a new argument in Remark 4 for the same statement that ``there is little loss in performance due to sketching''.

\end{itemize}


Thank you very much for your consideration.

\vspace{.5in}



{\setlength{\parindent}{0cm}Sincerely,\\

Yao Xie}

\newpage

\section*{Response to Reviewer 1}

{\bf Author Response}: We appreciate the reviewers' thorough reading and thoughtful comments, for providing many insights and great suggestions, which helps us greatly improve the quality of the paper.

\vspace{0.1in}


{\it Change point detection  problem stated in the paper is closely related to the field of quality control and multivariate control charts.
The assumption that observed random vector in a normal state (without change) is a multivariate Gaussian
with  a fully known covariance matrix (or equivalently it is
of the form of white Gaussian noise $N(0,I)$)  seems to be very classical and simple. The difficulty of the problem lies in the high dimensionality of the
observations.
The paper proposes the use of generalized likelihood ratio (GLR) statistics with maximum
likelihood ratio estimator of the mean vector based on sequence of observations. Random linear projections (" scatches") are applied for dimensionality reduction. Random normal vectors remain normal after such linear transformations and the same method can be utilized for change-point detection
on the basis of "projected" data.}

\vspace{0.1in}


{\it It should be noted that the idea of using random projection for change-point  detection is not new (both for fixed and time-varying projections),
see papers (and literature cited in there):

Skubalska-Rafaj\l{}owicz, Ewa
Random projections and Hotelling's T2 statistics for change detection in high-dimensional data streams.
Int. J. Appl. Math. Comput. Sci. 23 (2013), no. 2, 447-461. (MR3113554)

  Skubalska-Rafaj\l{}owicz, Ewa Change-point detection of the mean vector with fewer observations than the dimension using instantaneous normal random projections.
Stochastic models, statistics and their applications, 179-186, Springer Proc. Math. Stat., 122, Springer, Cham, 2015. (MR3336437)

These approaches also assume normality of the observed vector (but  the covariance matrix is not known) and they are based on individual observations
(without memory).

GLR method based on individual multivariate observation results in the Mahalanobis distance (and $\chi^{2}_{N}$  statistics) or
the Hotelling statistic monitoring (when the covariance matrix should be estimated from historical data). It is known that this approach
is very efficient (from EDD point of view) when a change in the mean ($||\mu||$) is relatively large in comparison to the total variance of the noise (here dimension $N$). Multivariate versions of CUSUM and EWMA methods based on the whole sequence of observations are usual choices when
when a change in the mean  is relatively small.

The  projection on lower dimensional space were proposed in the paper:
Runger, G.C. (1996). Projections and the U-squared
multivariate control chart, Journal of Quality Technology
28(3): 313-319.

See also, for example:
Bodnar, O. and Schmid, W. (2005). Multivariate control charts
based on a projection approach,  Allgemeines Statistisches
Archiv 89(1): 75-93. It is relevant reference in the context of discussion in Section VI (p.10, last paragraph).

Summarizing, a border list of related references should be added and the adequate contribution of the paper should be precisely explained.
}


\vspace{0.1in}
\noindent{\bf Author Response}: Thank you for pointing out an important link to quality control and multi-variate control charts. We have added all the suggested references to the paper, acknowledged the earlier work and contributions from the statistical quality control literature in the introduction (Page 1, second column, third paragraph), as well as pointed out the difference between our method and highlighted our contributions.

First, our method is derived from the statistical sequential hypothesis testing approach, treating both the post-change mean vector and the change-point location as unknown parameters when deriving the generalized likelihood ratio statistic. (In the SPC literature, the GLR chart only treats the post-change mean vector as unknown.) Second, the analytical techniques for establishing the ARL performances for SPC and our likelihood-based method are quite different. Third, we also consider the choice of random projections including sparse 0-1 projections and study the amount of dimensionality reduction can be performed (i.e., the minimum $M$) to achieve little performance loss, which are aspects that have not been explored in the above prior works in SPC.



We have also added the suggested reference to Section VI, Summary and Discussions.

\vspace{.1in}
{\it Additionally, I think that the paper:
Frankl P and Maehara H (1990) Some geometric applications of the beta distribution. Annals
of the Institute of Statistical Mathematics 42 (3):463-474, should  be additionally cited in the context of lemma 1,
since this paper concentrates on the topic of  random projections. }

\vspace{.1in}
\noindent{\bf Author Response}:
We have also added the suggested reference for Lemma 1.

\vspace{0.1in}
{\it Detection procedure provided in the paper is closely related to stoping rules defined in [5] (it is a special case of the rule given by (4.4) p.676
Thm. 1). Theorem 1(ARL) and its  proof follow precisely (as a special case) the adequate theorem from the paper [5].
For the proof of Theorem 2 the reader is sent to the relevant results in paper [5].}


\vspace{0.1in}
{\it However, there is  one open question.
In the paper (following [5]) it is stated that: "the expected detection
delay (EDD), defined to be the expected stopping time in the
extreme case where a change occurs immediately at $\kappa= 0$ .The EDD provides an upper bound on the expected delay
after a change-point until detection occurs when the change
occurs later in the sequence of observations".  But in [5] it is assumed that $\mu_{i} > 0$ and $( .)^{+}$
operator in the stoping rules is used. These restrictions are not present in the paper under considerations. In my opinion, after removing assumption
that $\mu_{i} > 0$, the  cited statement is not obvious. It needs a justification.}


\vspace{0.1in}\noindent{\bf Author Response}:
%
Indeed, this is a point requiring further explanation, and we realize that the original statement was not precise. Thank you for pointing this out. When $[\mu]_i > 0$, $\mathbb{E}^0\{T\}$ is an upper bound. Otherwise, it is not clear this will remain an upper bound. However, in the literature, using $\mathbb{E}^0\{T\}$ as a performance metric is still a common practice, since it gives some insights about the delay detection properties and is reasonably easy to analyze. We have added the following explanation in the text:

``The following argument from [28]  explains why we consider $\mathbb{E}^0\{T\}$. When there is a change at $\kappa$, we are interested in the expected delay until its detection, which is denoted as the conditional expectation $\mathbb{E}^{\kappa}\{T-\kappa|T > \kappa\}$, which is a function of $\kappa$. For many one-sided problems (i.e., the shift in the mean only occurs in the positive direction $[\mu]_i \geq 0$), it can be shown that $\sup_\kappa \mathbb{E}^{\kappa}\{T-\kappa|T > \kappa\} = \mathbb{E}^0\{T\}$. It is not obvious that this relation remains true when $[\mu]_i$ can be either positive or negative. However, since $\mathbb{E}^0\{T\}$ is certainly of interest and is reasonably easy to analyze, it is customary to consider $\mathbb{E}^0\{T\}$.''

\vspace{.1in}
{\it In page 6,  B 2) assumption $\mu_{i} > 0$  appears, but $( .)^{+}$ is not introduced into the stoping rule.}

\vspace{0.1in}\noindent{\bf Author Response}: In [5], they consider the one-side problem where the changes are assumed to be all in the positive direction, therefore, $(.)^+$ is used when estimating the post-change means. This is equivalent to incorporating the prior information in the detection procedure and it further improves the performance. In this paper, we considered a general version of the detection problem, where the change in the mean can be in either direction and hence we did not use the $(.)^+$ operation (otherwise the negative changes cannot be detected). In the subsection, when performing a subset of theoretical analysis for the sparse projection, to derive the results, we have assumed $\mu_i>0$. However, in our numerical examples, we do not make such an assumption when using sparse projection. So, for the consistency of writing, we did not introduce $(.)^+$ for the procedure.

\vspace{0.1in}
{\it It seems that using Gaussian random projections in the formulated  change-point detection problem is of rather theoretical value.
In this context,   sparse projection matrices  occur to be very attractive possibility, as a way to lessen the computational burden.
In my opinion, the approximate analysis of ARL and EED (p.6 subsection C) and sampling the observations  is the most important  method
easy usable in practice. In fact, this approach is sometimes used while processing large images.}

\vspace{0.1in}\noindent{\bf Author Response}: You are right. Thank you for acknowledging our contribution in sparse projection matrices.

\vspace{0.1in}
{\it Numerical Examples Section also needs many clarifications and some corrections.
First, the two regimes of performing the experiments are used in the paper, namely:
A. all coordinates are affected by the same change in the mean (Tab. I, II, IV, Fig. 3)
B.   only $p$ fraction of coordinates is affected by the mean change $\mu_{i}=1$.
It is clear that, these two approaches are equivalent in the mean, assuming Gaussian random projections.
Thus, for the comparison purposes variant B should be used both for Gaussian and for 0-1 matrices.}


\vspace{0.1in}\noindent{\bf Author Response}:
%
Thanks for the suggestion. For the purpose of comparison, we add two figures to consider now both variant A and variant B when the sketching matrix is Gaussian and expander graph. Fig. 4(a) and 4(b) are variant A and B for Gaussian random matrix. Fig. 5(a) and 5(b) are variant A and B for expander graph. In the case of variant A, the expander graph performs {\bf better} than Gaussian random matrix. We think that the reason is that expander graph is more powerful to aggregate the signal when all the entries of the post-change mean vector are positive. Therefore, we run a new experiment when the entries of the post-change mean vector are generated from i.i.d. uniform distribution in $[-3,3]$. The result of the new experiment is shown in Fig. 6. The result shows that expander graph performs {\bf similarly to} Gaussian random matrix   when post-change mean vector contain both positive and negative values, which proves our thoughts.


\vspace{0.1in}
{\it On the other hand, experiments shown in Table IV are very specific, when any $A_{t}$ will produce similar results. How it will  look like for  $\mu_{i}=1$
and $p=0.25$ (in both cases $||\mu||^2$ is $25$) ?}

\vspace{0.1in}\noindent{\bf Author Response}: We have added a new table (Table V) to address the case when $\mu_{i}=1$
and $p=0.25$ (in both cases $||\mu||^2$ is $25$). The simulated EDDs of the two cases are almost the same.

{\it Please, provide standard deviations obtained in experiments (as in Fig. 7).}

\vspace{0.1in}\noindent{\bf Author Response}: We have added error bars of EDDs as long as the plots are still clear. For other plots with many curves and showing error bars become cumbersome, we instead commented on the standard deviations of the EDD curves in the caption of the figure.

\vspace{.1in}
{\it Additionally, 500 seems to be a very small number  of repetitions. As is known, in Monte Carlo simulations of jump detection procedures even
$10^4$ repetitions are advisable in order to obtain convincing results.}


\vspace{0.1in}\noindent{\bf Author Response}: Thanks for the suggestion. We have re-run all the experiments with $10^4$ repetitions.


\vspace{0.1in}
{\it The analysis provided by the paper is restricted to the behavior of the methods in the mean sense.
Some results (at least comments) about variance of ARL's and  EDD's using various projections are necessary.}

\vspace{0.1in}\noindent{\bf Author Response}: We have additionally reported the standard deviation of ARL's and EDD's in all of our numerical examples.

\vspace{0.1in}
{\it Furthermore, some comments on large  $N$ (but finite) properties of the presented methods are desirable.}

\vspace{0.1in}\noindent{\bf Author Response}: Although our theoretical analysis is performed in the asymptotic regime when $N$ tends to infinity, however, we verified from our numerical examples that these results are also highly accurate in the large $N$ (but finite) regime, as demonstrated in Table I and Table IV. Therefore, these theoretical results are also of practical uses for determining the thresholds (analogous to the control limits of SPC). We have made this point clear in the revised paper.

\vspace{0.1in}
{\it Other comments.}
\begin{enumerate}
\item {\it p.7 l. 31 p ? or r}

\noindent{\bf Author Response}: It should be $r$. Fixed.

\item {\it p7 (2-nd column) l.35 (and also p.9 l. 50 ) N ? or  M - it seems that variance is not important in this case, at least from the theoretical point of view.}

\noindent{\bf Author Response}: The variance of the entries of the random Gaussian projection matrices is $1/N$ in all the experiments. We agree with the reviewer that, indeed, variance of the entries of the Gaussian random matrix does not matter. As shown in Section III.A, our procedure is equivalent to projecting the mean vector by a random subspace $V$, which is independent of the variance of the entries of the projection Gaussian matrix.

\item {\it Provide the smallest value of $p$ used in experiments depicted in Fig. 4 and Fig. 5.}

\noindent{\bf Author Response}: Done. The smallest value of $p$ used in the examples is 0.05.

\item {\it Make the title of subsection C (p. 8)  more precise}

\noindent{\bf Author Response}: We have changed it to ``time-varying projection by 0-1 matrices'' since we focus on this case.

\item {\it Provide ${\rm EDD}_{0}$`s in Tables II and III.}

\noindent{\bf Author Response}: Done.
\end{enumerate}

\clearpage

\section*{Response to Reviewer 2}

{\bf Author Response}: We appreciate the reviewers' thorough reading and thoughtful comments, for providing many insights and great suggestions, which helps us greatly improve the quality of the paper.

\vspace{0.1in}

{\it The paper discusses a problem of a structure break point detection for multivariate vectors of observations. The authors proposed here two approaches. The first one is based on the fixed projection method while the second one on the time-varying projections. In both cases the gaussianity is assumed. The structure break point detection is very timely topic because in many real signals before the further analysis the primary segmentation should be made. This is especially important in case of multivariate signals. In my opinion the paper could be accepted for publication in IEEE Transactions on Signal Processing however few aspect should be taken under consideration:}

\begin{enumerate}

\item {\it Page 2: authors mention the current paper is ``build on the preliminary work reported in [4].'' In my opinion it should be highlighted in what sense the paper extends the results of [4].}

{\bf Author Response}: We assume the reviewers meant ``... in [1]'', which is our GlobalSIP conference paper. The GlobalSIP paper contains initial ideas of this paper, as well as partial theoretical results such as ARL and EDD approximations for the fixed projections. The current paper provides complete proofs for these results. In addition, the new results include (1) time-varying projection and theoretical as well as numerical studies of its performance; (2) more extensive numerical results to show the accuracy of our theoretical results; (3) we included new real-data examples. We have stated these contributions explicitly in the introduction.


\item {\it It should be highlighted what is here new, what is the novelty. It is difficult to find which result is new and which is taken from the literature.}

{\bf Author Response}: We have made the novelty and contributions of our paper more explicit in the introduction and added comparisons with related SPC methods. The novelty of the paper is to present a new sequential change-point detection procedures using linear projections of the original data. We derive the generalized likelihood ratio procedure for such scenario assuming both the change-point location and the post-change mean vectors are unknown and perform rigorous statistical analysis of its performance. We consider time-varying projection and 0-1 matrices, which have not been considered in the SPC literature. The performance analysis for these two cases are also completely new and are not taken from literature. We also study the amount of dimensionality reduction that can be performed with little performance loss.

\item {\it Page 2: authors argue that those methods can be used also for dependent data for which the covariance matrix is known. What about the case when the covariance matrix is not given? This point should be discussed.}

{\bf Author Response}: We have moved the comments about covariance matrix into Section VI, Summary and Discussions.

In practice, when covariance matrix is not known, it can usually be estimated using a training stage. There are many techniques for covariance matrix estimation with proper regularization, and we have provided a reference which is a recent survey paper on the state-of-the-art of covariance matrix estimation: \\
Fan, J., Liao, Y. and Liu, H. (2016).
An overview on the estimation of large covariance and precision matrices.
Econometrics Journal, 19, C1--C32.

Moreover, we have added a comment regarding to estimating the covariance of the sketches directly and build a procedure taking into account of such estimated covariance. This may be easier since the covariance matrix of the sketches is of much smaller dimension than the covariance matrix of the original data.



\item {\it Page 3: please define the ratio statistic and generalized ratio statistic. They are not given by explicit forms, see for example D. Montiel, and H. Cang, and and H. Yang. Quantitative characterization of changes in dynamical behavior for single-particle tracking studies. The Journal of Physical Chemistry B, 110(40):19763-19770, 2006.}

{\bf Author Response}: The likelihood ratio statistic (assuming a post-change mean vector $\mu$) is given in equation (6) for our hypothesis testing model. The generalized ratio statistic (GLR) is derived by replacing the unknown post-change mean vector $\mu$ by its maximum likelihood estimator (MLE) for fixed $k$ and $t$, as derived from equation (7) to equation (9). The expression for log-GLR is given by equation (9). We have made them more explicit in the text. We have also added the suggested reference as one example of the log-LR and log-GLR statistic.


\item {\it Page 3: the singular value decomposition is mentioned. Please give the main idea of this method or even some references.}

{\bf Author Response}: We have added the main idea of SVD based method,
``There are also methods using principal component analysis (PCA) of data streams for change-point detection (e.g., [19], [26]). The main idea is to extract the several principle components of the data streams in a sliding time window, and performing change detection (e.g., using SPC control chart a CUSUM procedure) with these principle components." 

\item {\it Page 9: It is not obvious that the threshold $b$ is chosen such that ARL is 5000. It should be explained.}


{\bf Author Response}: Other than obtaining $b$ from theorem 1 or equation (29), we usually use simulation to find threshold $b$. We fix a threshold $b$ and then run the detection procedures on standard multivariate normal processes. Then we can obtain a simulated ARL for each fixed $b$. Since the ARL is increasing with $b$, we apply a bisection search to obtain a threshold such that the simulated ARL is $5000$.


\item {\it In both analyzed vectors of observations the gaussianity should be proved for the data. For example the authors can use some statistical test to prove this.}

{\bf Author Response}: We have added the verification for the solar flare data, using one-sample Kolmogorov-Smirnov(KS) test and QQ plot(shown in Fig. 10). In other examples, the signals are generated from normal distribution.

\item {\it Page 9-10: The statement ``When the power network is in a normal condition, the signals are i.i.d. standard Gaussian distribution'' should be explained deeper. Why is it true? Some references should be added to this point.}

{\bf Author Response}: When the power system is in the steady state, the signals usually correspond to a true state plus Gaussian observation noise. We have added the following reference:

Abur, Ali, and Antonio Gomez Exposito. Power system state estimation: theory and implementation. CRC press, 2004.

The true state can be estimated using the techniques in the above reference. Once we have estimated the states, we can subtract that from the observations and examine the residual as the signals $x_t$. Hence, $x_t$ can be assumed to be Gaussian distributed with zero mean. We have made this clear in the text.

\item {\it Page 10: Once again, why the threshold is taken such that ARL is 5000? This is not clear.}

{\bf Author Response}: In the sequential change-point detection literature, the ARL is usually set to a large number, and 5000 or 10000 is typically chosen. This is similar to that in the statistics literature, significance level is usually chosen to be 0.05 or 0.01. In practice, however, how large ARL should be depends on the sampling rate and how much false-alarms we can tolerate (e.g., having false alarm once a month or once a year). We have added in the revised paper the following

``In the subsequent examples, we select ARL to be 5000 to represent a low rate of false detection (similar choice were make in other sequential change-point detection work such as [5]). In practice, however, the target ARL value depends on sampling rate and how frequent we can tolerate false detection (once a month or once a year).''

\item {\it In the literature one can find many methods for structure break pint detection. In my opinion in this paper there should be proved or highlighted that the proposed approach is better than other classical approaches.}

{\bf Author Response}: We have make our contributions more explicit in the introduction. In addition, we have added one numerical example to compare our sketching procedure to a classic method. The classic method we consider is to apply the multivariate CUSUM method directly on $M$ sketches. The multivariate CUSUM method is believed to be one of the most powerful methods in change detection.

The multivariate CUSUM method is in the following reference:\\
Woodall, William H., and Matoteng M. Ncube. ``Multivariate CUSUM quality-control procedures." Technometrics 27.3 (1985): 285-292.

In Section V.D,  we explain the difference of our method with the classic method that we compare with. The comparison results are shown in Fig. 8(a) and Fig. 8(b): with the same ARL, our method has smaller EDD than this classical method.

\end{enumerate}

\clearpage

\section*{Response to Reviewer 3}

{\bf Author Response}: We appreciate the reviewers' thorough reading and thoughtful comments, for providing many insights and great suggestions, which helps us greatly improve the quality of the paper.

\vspace{0.1in}

{\it
This is an interesting paper that studies the sequential change-point detection problem for the case where Gaussian vector observations undergo only an unknown shift in mean at the change point. The new aspect of the paper is that the vector observations are first subject to a projection to a lower dimensional space (or sketching) before they are used for decision-making. The paper establishes theoretically that there is no loss in performance, characterized through a tradeoff between expected detection delay (EDD) and average run length (ARL), due to sketching as long as the sketching matrix satisfies certain reasonable conditions. These theoretical results are backed by numerical results relevant to applications such as solar flare detection and failure detection in power networks.

The paper is well-written for the most part. }



\vspace{0.1in}

{\bf Author Response}: Thank you.

\vspace{0.1in}


{\it


However, there is one possibly major flaw in the paper that keeps me from recommending acceptance. If the authors can correct this flaw, I believe it will be a good paper worthy of publication in TSP.

\vspace{0.1in}

Major Error:

One of the main results of the paper is that when the sketching matrix A is a Gaussian random matrix, there is no loss in detection performance asymptotically as the threshold b goes to infinity. This result follows from the approximation given in equation (23) of the paper. Now let us examine the steps that lead to (23) carefully. The authors first note that EDD is proportional to the test threshold b (see (21)), which is allowed to be different for the detector using the original data (N) and that using the reduced data (M). Then, they invoke the fact that ratio of b to M is being kept constant. If we call this constant c, and denote by b1 the threshold for the original data and b2 the threshold for the reduced data, then it must be true that b1/N = b2/M, from which they conclude that b1/b2 = N/M, which results in the factor of N/M in (23).

\vspace{0.1in}


The problem with the above argument is that since the thresholds b1 and b2 are different for M different from N (in order for b1/b2 = N/M), the resulting ARLs for these tests will also be different. As the authors argue in the paragraph preceding Remark 4, the ARL will be of the form $\exp (b_1 {\rm const.})$ for the original data and of the form $\exp (b_2 {\rm const.})$ for the reduced data. One can only compare the EDD?s for the two cases if the ARLs are maintained to be same, but in this case the differ by a factor N/M inside the exponent!

\vspace{0.1in}


The authors could consider varying the threshold b with M in such a way that the ARL is kept constant as b goes to infinity. In this case eq (23) would be modified to something considerably more complicated. Perhaps the authors can still establish that there is little loss in performance due to sketching using the new version of (23). In that case, I believe their paper would be worthy of publication.}

\vspace{0.1in}

{\bf Author Response}: Thank you for pointing out this issue. We have found a different way to establish the result that there is little loss in performance due to sketching. The new argument, in Remark 3, avoids the original confusing statements. We have also added a new figure, Fig. 1, to explain this.

We agree with the reviewer. In the new argument, to compare the performance across different $M$, we fix ARL to be a constant. We first find out from Theorem 1 that to maintain a constant ARL, the threshold $b$ should be linearly proportional to $M$. Then using Theorem 1, we verify this numerically by finding the $b$ that corresponding to a constant ARL for each value of $M$. (In Section V, we have verified that Theorem 1 is a very accurate approximation.) The numerical result in Fig. 1 shows that the linear approximation works well.


Then the rest of the argument follows as before, as explained in Remark 4. The EDD is proportional to $2b/\|V^\intercal \mu\|^2$, and hence $EDD(N)/EDD(M)$ is proportional to $(N/M)\cdot \Gamma$. This will lead to the same conclusion as we had before.






%\bibliographystyle{plain}
%\bibliography{AuthorResponse,slope_letter}

\end{document}
