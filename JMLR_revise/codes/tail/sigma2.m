function sigma2 = sigma2(X,bandw)

[~,L] = size(X);

h = zeros(floor(L/8),1);
  
       for j = 1:floor(L/8)
           Y = X(:,(j-1)*8+1:j*8);
           
           D = zeros(8,8); % D is the pairwise distance matrix
           
          for i = 1:8
           
              temp = bsxfun(@minus,Y(:,(i+1):8),Y(:,i));
              
              D(i,(i+1):8) = sum(temp.^2,1);
          end
     
         
         D=D+D';
         
         Kxx = exp(-1/2/bandw * D);
           
           
         h1 = Kxx(1,2) + Kxx(6,7) - ...
             Kxx(1,6) - Kxx(2,7) ;
         h2 = Kxx(2,3) + Kxx(7,8) - ...
             Kxx(2,7) - Kxx(3,8) ;
         h3 = Kxx(4,5) + Kxx(6,8) - ...
             Kxx(4,6) - Kxx(5,8) ;
         
         h(j) = h1*h2*h3;  
       end
       
       sigma2 = mean(h);


end