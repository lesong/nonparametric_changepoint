

clear
close all

format long

N=5;
M=150;

load sequence_1.mat

L = length(sequence);
mu = mean(sequence);
sigma = std(sequence);
sequence = (sequence - mu) ./ sigma;
%sequence = sequence'./100;
seqCut = sequence(1:1500);

for k =1:50
    
X_s =datasample(seqCut, 5000);
r = 0.1;
bandw = r * bandw1(X_s);
S_var = est_var (X_s, bandw, M, N);


%% compute sigma_2_sq 

sigma_2_sq=hyy(X_s,bandw);
sigma_4_sq=hxxyy(X_s,bandw);
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
sigma = est_skew(X_s, bandw, M, N);

 
%% 
outcome = [];
for b= 1: 0.01: 7
    
  
  
Pr=0;

for B=2:M
    
    % compute exponential part
    var = 2/B/(B-1)*C;
    C1 =  8.*(B-2)./B./B./(B-1)./(B-1);
    C2 = 4./B./B./(B-1)./(B-1);
    S_skew = C1*((1/N/N)*sigma(1) + (3*(N-1)/N/N)*sigma(2) + ((N-1)*(N-2)/N/N)*sigma(3) )...
     + C2*((1/N/N)*sigma(4) +(3*(N-1)/N/N)*sigma(5) + ((N-1)*(N-2)/N/N)*sigma(6) );
                                                                                
    k3 = var^(-3/2)*S_skew;
    theta = ( -1 + sqrt(2*k3*b+1))/k3;
    
    temp1 = b*exp( theta^2./2 + k3* (theta)^3/6 -theta.*b);
    temp2 = b*sqrt((2*B-1)/B/(B-1));
    temp2 = fv(temp2); 
    temp3 = (2*B-1)/2/sqrt(2*pi)/B/(B-1);
    
    Pr=Pr+temp1*temp2*temp3;
    
end
 outcomeTemp = [b,Pr];
 outcome = [outcome;outcomeTemp];

end

L = size(outcome,1);
for i = 1:L
    
    if outcome(i,2) > 0.10 && outcome(i+1,2) < 0.10
        threshold1(k) = outcome(i,1);
    end
    
    
    if outcome(i,2) > 0.05 && outcome(i+1,2) < 0.05
        threshold2(k) = outcome(i,1);
    end
    
    if outcome(i,2) > 0.01 && outcome(i+1,2) < 0.01
        threshold3(k) = outcome(i,1);
    end
    
end

fprintf('--iteration%d--threshold1%f--threshold2%f--threshold3%f\n', k, threshold1(k),threshold2(k),threshold3(k))
end
max(threshold1)
max(threshold2)
max(threshold3)



