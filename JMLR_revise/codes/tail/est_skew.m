% compute the third moment

function S_skew = est_skew(X, bandw, M, N)

[~,L] = size(X);

  
       for j = 1:floor(L/9)
           
           Y = X(:,(j-1)*9+1:j*9);
           
           D = zeros(9,9); % D is the pairwise distance matrix
           
          for i = 1:9
           
              temp = bsxfun(@minus,Y(:,(i+1):9),Y(:,i));
              
              D(i,(i+1):9) = sum(temp.^2,1);
          end
     
         
         D=D+D';
         
         Kxx = exp(-1/2/bandw * D);
           
           
         h1 = Kxx(1,2) + Kxx(7,8) - ...
             Kxx(1,7) - Kxx(2,8) ;
         h2 = Kxx(2,3) + Kxx(8,9) - ...
             Kxx(2,8) - Kxx(3,9) ;
         h3 = Kxx(1,3) + Kxx(7,9) - ...
             Kxx(1,7) - Kxx(3,9) ;
         h4 = Kxx(4,5) + Kxx(7,9)-...
             Kxx(4,7)-Kxx(5,9);
         h5 = Kxx(3,4) + Kxx(8,9)-...
             Kxx(3,8)-Kxx(4,9);
         h6 = Kxx(5,6) + Kxx(7,9)-...
             Kxx(5,7)-Kxx(6,9);
         h8 = Kxx(3,4) + Kxx(7,8)-...
             Kxx(3,7)-Kxx(4,8);
         h9 = Kxx(5,6) + Kxx(7,8)-...
             Kxx(5,7)-Kxx(6,8);
         
         
         sigma1(j) = h1*h2*h3;
         sigma2(j) = h1*h2*h4;
         sigma3(j) = h1*h5*h6;
         sigma4(j) = h1^3;
         sigma5(j) = (h1^2)*h5;
         sigma6(j) = h1*h8*h9;
         
         
         
       end
       
       S_skew(1) = mean(sigma1);
       S_skew(2) = mean(sigma2);
       S_skew(3) = mean(sigma3);
       S_skew(4) = mean(sigma4);
       S_skew(5) = mean(sigma5);
       S_skew(6) = mean(sigma6);

    
 

end
