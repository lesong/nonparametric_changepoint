% just estimate skewness

%% analytic computation

clear

close all


format long


B= 100;

mu_0=0; % the mean and variance under null
sigma_1=1;
  

%% estimate bandwidth

X_s=zeros(10,2000);
X_s(:,[1:2000])=normrnd(mu_0,sigma_1,[10,2000]);


%% calculate the kernel matrix
[ms,ns]=size(X_s);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,X_s(:,(i+1):ns),X_s(:,i));
    Ds(i,(i+1):ns)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw = median(Ds(Ds~=0));

%% compute sigma_2_sq 
for N = 1: 100

sigma_2_sq=hyy(X_s,bandw);
sigma_4_sq=hxxyy(X_s,bandw);
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
sigma = est_skew(X_s, bandw, B, N);

var = 2/B/(B-1)*C;
C1 =  8.*(B-2)./B./B./(B-1)./(B-1);
C2 = 4./B./B./(B-1)./(B-1);
S_skew = C1*((1/N/N)*sigma(1) + (3*(N-1)/N/N)*sigma(2) + ((N-1)*(N-2)/N/N)*sigma(3) )...
 + C2*((1/N/N)*sigma(4) +(3*(N-1)/N/N)*sigma(5) + ((N-1)*(N-2)/N/N)*sigma(6) );

k3 = var^(-3/2)*S_skew

   
end
 