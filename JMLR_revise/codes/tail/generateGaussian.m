% generate sythetic data
clear
clc

Bmax = 150;
N = 10000;

% generate covariance matrix

Cmatrix = zeros((Bmax-1),(Bmax-1));

for i = 1 : (Bmax-1)
    for j = 1 : (Bmax-1)
        u = i+1;
        v = j+1;
        Cmatrix(i,j) = sqrt( (u*(u-1)/2) *(v*(v-1)/2))/ ( max(u,v)*( max(u,v)-1 )/2 ); 
    
    end
end

% generate multivariate gaussian

mu = zeros(1, (Bmax-1));

sample = mvnrnd(mu, Cmatrix, N);
sampleMax = max(sample,[], 2);
quantile(sampleMax, 0.90)
quantile(sampleMax, 0.95)
quantile(sampleMax, 0.99)
