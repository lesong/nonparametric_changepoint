% offline 


clear
close all


load reference.mat
load test.mat

N=5;
%M=200;
M=200;

%%

%% estimate bandwidth
bandw=bandw1(reference);

%% estimate variance

sigma_2_sq=hyy(reference, bandw);
sigma_4_sq=hxxyy(reference, bandw);

C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
B=2:1:M;
S_var=C*2./B./(B-1);

 %% compute kernel matrix    
 for i=1:1000
     
 Kxx_post=fKxx(data_test{i}, data_test{i},N,M,bandw,1);  % M by M
 Kxx_pre=fKxx(reference, reference,N,M,bandw,2);  %  N*M by M 
 Kxx_cross=fKxx(reference, data_test{i} ,N,M,bandw,3) ; % N*M by M


%% compute statistic
 
 B_stat=zeros(1,M-1);
 for B=2:M
     MMD=[];
             T=Kxx_post(M-B+1:M, M-B+1:M);
             temp1=1/B/(B-1)*sum(T(:));
         for j=1:N  
             A=Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C=Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             MMD(j)= 1/B/(B-1)*sum(A(:))+ temp1 - 2/B/(B-1)*sum(C(:));
         end 
         S=mean(MMD);
       
         B_stat(B-1)=S./sqrt(S_var(B-1)) ;
         %B_stat(B-1)=S;
 end
 stat(i)={B_stat};
 
 end
 
 
 
% b=3.34; % threshold
%figure
%subplot(2,1,1)
%plot(testing,'o');
%ylim([-10,10])
%subplot(2,1,2)
%plot(B_stat(end:-1:1)); hold on
%ylim([-1,8]);
%plot(1:0.01:500,b); hold on
%loc=find(B_stat==max(B_stat));
%plot(M-loc, -1:0.01:8)

%% threshold=3.34














