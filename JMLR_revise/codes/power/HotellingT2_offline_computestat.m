% compare with hotelling T^2

clear
close all


mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

%% Hotelling T

M=200; % M is the size of sliding window

K=100; 



%%  offline control chart

load data_test_case2_1.mat

  for t=1:K % total number of runs    
    
      Y=data_test{t}; % Y is testing data
      [m,n]=size(Y);
       
      
      %% initialize      
      % sample data
        
       index=1;
       % compute statistic
       
   while  (index < n-1) 
       index=index+1;
       
       data_pre=Y(:, 1:index);
       data_post=Y(:,index+1:n);
       
       mean_pre=mean(data_pre,2);
       mean_post=mean(data_post,2);
       
       
       
       temp_pre=bsxfun(@minus, data_pre, mean_pre);
       est_cov_pre=sum(bsxfun(@times, reshape(temp_pre,m,1,index), reshape(temp_pre, 1,m,index)),3);
       
       temp_post=bsxfun(@minus, data_post, mean_post);
       est_cov_post=sum(bsxfun(@times, reshape(temp_post,m,1,n-index), reshape(temp_post, 1,m,n-index)),3);
       
       est_cov=( est_cov_pre+est_cov_post)/(n-2);
       
       
       S_inv=inv(est_cov);
       
       temp=(mean_pre-mean_post)'*S_inv*(mean_pre-mean_post);

       T_2(index-1)=(index*(n-index)/n)*temp;
 
    end         

%fprintf(1, '--experiment no %d of %d\n', t, rep);
stat(t)={T_2};
end


         
 