%% GLR offline line

% for gaussian d=20

% compare with hotelling T^2

clear
close all


load  data_test_case5.mat % data_test 100samples
%% offline
[m,n]=size(data_test{1});

for t=1:100; 

       GLR=zeros(1,n);
       Y=data_test{t};
       mean_t=mean(Y,2);
       temp_t=bsxfun(@minus, Y, mean_t);
       est_cov_t=mean(bsxfun(@times, reshape(temp_t,m,1,n), reshape(temp_t, 1,m,n)),3);
       temp1=n*log(det(est_cov_t));
     
       for   index=m+1+5:1:(n-m-1)-5
       
  % while  (20<=index)&&(index<= n-22) 
       %index=index+1;
       
       data_pre=Y(:, 1:index);
       data_post=Y(:,index+1:n);
       
       mean_pre=mean(data_pre,2);
       mean_post=mean(data_post,2);
       
       
       
       temp_pre=bsxfun(@minus, data_pre, mean_pre);
       est_cov_pre=mean(bsxfun(@times, reshape(temp_pre,m,1,index), reshape(temp_pre, 1,m,index)),3);
       
       temp_post=bsxfun(@minus, data_post, mean_post);
       est_cov_post=mean(bsxfun(@times, reshape(temp_post,m,1,n-index), reshape(temp_post, 1,m,n-index)),3);
       
       
       
       GLR(index-1)=temp1-index*log(det(est_cov_pre))-(n-index)*log(det(est_cov_post));
       
       
 
       end         

    stat(t)={GLR};
end

