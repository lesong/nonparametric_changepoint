function [K]=fKxx(A,B,N,M,bandw,flag )
    
% one dimension
% we use row index to refer to pre-data and 
% column index to refer to post-data
% Here, we use A refer to pre-data and
% B refer to post-data



switch flag
    
    
    case 1  %  Kxx_post=fKxx(X(:, index-M+1: index), X(:, index-M+1: index),1);  
        
        
        D=[];
        for i=1:M
            temp=bsxfun (@minus, A(:,(i+1):M), A(:, i)); 
            %D(i,(i+1):M)=temp.*temp;
             D(i,(i+1):M)=sum(temp.^2,1);
            %D((i+1):M,i)=dot(temp,temp)';
        end

          D=D+D'; % D is the pairwise distance matrix

          K = exp(-D/2/bandw);
          K(logical(eye(size(K))))=0; % set the diagonal entries 0
        
        
    case 2  % Kxx_pre=fKxx(data_pre, data_pre,2); 
        
          K=[];
        
        
        for j=1:N
            
            D=[];
 
            for i=1:M
                temp=bsxfun (@minus, A(:,(j-1)*M+i+1 : j*M ), A(:, (j-1)*M+i));
                %D(i,(i+1):M)=temp.*temp;
                %D((i+1):M,i)=dot(temp,temp)';
                 D(i,(i+1):M)=sum(temp.^2,1);
            end

            D=D+D';
    
          temp_1=exp(-1/2/bandw * D);
          temp_1(logical(eye(size(temp_1))))=0; % set the diagonal entries 0
   
          K=[K;temp_1];
        end
            
              
        
    case 3
        
         K=[];
         
         for j=1:N
             D=[];
              for i=1:M
                  temp=bsxfun (@minus, A(:,(j-1)*M+1 : j*M), B(:, i));
                  %D(i, 1:M)=temp.*temp;
                  %D(1:M,i)=dot(temp,temp)';
                   D(i,1:M)=sum(temp.^2,1);
                
              end
         
           temp_1=exp(-1/2/bandw * D);
           temp_1(logical(eye(size(temp_1))))=0; % set the diagonal entries 0
   
        % apply RBF and obtain kernel matrix Kxx
          K=[K;temp_1];
         end
        
        
         
    case 4  % temp=fKxx(X(:,index-M+1:index), X(:, index),N,M,4); % M by 1
        
        K=[];
        
         temp=bsxfun (@minus, A, B);
         D=dot(temp,temp)';
         K=exp(-1/2/bandw * D);
         K(M)=0;
         
       
    case 5
         K=[];
        
         temp=bsxfun (@minus, A, B);
         D=dot(temp,temp)';
         K=exp(-1/2/bandw * D);
         r=M:M:N*M;
         K(r)=0;
        
end

