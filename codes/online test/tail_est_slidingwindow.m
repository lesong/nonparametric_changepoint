%% analytic computation

clear
close all
format long

%% 
M=10; %M is the maximum block size 
T=500; % T is the time

b=4.5:0.05:5;
L=length(b);
output=zeros(L,2);
j=1;

for b=4.5:0.05:5

Pr=0;
temp1=exp(-0.5*(b^2));
temp2=1/sqrt(2* pi);

 for B=2:M
     
    
    
    temp3=b*sqrt((2*B-1)/B/(B-1));
    temp4=fv(temp3);
    temp5=(b^2)*(2*B-1)/2/B/(B-1);
    temp6=(temp4*temp5)^2;
    
    Pr=Pr+temp6*(1-B/T);
     
 end
 
 Pr=Pr*T*temp1*temp2;

output(j,1)=Pr;

output(j,2)=b;
j=j+1;

end
output

%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2
