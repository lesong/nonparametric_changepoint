clear
%tail estimate new


%% analytic computation

clear
close all
format long

%% 
M=5; %M is the maximum block size 
T=500; % T is the time

b=4.5;


Pr=0;
temp1=exp(-0.5*(b^2));

for B=2:M
    
    
    temp2=b*sqrt((2*B-1)/B/(B-1));
    temp2=fv(temp2);
    temp3=(b^2)*(2*B-1)/2/B/(B-1);
    Pr=Pr+temp2*temp3;
    
end


for t=M+1:1:T
     for k=t-M:1:t-2
    
    temp2=b*sqrt((2*(t-k)-1)/(t-k)/((t-k)-1));
    temp3=fv(temp2);
    temp4=(b^2)*(2*(t-k)-1)/2/(t-k)/((t-k)-1);
    temp5=(temp3*temp4)^2;
   
    Pr=Pr+temp5;
     end
end
 
Pr=Pr*temp1./sqrt(2* pi);

output(1,1)=Pr;

output(1,2)=b;


output

%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2
