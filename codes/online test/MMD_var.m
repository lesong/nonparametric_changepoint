%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point

B=5; % M is the maxima block size

%% estimate bandw
X=zeros(20,500);
X(:,[1:500])=normrnd(mu_0,sigma_1,[20,500]);

       %% calculate the kernel matrix
      [m,n]=size(X);
       D=zeros(m,n); % D is the pairwise distance matrix
       for i=1:n
       temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
       D(i,(i+1):n)=dot(temp,temp);
       end
   % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));


%% esimate variance of MMD for the offline case

repeatno = 2000; 
MMD=zeros(1,repeatno);

   for j= 1:repeatno
       
       X=zeros(20,B);
       X(:,[1:B])=normrnd(mu_0,sigma_1,[20,B]);
       A=fKxx1(X,X,B,bandw,1);
       
       Y=zeros(20,B);
       Y(:,[1:B])=normrnd(mu_0,sigma_1, [20,B]);
       T=fKxx1(Y,Y,B,bandw,1);
       
       C=fKxx1(X,Y,B,bandw,2);

       MMD(j)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
       
   end


MMD_v_off=var(MMD)

%%

MMD=zeros(1,repeatno);


X=zeros(20,B);
X(:,[1:B])=normrnd(mu_0,sigma_1,[20,B]);
A=fKxx1(X,X,B,bandw,1);

for j=1:repeatno
    
       Y=zeros(20,B);
       Y(:,[1:B])=normrnd(mu_0,sigma_1, [20,B]);
       T=fKxx1(Y,Y,B,bandw,1);
       
       C=fKxx1(X,Y,B,bandw,2);

       MMD(j)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));

end

 MMD_v_on=var(MMD)