%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

%% Exponential Case
mu_0=1; % the mean before the change point
mu_1=5; % the mean after the change point

X=zeros(20,2000);
X(:,[1:1000])=exprnd(mu_0,[20,1000]);
X(:,[1001:2000])=exprnd(mu_1,[20,1000]);

%% Gaussian Case
%  X_1 ~ Gaussian (mu_0,sigma_1)   
%  X_2 ~ Gaussian (mu_1,sigma_2)

%mu_0=0; % the mean before the change point
%mu_1=0; % the mean after the change point
%sigma_1=1; % the standard deviation before the change point
%sigma_2=5; % the standard deviation after the change point

%X=zeros(20,2000);
%X(:,[1:1000])=normrnd(mu_0,sigma_1,[20,1000]);
%X(:,[1001:2000])=normrnd(mu_1,sigma_2,[20,1000]);

%% calculate the kernel matrix
[m,n]=size(X);
D=zeros(m,n); % D is the pairwise distance matrix
for i=1:n
    temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
    D(i,(i+1):n)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(D(D~=0));
D=D+D';
% apply RBF and obtain kernel matrix Kxx
Kxx = exp(-1/2/bandw * D);
Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
% plot colormap for kernel matrix
figure
imagesc(Kxx);

%% CUSUM_B_test
W=300; % W is the length of sliding window

B=100; % B is the block size
B_statistic=[];

num_block=floor(W/B); % the totoal number of blocks

for i=1:n-W % the start point of sliding window
    S=[];
    for j=1:num_block-1
        
       A=Kxx(i+(j-1)*B:i+j*B,i+(j-1)*B:i+j*B);
       T=Kxx(i+(num_block-1)*B:i+num_block*B,i+(num_block-1)*B:i+num_block*B);
       C=Kxx(i+(j-1)*B:i+j*B,i+(num_block-1)*B:i+num_block*B);
       C(logical(eye(size(C))))=0;
       S(j)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
    end
     B_statistic(i)=mean(S);
end

figure
plot(1:n-W, B_statistic,'-bo','LineWidth',1,'MarkerSize',6); 
xlabel('Time','FontSize',13);
ylabel('B Statistic','FontSize',13);  


