% Monte Carlo 

% B test
% With sliding windows
clear 
clc
close all

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point


%% CUSUM_B_test
N=3; % N is the total number of blocks
M=5; % M is the upper bound for size of blocks
%B=2:M;

b=0.5; % b is the threshold
rep=100; 
freq=0;



for t=1:rep % total number of runs
    
      flag=1; % once cross the threshold, set the flag=0
    
    
      %% initialize
      
      % sample data
       
       X=zeros(20,1000);
       X(:,[1:1000])=normrnd(mu_0,sigma_1,[20,1000]);
       [m,n]=size(X);
     
       % we need to remember and update data_pre
       data_pre=X(:,1:N*M ); 
       index=(N+1)*M; % index refers to the new coming data
      
      
      %  compute Kxx_post, Kxx_pre and Kxx_cross
       
      Kxx_post=fKxx(X(:, index-M+1: index), X(:, index-M+1: index),N,M,1);  % M by M
      Kxx_pre=fKxx(data_pre, data_pre,N,M,2);  %  N*M by M 
      Kxx_cross=fKxx(data_pre, X(:, index-M+1: index) ,N,M,3) ; % N*M by M
    
      Kxx_pre_update=Kxx_post;
      
      %% realize online testing
  
   while (flag==1) && (index < n)    
   
       S=[];
       for B=2:M
         
          MMD=[];
         for j=1:N  
             T=Kxx_post( M-B+1:M,M-B+1:M  );
             A=Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C=Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             MMD(j)= 1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
         end 
         S(B-1)=mean(MMD);
       end
     
     B_stat=max(S);
     
     if B_stat >= b
         freq=freq+1;     
         flag=0;
     end
      
     
   
           index=index+1; % index is the end of sliding window
     
           % given new data, we need to update Kxx_post, Kxx_pre, and Kxx_cross
             
           Kxx_post(1:M-1,1:M-1)=Kxx_post(2:M,2:M);
           temp=fKxx(X(:,index-M+1:index), X(:, index),N,M,4); % M by 1
           Kxx_post(:,M)=temp;
           Kxx_post(M,:)=temp';
           
           
           % we can introduce mod to remember which column to update
           r=mod(index,M);
          
           if r==0
               r=r+M;
           end
           
           Kxx_cross(:,r)=fKxx(data_pre, X(:,index),N,M,5); % N*M by 1
           
            
           % update Kxx_post
           
           if  mod(index, M)==0 
               
               Kxx_pre=[Kxx_pre(M+1:N*M,:);Kxx_pre_update];
               data_pre=X(:,index-(N+1)*M+1:index-M);
               Kxx_pre_update=Kxx_post;
           end
               
          
               
               
   end
  
   fprintf(1, '--experiment no %d of %d\n', t, rep);
   
end         

freq/rep
         
 