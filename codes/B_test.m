%% CUSUM kernel change point detection
clear 
clc
close all

%% generate data X 

%% Exponential Case
mu_0=1; % the mean before the change point
mu_1=5; % the mean after the change point

X=zeros(20,1000);
X(:,[1:500])=exprnd(mu_0,[20,500]);
X(:,[501:1000])=exprnd(mu_1,[20,500]);

%% Gaussian Case
%  X_1 ~ Gaussian (mu_0,sigma_1)   
%  X_2 ~ Gaussian (mu_1,sigma_2)

%mu_0=0; % the mean before the change point
%mu_1=0; % the mean after the change point
%sigma_1=1; % the standard deviation before the change point
%sigma_2=5; % the standard deviation after the change point

%X=zeros(20,1000);
%X(:,[1:500])=normrnd(mu_0,sigma_1,[20,500]);
%X(:,[501:1000])=normrnd(mu_1,sigma_2,[20,500]);

%% calculate the kernel matrix
[m,n]=size(X);
D=zeros(m,n); % D is the pairwise distance matrix
for i=1:n
    temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
    D(i,(i+1):n)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(D(D~=0));
D=D+D';
% apply RBF and obtain kernel matrix Kxx
Kxx = exp(-1/2/bandw * D);
Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
% plot colormap for kernel matrix
figure
imagesc(Kxx);

%% CUSUM_B_test
B=100; % B is the block size
B_statistic=[];
num_block=floor(n/B); % the totoal number of blocks

B_statistic(1)=0;
for t=2:num_block-1 % t is the start of change point (last block)
    S=[];
    for j=1:t-1  % j is the block index of reference data
        A=Kxx((j-1)*B+1:j*B,(j-1)*B+1:j*B );
        T=Kxx((t-1)*B+1:t*B,(t-1)*B+1:t*B );
        C=Kxx((j-1)*B+1:j*B,(t-1)*B+1:t*B );
        C(logical(eye(size(C))))=0;
        S(j)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
    end
    B_statistic(t)=mean(S);
end
  
figure
plot(1:B:(length(B_statistic)-1)*B+1, B_statistic,'-bo','LineWidth',1,'MarkerSize',6); 
xlabel('Time','FontSize',13);
ylabel('B Statistic','FontSize',13);   
    
    
    



















