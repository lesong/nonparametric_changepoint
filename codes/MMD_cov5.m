%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point



B=10; % M is the maxima block size
k=5;
repeatno = 2000; 
MMD=zeros(2,repeatno);




   for j= 1:repeatno
       
       X=zeros(20,3*(B));
       X(:,[1:3*(B)])=normrnd(mu_0,sigma_1,[20,3*(B)]);

       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
       temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
       D(i,(i+1):n)=dot(temp,temp);
       end
   % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));
      D=D+D';
   % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
       
       A=Kxx(1:B, 1:B);
       T=Kxx(n-B+1:n, n-B+1:n);
       C=Kxx(1:B, n-B+1:n);
       C(logical(eye(size(C))))=0;
       MMD(j,1)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
      
      
       A=Kxx(B+1:B+B-k+1, B+1:B+B-k+1); 
       T=Kxx(n-B+k+1:n, n-B+k+1:n);
       C=Kxx(B+1:B+B-k+1,n-B+k+1:n);
       C(logical(eye(size(C))))=0;
       MMD(j,2)=1/(B-k)/(B-k-1)*sum(A(:))+1/(B-k)/(B-k-1)*sum(T(:))-2/(B-k)/(B-k-1)*sum(C(:));
              
       
   end
   
   MMD_cov=(1/(repeatno-1))*(MMD(:,1)-mean(MMD(:,1)))'*(MMD(:,2)-mean(MMD(:,2)));


MMD_cov

% choose L data to estimate
for L=1000:1000: 5000;
h=zeros(floor(L/6),2);

   for j=1:floor(L/6)
           
           
       X=zeros(20,6);
       X(:,[1:6])=normrnd(mu_0,sigma_1,[20,6]);
       s = rng;
       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
       end
     % use rule of thumb to determine the bandwidth
        bandw=median(D(D~=0));
        D=D+D';
    % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      
           
   
      h(j,1)=Kxx(1,2)+Kxx(5,6) - ...
             Kxx(1,5) -Kxx(2,6) ;
      h(j,2)=Kxx(3,4)+Kxx(5,6) - ...
             Kxx(3,5) -Kxx(4,6) ;
end
       
       MMD_covest=(2/(B)/(B-1))*(1/(floor(L/6)-1))*(h(:,1)-mean(h(:,1)))'*(h(:,2)-mean(h(:,2)));
      
       %MMD_covest=(2/B/(B-1))*mean((mean(h,2).^2));

MMD_covest
    
end

    
    
    
    
    
    
    
    
    


