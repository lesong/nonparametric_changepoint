% Monte Carlo 

% B test
% With sliding windows
clear 
clc
close all

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point


%% CUSUM_B_test
N=20; % N is the total number of blocks
M=10; % M is the upper bound for size of blocks


rep=1000;
B_stat=zeros(rep,1);


%% estimate bandwidth

X_s=zeros(20,500);
X_s(:,[1:500])=normrnd(mu_0,sigma_1,[20,500]);

[ms,ns]=size(X_s);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,X_s(:,(i+1):ns),X_s(:,i));
    Ds(i,(i+1):ns)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(Ds(Ds~=0));



sigma_2_sq=0.004574751923643;
sigma_4_sq=0.018622680903357;
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
S_var=zeros(1,M-1);
for i=1:1:M-1
    S_var(i)=C*2/(i+1)/(i);
end


  for t=1:rep % total number of runs
    
      
      % sample data
       
       X=zeros(20,(N+1)*M);
       X(:,[1:(N+1)*M])=normrnd(mu_0,sigma_1,[20,(N+1)*M]);
       [m,n]=size(X);
     
       % we need to remember and update data_pre
       data_pre=X(:,1:N*M ); 
       index=(N+1)*M; % index refers to the endpoint of sliding window
      
      
      %  compute Kxx_post, Kxx_pre and Kxx_cross
       
      Kxx_post=fKxx(X(:, index-M+1: index), X(:, index-M+1: index),N,M,bandw,1);  % M by M
      Kxx_pre=fKxx(data_pre, data_pre,N,M,bandw,2);  %  N*M by M 
      Kxx_cross=fKxx(data_pre, X(:, index-M+1: index) ,N,M,bandw,3) ; % N*M by M
    
      
      %% estimate variance
      %sigma_2_sq=hyy(X,bandw);
      %sigma_4_sq=hxxyy(X,bandw);
      %C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
      %S_var=zeros(1,M-1);
      %for i=1:1:M-1
      %S_var(i)=C*2/(i+1)/(i);
      %end
      
   
       S=[];
       for B=2:M
         
          MMD=[];
         for j=1:N  
             T=Kxx_post( M-B+1:M,M-B+1:M  );
             A=Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C=Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             MMD(j)= 1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
         end 
         S(B-1)=mean(MMD);
       end
     %S./sqrt(S_var)
     B_stat(t)=max((S./sqrt(S_var)));
     
          
               
  end
output=zeros(20,2);
j=1;
   for q=0.8:0.01:0.99
    output(j,1)=q;
    output(j,2)=quantile(B_stat,q);
    j=j+1;
   end
   output
  

         
 