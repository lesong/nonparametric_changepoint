%% analytic computation

clear
close all
format long

%% 
M=50;
b=1.8:0.03:4.2;
L=length(b);
output=zeros(L,2);
j=1;
for b=1.8:0.03:4.2;

Pr=0;

for B=2:M
    
    temp1=exp(-0.5*(b^2));
    temp2=b*sqrt((2*B-1)/B/(B-1));
    temp2=fv(temp2);
    temp3=(b^2)*(2*B-1)/2/B/(B-1)/sqrt(2* pi);
    Pr=Pr+temp1*temp2*temp3;
    
end

output(j,1)=Pr;
output(j,2)=b;
j=j+1;

end

figure
plot(output(:,2),output(:,1),'-r','LineWidth',1.5); hold on
xlim([1.8,4.2]);
ylim([0,0.8]);
xlabel('b');
ylabel('Significant Level')
load('Bstat_MC_M50.mat','B_stat');

j=1;
   for q=0.6:0.01:0.99
    output2(j,1)=1-q;
    output2(j,2)=quantile(B_stat,q);
    j=j+1;
   end
   
plot(output2(:,2),output2(:,1),'-b','LineWidth',1.5);  
legend('Formula','Sampling')
%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2
