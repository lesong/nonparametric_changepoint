% Monte Carlo 

% B test
% With sliding windows
clear 
clc
close all

mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point


%% CUSUM_B_test
N=10; % N is the total number of blocks
M=10; % M is the upper bound for size of blocks
%B=2:M;

rep=500; 


%% estimate bandwidth

X_s=zeros(20,500);
X_s(:,[1:500])=normrnd(mu_0,sigma_1,[20,500]);


%% calculate the kernel matrix
[ms,ns]=size(X_s);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,X_s(:,(i+1):ns),X_s(:,i));
    Ds(i,(i+1):ns)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(Ds(Ds~=0));


sigma_2_sq=0.004574751923643;
sigma_4_sq=0.018622680903357;
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;
S_var=zeros(1,M-1);
for i=1:1:M-1
    S_var(i)=C*2/(i+1)/(i);
end

 b=5.62;    
 freq=0;

 Kxx_post=zeros(M,M);
 Kxx_pre=zeros(N*M,M);
 Kxx_cross=zeros(N*M,M);
 
 
for t=1:rep % total number of runs    
      flag=1; % once cross the threshold, set the flag=0
       
      %% initialize      
      % sample data
       
       X=zeros(20,2*N*M+500); % reference data: 400*2
       X(:,[1:2*N*M+500])=normrnd(mu_0,sigma_1,[20,2*N*M+500]);
       [m,n]=size(X);
     
       % we need to remember and update data_pre
        
       index=(2*N+1)*M; % index refers to the endpoint of sliding window
       
       % initialize 
       %  compute Kxx_post, Kxx_pre and Kxx_cross  
       Kxx_post=fKxx1(X(:, index-M+1:index), X(:, index-M+1:index),M,bandw,1); % M by M
       
       Kxx_pre=[];
       Kxx_cross=[];
       
       for k=3:2:(2*N+1)
       Kxx_pre=[Kxx_pre; fKxx1(X(:,index-k*M+1: index-k*M+M),X(:,index-k*M+1: index-k*M+M),M,bandw,1)]; %  N*M by M 
       Kxx_cross=[Kxx_cross; fKxx1(X(:,index-k*M+1: index-k*M+M), X(:, index-M+1: index),M,bandw,2)]; % N*M by M
       end
      
   while (flag==1) && (index < n)    
   
       S=[];
       for B=2:M
         
          MMD=[];
         for j=1:N  
             T=Kxx_post( M-B+1:M,M-B+1:M  );
             A=Kxx_pre( j*M-B+1:j*M,  M-B+1:M );
             C=Kxx_cross( j*M-B+1:j*M,  M-B+1:M);
             MMD(j)= 1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:)) - 2/B/(B-1)*sum(C(:));
         end 
         S(B-1)=mean(MMD);
       end
    
     B_stat=max((S./sqrt(S_var)));
     
     if B_stat > b
         freq=freq+1; 
         index-(2*N+1)*M
         
         flag=0;
     end
        
           index=index+1; % index is the end of sliding window
     
           % given new data, we need to update Kxx_post, Kxx_pre, and Kxx_cross
           
           % Kxx_post
             
           Kxx_post(1:M-1,1:M-1)=Kxx_post(2:M,2:M);
           temp=fKxx1(X(:,index-M+1:index), X(:, index),M,bandw,3); % M by 1
           Kxx_post(:,M)=temp;
           Kxx_post(M,:)=temp';   
           
           % Kxx_pre
           
              for j=1:2:(2*N-1)  
               Kxx_pre((j-1)./2*M+1:(j-1)./2*M+M-1, 1:M-1)=Kxx_pre((j-1)./2*M+2:(j-1)./2*M+M, 2:M);
               temp=fKxx1( X(:,index-(j+2)*M+1:index-(j+2)*M+M), X(:,index-(j+2)*M+M),M,bandw,3);
               Kxx_pre((j-1)./2*M+1:(j-1)./2*M+M, M)=temp;
               Kxx_pre((j-1)./2*M+M,:)=temp';
              
                       
           % Kxx_cross
          
             
              
               Kxx_cross((j-1)./2*M+1:(j-1)./2*M+M-1, 1:M-1)=Kxx_cross((j-1)./2*M+2:(j-1)./2*M+M, 2:M);
               temp1=fKxx1( X( :,index-(j+2)*M+1:index-(j+2)*M+M), X(:,index),M,bandw,3);
               temp2=fKxx1( X( :,index-j*M+1:index-j*M+M), X(:,index-j*M+M),M,bandw,3);
               Kxx_cross((j-1)./2*M+1:(j-1)./2*M+M, M)=temp1;
               Kxx_cross((j-1)./2*M+M,:)=temp2';
                           
             end   
                             
               
   end
  
 fprintf(1, '--experiment no %d of %d\n', t, rep); 
end         

freq/rep



         
 