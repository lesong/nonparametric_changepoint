%% analytic computation

clear
% compare empirical tail probability under true Gaussian and the estimated
% tail probability

close all
clc

M=50;
output=zeros( 20,2);
for t=1:50
%% empirical tail probability

% generate data
data_mean=zeros(1,M-1);
data_cov=zeros(M-1,M-1);
data_var=ones(1,M-1); % after normalization, all variances should be 1

for i=1:1:M-1
    for j=1:1:M-1
        B_min=min(i+1,j+1);
        B_max=max(i+1,j+1);
        data_cov(i,j)=sqrt( B_min*(B_min-1)/B_max/(B_max-1));
    end
end

%gnereate 10000 sample
data=mvnrnd(data_mean,data_cov,10000);
data_max=max(data,[],2);
%figure
%hist(data_max,500);
%[f,x]=ecdf(data_max);
%figure
%stairs(x,f)
j=1;

for q=0.8:0.01:0.99
    output(j,1)=output(j,1)+q;
    output(j,2)=output(j,2)+quantile(data_max,q);
    j=j+1;
end
end
output/50