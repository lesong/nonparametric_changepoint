%% analytic computation

clear

close all

clc

N=20;
M=20;

mu_0=0; % the mean and variance under null
sigma_1=1;


%% estimate bandwidth

X_s=zeros(20,200);
X_s(:,[1:200])=normrnd(mu_0,sigma_1,[20,200]);


%% calculate the kernel matrix
[ms,ns]=size(X_s);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,X_s(:,(i+1):ns),X_s(:,i));
    Ds(i,(i+1):ns)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(Ds(Ds~=0));

%% compute sigma_2_sq 
sigma_2_sq=0.004574751923643;
sigma_4_sq=0.018622680903357;
C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;

%% generate data
data_mean=zeros(1,M-1);
data_cov=zeros(M-1,M-1);
data_var=zeros(1,M-1);
for i=1:1:M-1
    for j=1:1:M-1
        t=max(i+1,j+1);
       data_cov(i,j)=C*2/t/(t-1);
       if i+1==j+1
           data_var(i)=C*2/(i+1)/(i);
       end
    end
end

%gnereate 10000 sample
data=mvnrnd(data_mean,data_cov,10000);
data_n=bsxfun(@rdivide, data, sqrt(data_var));
data_max=max(data_n,[],2);
save gen_sta.mat data_max
figure; hist(data_max,100);
[f,x]=ecdf(data_max);
figure
stairs(x,f)

