%% analytic computation

clear
close all
format long

%% 
M=10; %M is the maximum block size 
T=20; % T is the time

b=4.5;
L=length(b);
output=zeros(L,2);
j=1;


Pr=0;

 for t=M+1:1:T
     for k=t-M:1:t-2
    
    temp1=exp(-0.5*(b^2));
    temp2=b*sqrt((2*(t-k)-1)/(t-k)/((t-k)-1));
    temp3=fv(temp2);
    temp4=(b^2)*(2*(t-k)-1)/2/(t-k)/((t-k)-1);
    temp5=(temp3*temp4)^2;
    temp6=1/sqrt(2* pi);
    Pr=Pr+temp1*temp5*temp6;
     end
 end

output(j,1)=Pr;

output(j,2)=b;
j=j+1;


output

%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2
