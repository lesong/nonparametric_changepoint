%% analytic computation

clear
close all
format long

%% 
M=10;
b=1.6:0.001:3.9;
L=length(b);
output=zeros(L,2);
j=1;
for b=1.6:0.001:3.9;

Pr=0;

for B=2:M
    
    temp1=exp(-0.5*(b^2));
    temp2=b*sqrt((2*B-1)/B/(B-1));
    temp2=fv(temp2);
    temp3=(b^2)*(2*B-1)/2/B/(B-1)/sqrt(2* pi);
    Pr=Pr+temp1*temp2*temp3;
    
end

output(j,1)=1-Pr;
output(j,2)=b;
j=j+1;

end
output

%% tail probability = 0.1
% theory b = 2.6
% simulation b = 2.2
