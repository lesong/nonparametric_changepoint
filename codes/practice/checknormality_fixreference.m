%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point



for B=10; % M is the maxima block size
repeatno = 1000; 
Z_B=zeros(1,repeatno);

N=20; % N is the number of blocks

X=zeros(20,N*B);
X(:,[1:N*B])=normrnd(mu_0,sigma_1,[20,N*B]);

for j= 1:repeatno
    
   
       
       % sample post-change data

       % sample pre-change data
       Y=zeros(20,B);
       Y(:,[1:B])=normrnd(mu_0,sigma_1,[20,B]);
       
       %% calculate the kernel matrix
       
       
       for i=1:N
           D=zeros(2*B,2*B);
           Y1=X(:,(i-1)*B+1:i*B);
           Y1=[Y1,Y];
           for k=1:2*B
           temp=bsxfun(@minus,Y1(:,(k+1):2*B),Y1(:,k));
           D(k,(k+1):2*B)=dot(temp,temp);
           end
   % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));
      D=D+D';
   % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
             
       A=Kxx(1:B, 1:B);
       T=Kxx(B+1:2*B, B+1:2*B);
       C=Kxx(1:B, B+1:2*B);
       C(logical(eye(size(C))))=0;
       MMD(i)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
       
       end
       Z_B(j)=mean(MMD);
      % fprintf(1, '--experiment no %d of %d\n',j, repeatno); 
end
hist(Z_B',100)


end


