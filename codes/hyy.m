function sigma_2=hyy(mu_0, sigma_1,bandw)


for t=1:50
L=3000;

h1=zeros(floor(L/6),2);

for j=1:floor(L/6)
    
       X=zeros(20,6);
       X(:,[1:6])=normrnd(mu_0,sigma_1,[20,6]);
       
       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
       end
     % use rule of thumb to determine the bandwidth
     
        
        D=D+D';
    % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
    
           
      h1(j,1)=Kxx(1,2)+Kxx(5,6) - ...
             Kxx(1,5) -Kxx(2,6) ;
      h1(j,2)=Kxx(3,4)+Kxx(5,6) - ...
             Kxx(3,5) -Kxx(4,6) ;
end
       
       sigma_2(t)=(1/(floor(L/6)-1))*(h1(:,1)-mean(h1(:,1)))'*(h1(:,2)-mean(h1(:,2)));


end

sigma_2=mean(sigma_2);




end
