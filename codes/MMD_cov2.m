%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point



B=10; % M is the maxima block size
A=6;
k=5;
repeatno = 3000; 
MMD=zeros(repeatno,2);





for j= 1:repeatno
       
       X=zeros(20,3*(B+k));
       X(:,[1:3*(B+k)])=normrnd(mu_0,sigma_1,[20,3*(B+k)]);

       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
       temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
       D(i,(i+1):n)=dot(temp,temp);
       end
   % use rule of thumb to determine the bandwidth
     bandw=median(D(D~=0));
     
      D=D+D';
   % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
       
       R=Kxx(1:B, 1:B);
       T=Kxx(n-B+1:n, n-B+1:n);
       C=Kxx(1:B, n-B+1:n);
       C(logical(eye(size(C))))=0;
       MMD(j,1)=1/B/(B-1)*sum(R(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
      
              
       R=Kxx(B-A+1:B-A+B+k, B-A+1:B-A+B+k); 
       T=Kxx(n-B-k+1:n, n-B-k+1:n);
       C=Kxx(B-A+1:B-A+B+k, n-B-k+1:n);
       C(logical(eye(size(C))))=0;
       MMD(j,2)=1/(B+k)/(B+k-1)*sum(R(:))+1/(B+k)/(B+k-1)*sum(T(:))-2/(B+k)/(B+k-1)*sum(C(:));
              
       
end
   
   MMD_cov=(1/(repeatno-1))*(MMD(:,1)-mean(MMD(:,1)))'*(MMD(:,2)-mean(MMD(:,2)));


MMD_cov




L=3000;

h1=zeros(floor(L/6),2);

for j=1:floor(L/6)
    
       X=zeros(20,6);
       X(:,[1:6])=normrnd(mu_0,sigma_1,[20,6]);
       
       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
       end
     % use rule of thumb to determine the bandwidth
        bandw=median(D(D~=0));
        
        D=D+D';
    % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
    
           
      h1(j,1)=Kxx(1,2)+Kxx(5,6) - ...
             Kxx(1,5) -Kxx(2,6) ;
      h1(j,2)=Kxx(3,4)+Kxx(5,6) - ...
             Kxx(3,5) -Kxx(4,6) ;
end
       
       sigma_2=(1/(floor(L/6)-1))*(h1(:,1)-mean(h1(:,1)))'*(h1(:,2)-mean(h1(:,2)))


h2=zeros(floor(L/5),2);


for j=1:floor(L/5)
    
    
      X=zeros(20,5);
       X(:,[1:5])=normrnd(mu_0,sigma_1,[20,5]);
       
       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
       end
     % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));
       
        D=D+D';
    % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
    
           
      h2(j,1)=Kxx(1,2)+Kxx(3,5) - ...
             Kxx(1,3) -Kxx(2,5) ;
      h2(j,2)=Kxx(1,2)+Kxx(3,4) - ...
             Kxx(1,3) -Kxx(2,4) ;
end
       
       sigma_3=(1/(floor(L/5)-1))*(h2(:,1)-mean(h2(:,1)))'*(h2(:,2)-mean(h2(:,2)))



h3=zeros(floor(L/4),1);


for j=1:floor(L/4)
    
    
       X=zeros(20,4);
       X(:,[1:4])=normrnd(mu_0,sigma_1,[20,4]);
       
       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
       end
     % use rule of thumb to determine the bandwidth
       bandw=median(D(D~=0));
       
        D=D+D';
    % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      
      
      h3(j)=Kxx(1,2)+Kxx(3,4) - ...
             Kxx(1,3) -Kxx(2,4) ;
end
       
       sigma_4=mean(h3.^2);



   C1=A*(A-1)*0.5*B*(B-1)*0.5*(B+k-2)*(B+k-3)*0.5...
       +A*(A-1)*0.5*B*(B-1)*0.5*(B+k-2)*(B+k-3)*0.5...
       +B*(B-1)*0.5*(B-A)*(B-A-1)*0.5*(B+k)*(B+k-1)*0.5...
       +B*(B-1)*0.5*A*(B-A)*(B+k-1)*(B+k-2)*0.5;
   
   C2=A*(A-1)*0.5*B*(B+k-1)*(B-1)+A*(A-1)*0.5*B*(B-1)*(B+k-2)...
       +A*(B-A)*B*(B-1)*0.5*(B+k-1);
   
   C3=A*(A-1)*0.5*B*(B-1)*0.5;
   
   MMD_covest=(2/B/(B-1))*(2/(B+k)/(B+k-1))*(C1*sigma_2+C2*sigma_3+C3*sigma_4);

    
    
  MMD_covest














