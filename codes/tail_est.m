%% analytic computation

clear

close all


format long

N=20;
M=20;

mu_0=0; % the mean and variance under null
sigma_1=1;


%% estimate bandwidth

X_s=zeros(20,200);
X_s(:,[1:200])=normrnd(mu_0,sigma_1,[20,200]);


%% calculate the kernel matrix
[ms,ns]=size(X_s);
Ds=zeros(ns,ns); % D is the pairwise distance matrix
for i=1:ns
    temp=bsxfun(@minus,X_s(:,(i+1):ns),X_s(:,i));
    Ds(i,(i+1):ns)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(Ds(Ds~=0))

%% compute sigma_2_sq 


% sigma_2_sq=hyy(mu_0, sigma_1,bandw)
sigma_2_sq=0.004574751923643;
sigma_4_sq=0.018622680903357;

%sigma_4_sq=hxxyy(mu_0,sigma_1,bandw)


C=(sigma_4_sq/N) +(N-1)*sigma_2_sq/N;

%% 

b=0.1;

Pr=0;

for B=2:M
    
    temp1=-0.25*(b^2)*B*(B-1)/C;
    temp1=exp(temp1)
    temp2=(b^2)*(2*B-1)/2/(C^0.5);
    temp2=fv(temp2)
    temp3=(b^2)*(2*B-1)/8/C/((pi *C/B/(B-1))^0.5)
    Pr=Pr+temp1*temp2*temp3;
    
end

Pr
