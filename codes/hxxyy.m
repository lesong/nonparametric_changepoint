function sigma_4=hxxyy(mu_0, sigma_1, bandw)


for t=1:50

L=3000;


h=zeros(floor(L/4),1);
  
       for j=1:floor(L/4)
           
           X=zeros(20,4);
           X(:,[1:4])=normrnd(mu_0,sigma_1,[20,4]);
       
       %% calculate the kernel matrix
          [m,n]=size(X);
          D=zeros(n,n); % D is the pairwise distance matrix
         for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
         end
     % use rule of thumb to determine the bandwidth
         
         D=D+D';
    % apply RBF and obtain kernel matrix Kxx
         Kxx = exp(-1/2/bandw * D);
           
           
         h(j)=Kxx(1,2)+Kxx(3,4) - ...
             Kxx(1,3) -Kxx(2,4) ;
       end
       sigma_4(t)=mean(h.^2);
       
end

sigma_4=mean(sigma_4);
end

















