%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point



B=10; % M is the maxima block size
k=5;
repeatno = 3000; 
MMD=zeros(repeatno,2);





for j= 1:repeatno
       
       X=zeros(20,2*(B+k));
       X(:,[1: 2*(B+k)])=normrnd(mu_0,sigma_1,[20,2*(B+k)]);

       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
       for i=1:n
       temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
       D(i,(i+1):n)=dot(temp,temp);
       end
   % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));
       %bandw=1;
       D=D+D';
   % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
       
       R=Kxx(1:B, 1:B);
       T=Kxx(n-B+1:n, n-B+1:n);
       C=Kxx(1:B, n-B+1:n);
       C(logical(eye(size(C))))=0;
       MMD(j,1)=1/B/(B-1)*sum(R(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
      
              
       R=Kxx(1:B+k, 1:B+k); 
       T=Kxx(n-B-k+1:n, n-B-k+1:n);
       C=Kxx(1:B+k, n-B-k+1:n);
       C(logical(eye(size(C))))=0;
       MMD(j,2)=1/(B+k)/(B+k-1)*sum(R(:))+1/(B+k)/(B+k-1)*sum(T(:))-2/(B+k)/(B+k-1)*sum(C(:));
              
       
end
   
   MMD_cov=(1/(repeatno-1))*(MMD(:,1)-mean(MMD(:,1)))'*(MMD(:,2)-mean(MMD(:,2)));


MMD_cov




for L=1000:1000:5000;




h=zeros(floor(L/4),1);

       for j=1:floor(L/4)
           
           X=zeros(20,4);
           s = rng;
           X(:,[1:4])=normrnd(mu_0,sigma_1,[20,4]);
       
       %% calculate the kernel matrix
          [m,n]=size(X);
           D=zeros(n,n); % D is the pairwise distance matrix
         for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
         end
     % use rule of thumb to determine the bandwidth
         bandw=median(D(D~=0));
         %bandw=1;
         D=D+D';
    % apply RBF and obtain kernel matrix Kxx
         Kxx = exp(-1/2/bandw * D);
           
           
         h(j)=Kxx(1,2)+Kxx(3,4) - ...
             Kxx(1,3) -Kxx(2,4) ;
       end
       MMD_vest=(2/(B+k)/(B+k-1))*mean(h.^2)

end











