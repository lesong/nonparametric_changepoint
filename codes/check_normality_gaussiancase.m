%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point



for B=5:5:20; % M is the maxima block size
repeatno = 1000; 
Z_B=zeros(1,repeatno);

N=40; % N is the number of blocks


for j= 1:repeatno
    
   
       
       % sample post-change data

       % sample pre-change data
       X=zeros(20,(N+1)*B);
       X(:,[1:(N+1)*B])=normrnd(mu_0,sigma_1,[20,(N+1)*B]);
       
       %% calculate the kernel matrix
       
       
       for i=1:N
           D=zeros(2*B,2*B);
           Y=X(:,(i-1)*B+1:i*B);
           Y=[Y,X(:,N*B+1:(N+1)*B)];
           for k=1:2*B
           temp=bsxfun(@minus,Y(:,(k+1):2*B),Y(:,k));
           D(k,(k+1):2*B)=dot(temp,temp);
           end
   % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));
      D=D+D';
   % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
             
       A=Kxx(1:B, 1:B);
       T=Kxx(B+1:2*B, B+1:2*B);
       C=Kxx(1:B, B+1:2*B);
       C(logical(eye(size(C))))=0;
       MMD(i)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
       
       end
       Z_B(j)=mean(MMD);
       fprintf(1, '--experiment no %d of %d\n',j, repeatno); 
end

pd=fitdist(Z_B'/var(Z_B)^0.5,'Normal');
x=-3:0.01:3;
y=pdf(pd,x);
 plot(x, y); hold on
mean(Z_B)
var(Z_B)
mean(Z_B.^3)
end


