%% CUSUM kernel change point detection
% B test
% With sliding windows
clear 
clc
close all

%% generate data X 

% parameter for the null distribution
mu_0=0; % the mean before the change point
sigma_1=1; % the standard deviation before the change point



B=10; % M is the maxima block size
repeatno = 2000; 
MMD=zeros(1,repeatno);



   for j= 1:repeatno
       
       
       X=zeros(20,2*B);
       X(:,[1:2*B])=normrnd(mu_0,sigma_1,[20,2*B]);

       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(m,n); % D is the pairwise distance matrix
       for i=1:n
       temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
       D(i,(i+1):n)=dot(temp,temp);
       end
   % use rule of thumb to determine the bandwidth
      bandw=median(D(D~=0));
      D=D+D';
   % apply RBF and obtain kernel matrix Kxx
      Kxx = exp(-1/2/bandw * D);
      Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
             
       A=Kxx(1:B, 1:B);
       T=Kxx(B+1:2*B, B+1:2*B);
       C=Kxx(1:B, B+1:2*B);
       C(logical(eye(size(C))))=0;
       MMD(j)=1/B/(B-1)*sum(A(:))+1/B/(B-1)*sum(T(:))-2/B/(B-1)*sum(C(:));
       
   end


MMD_v=var(MMD)

% choose L data to estimate
 for L=1000:1000:6000;

       X=zeros(20,L);
       s = rng;
       X(:,[1:L])=normrnd(mu_0,sigma_1,[20,L]);
       
       %% calculate the kernel matrix
       [m,n]=size(X);
       D=zeros(n,n); % D is the pairwise distance matrix
      for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
      end
     % use rule of thumb to determine the bandwidth
     bandw=median(D(D~=0));
      D=D+D';
    % apply RBF and obtain kernel matrix Kxx
     Kxx = exp(-1/2/bandw * D);
     Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0


h=zeros(floor(L/4),1);
  
       for j=1:floor(L/4)
           
           X=zeros(20,4);
           X(:,[1:4])=normrnd(mu_0,sigma_1,[20,4]);
       
       %% calculate the kernel matrix
          [m,n]=size(X);
          D=zeros(n,n); % D is the pairwise distance matrix
         for i=1:n
         temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
         D(i,(i+1):n)=dot(temp,temp);
         end
     % use rule of thumb to determine the bandwidth
         bandw=median(D(D~=0));
         D=D+D';
    % apply RBF and obtain kernel matrix Kxx
         Kxx = exp(-1/2/bandw * D);
           
           
         h(j)=Kxx(1,2)+Kxx(3,4) - ...
             Kxx(1,3) -Kxx(2,4) ;
       end
       MMD_vest=(2/B/(B-1))*mean(h.^2)
end

    
   
    
    
    
    
    
    
    
    
    


