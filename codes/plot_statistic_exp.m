%% CUSUM kernel change point detection
clear 
clc
close all

%% generate data X 

%  X_1 ~ exp(mu_0)   
%  X_2 ~ exp(mu_1)

mu_0=1; % the mean before the change point
mu_1=5; % the mean after the change point

X=zeros(20,500);
X(:,[1:250])=exprnd(mu_0,[20,250]);
X(:,[251:500])=exprnd(mu_1,[20,250]);

%% calculate the kernel matrix
[m,n]=size(X);
D=zeros(m,n); % D is the pairwise distance matrix
for i=1:n
    temp=bsxfun(@minus,X(:,(i+1):n),X(:,i));
    D(i,(i+1):n)=dot(temp,temp);
end
% use rule of thumb to determine the bandwidth
bandw=median(D(D~=0));
D=D+D';
% apply RBF and obtain kernel matrix Kxx
Kxx = exp(-1/2/bandw * D);
Kxx(logical(eye(size(Kxx))))=0; % set the diagonal entries 0
% plot colormap for kernel matrix
figure
imagesc(Kxx);

%% CUSUM
CUSUM=zeros(1,n-3);
for t=4:n
    S=zeros(1,t-3);
    for l=2:t-2
        A=Kxx(1:l,1:l);
        B=Kxx(l+1:t,l+1:t);
        C=Kxx(1:l,l+1:t);
        S(l-1)=1/l/(l-1)*sum(A(:))+1/(t-l-1)/(t-l)*sum(B(:))-2/l/(t-l)*sum(C(:));
    end
    CUSUM(t-3)=max(S);
end
figure
plot(CUSUM,'LineWidth',1.25)
ylim([-0.05,1])
xlabel('Time','FontSize',13);
ylabel('Statistic','FontSize',13);





























