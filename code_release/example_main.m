clear;
clc;

%%
sequence = csvread('feature.csv', 1, 0)';
reference = sequence(:, 1 : 10800);
data_test = sequence(:, 10801 : end);

figure; subplot(2,1,1); plot(data_test); xlabel('Location'); ylabel('Signal');

%%
window_size = 500;
num_blocks = 20;

M_stat = compute_mstats_online(reference, data_test, window_size, num_blocks, ...
                                'resample', 0, 'gamma', 30);

subplot(2,1,2);
plot(M_stat); % note: the algorithm stops whenever the change-point is detected
xlabel('Index');
ylabel('M-Statistic');

hold on
% localize the change-point 

loc = find(M_stat==max(M_stat));
plot(loc, M_stat(loc), 'ro'); hold on 