function mstats = compute_mstats_online(total_ref_data, test_data, window_size, num_blocks, varargin)
    dim = size(total_ref_data, 1);
    if window_size * num_blocks > size(total_ref_data, 2)
        error('window size x num blocks is larger than length of reference data\n');
    end
    options = get_varargin_options(varargin{:});
    
    if ~isfield(options, 'resample')
        options.resample = 0;
    end
    
    if ~isfield(options, 'bandw')
        dist_mat = EuDist2(total_ref_data');       
        options.bandw = median(dist_mat(dist_mat ~= 0));
    end    
    if ~isfield(options, 'gamma')
        options.gamma = 1.0;        
    end    
    options.bandw = options.bandw * options.gamma;
    
    if ~isfield(options, 'variance')
        options.variance = est_var_online(total_ref_data, options.bandw, window_size, num_blocks);
    end    
    if ~isfield(options, 'arl')
        options.arl = 5000;
    end    
    if ~isfield(options, 'threshold')        
        options.threshold = get_threshold(window_size, options.arl);
    end
    
    options.t = options.bandw;
    options.KernelType = 'Gaussian';
    options
    
    
    ref_data = [];
    for i = 1 : num_blocks
        ref_data = [ref_data, total_ref_data(:, (i - 1) * window_size + 1 : i * window_size)'];
    end
    
    % construct A
    ref_kernel_mat = cell(1, num_blocks);
    avg_kxx = 0;
    for i = 1 : num_blocks
        A = constructKernel(ref_data(:, (i - 1) * dim + 1 : i * dim), [], options);
        A(logical(eye(size(A)))) = 0;
        ref_kernel_mat{i} = A;
        avg_kxx = avg_kxx + sum(A(:)) / window_size / (window_size - 1);
    end
    avg_kxx = avg_kxx / num_blocks;    

    % construct T
    cur_test_block = test_data(:, 1 : window_size)';    
    T = constructKernel(cur_test_block, [], options);    
    T(logical(eye(size(T)))) = 0;
    
    % construct C
    C = cell(1, num_blocks);
    for i = 1 : num_blocks
        C{i} = constructKernel(ref_data(:, (i - 1) * dim + 1 : i * dim), cur_test_block, options);
    end
    
    X = [total_ref_data, test_data];
    cur_pool_len = size(total_ref_data, 2);
    
    mstats = zeros(1, size(test_data, 2));
    for idx = window_size : size(test_data, 2)               
        mmd = avg_kxx + sum(T(:)) / window_size / (window_size - 1);
        tmp = 0;
        for i = 1 : num_blocks
            tmp = tmp - 2.0 * sum(C{i}(:)) / window_size / (window_size - 1);            
        end
        mmd = mmd + tmp / num_blocks;
        
        mstats(idx) = mmd / sqrt(options.variance);
        
        if idx == size(test_data, 2) || mstats(idx) > options.threshold
            break;
        end
                
        if options.resample
            % update A
            cur_pool_len = cur_pool_len + 1;
            new_ref_data = reshape(datasample(X(:, 1 : cur_pool_len), num_blocks, 2, 'Replace', false), dim * num_blocks, 1)'; 
            ref_data = [ref_data(2 : end, :); new_ref_data];
            avg_kxx = 0;
            for i = 1 : num_blocks
                cur_block = (i - 1) * dim + 1 : i * dim;
                A = ref_kernel_mat{i};
                A(1 : end - 1, 1 : end - 1) = ref_kernel_mat{i}(2 : end, 2 : end);
                kxx = constructKernel(new_ref_data(1, cur_block), ref_data(1 : end - 1, cur_block), options);
                A(end, 1 : end - 1) = kxx;
                A(1 : end - 1, end) = kxx';
                ref_kernel_mat{i} = A;
                avg_kxx = avg_kxx + sum(A(:)) / window_size / (window_size - 1);
            end
            avg_kxx = avg_kxx / num_blocks;    
        end
        
        % update T
        cur_test_block = [cur_test_block(2 : end, :); test_data(:, idx + 1)'];
        T(1 : end - 1, 1 : end - 1) = T(2 : end, 2 : end);
        kxx = constructKernel(cur_test_block(end, :), cur_test_block(1 : end - 1, :), options);
        T(end, 1 : end - 1) = kxx;
        T(1 : end - 1, end) = kxx;
        
        % update C
        for i = 1 : num_blocks
            cur_block = (i - 1) * dim + 1 : i * dim;
                        
            if options.resample
                C{i}(1 : end - 1, 1 : end - 1) = C{i}(2 : end, 2 : end);
                kxy = constructKernel(new_ref_data(1, cur_block), cur_test_block, options);
                C{i}(end, 1 : end) = kxy;
            else
                C{i}(1 : end, 1 : end - 1) = C{i}(1 : end, 2 : end);                                        
            end
            kxy = constructKernel(ref_data(:, cur_block), cur_test_block(end, :), options);
            C{i}(1 : end, end) = kxy;
        end
    end
end