function threshold = get_threshold(window_size, ARL)
    
    l = 1.0;
    r = 1.0;
    while 1 == 1
        if compute_arl(l, window_size) > ARL
            l = l / 2.0;
        else
            break
        end
    end
    
    while 1 == 1
        if compute_arl(r, window_size) < ARL
            r = r * 2;
        else
            break
        end
    end
    
    while abs(l - r) > 1e-6
        b = (l + r) / 2;
        if compute_arl(b, window_size) < ARL
            l = b;
        else
            r = b;
        end
    end
    
    threshold = b;
end

function est_arl = compute_arl(b, window_size)
    temp1=exp(-0.5*(b^2));
    temp2=1/sqrt(2* pi);
    temp3=b*sqrt(2*(2*window_size-1)/window_size/(window_size-1));
    temp4=fv(temp3);
    temp5=(b^2)*(2*window_size-1)/window_size/(window_size-1);
    temp6=temp4*temp5;
 
    est_arl = 1.0 / temp6 / temp1 / temp2;
end