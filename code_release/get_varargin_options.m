function options = get_varargin_options(varargin)
    nArgs = length(varargin);
    if round(nArgs/2)~=nArgs/2
        error('needs propertyName/propertyValue pairs')
    end
    options = {};
    for pair = reshape(varargin,2,[]) %# pair is {propName;propValue}
        inpName = lower(pair{1}); %# make case insensitive
        options.(inpName) = pair{2};
    end
end