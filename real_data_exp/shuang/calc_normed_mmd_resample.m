function [normed_mmd_stats, threshold, var_zb] = calc_normed_mmd_resample(X, ref_len, B, options)
    dim = size(X, 1);
    sequence_length = size(X, 2);
    
    num_blocks = floor(ref_len / B);
    ref_len = num_blocks * B;
    ref_data = X(:, 1 : ref_len);
    var_zb = get_var_zb(ref_data, B, options.t);
    
    ref_data = [];
    for i = 1 : num_blocks
        ref_data = [ref_data, X(:, (i - 1) * B + 1 : i * B)'];
    end
    
    % construct A
    ref_kernel_mat = cell(1, num_blocks);
    for i = 1 : num_blocks
        A = constructKernel(ref_data(:, (i - 1) * dim + 1 : i * dim), [], options);
        A(logical(eye(size(A)))) = 0;
        ref_kernel_mat{i} = A;
    end
    
    % construct T
    test_data = X(:, ref_len + 1 : ref_len + 1 + B - 1)';
    T = constructKernel(test_data, [], options);
    T(logical(eye(size(T)))) = 0;
    
    % construct C
    C = cell(1, num_blocks);
    for i = 1 : num_blocks
        C{i} = constructKernel(ref_data(:, (i - 1) * dim + 1 : i * dim), test_data, options);
    end
    
    threshold = get_threshold(B, 100, options.prob);
    
    idx = 0;
    for pos = ref_len + 1 : sequence_length - B + 1
        idx = idx + 1;
        
        avg_kxx = 0;
        for i = 1 : num_blocks
            avg_kxx = avg_kxx + sum(ref_kernel_mat{i}(:)) / B / (B - 1);
        end
        avg_kxx = avg_kxx / num_blocks;
        
        mmd = avg_kxx + sum(T(:)) / B / (B - 1);
        
        tmp = 0;
        for i = 1 : num_blocks
            tmp = tmp - 2.0 * sum(C{i}(:)) / B / (B - 1);
        end
        mmd = mmd + tmp / num_blocks;
        
        normed_mmd_stats(idx) = mmd / sqrt(var_zb);
        
        if pos == sequence_length - B + 1
            break;
        end
        
        % update A
        new_ref_data = reshape(datasample(X(:, 1 : pos), num_blocks, 2), dim * num_blocks, 1)';
        ref_data = [ref_data(2 : end, :); new_ref_data];
        for i = 1 : num_blocks
            cur_block = (i - 1) * dim + 1 : i * dim;
            A = ref_kernel_mat{i};
            A(1 : end - 1, 1 : end - 1) = ref_kernel_mat{i}(2 : end, 2 : end);
            kxx = constructKernel(new_ref_data(1, cur_block), ref_data(1 : end - 1, cur_block), options);
            A(end, 1 : end - 1) = kxx;
            A(1 : end - 1, end) = kxx';
            ref_kernel_mat{i} = A;
        end
        
        % update T
        test_data = [test_data(2 : end, :); X(:, pos + B)'];
        T(1 : end - 1, 1 : end - 1) = T(2 : end, 2 : end);
        kxx = constructKernel(test_data(end, :), test_data(1 : end - 1, :), options);
        T(end, 1 : end - 1) = kxx;
        T(1 : end - 1, end) = kxx;
        
        % update C
        for i = 1 : num_blocks
              cur_block = (i - 1) * dim + 1 : i * dim;

              C{i}(1 : end - 1, 1 : end - 1) = C{i}(2 : end, 2 : end);

              kxy = constructKernel(new_ref_data(1, cur_block), test_data, options);
              C{i}(end, 1 : end) = kxy;

              kxy = constructKernel(ref_data(:, cur_block), test_data(end, :), options);
              C{i}(1 : end, end) = kxy;
        end
    end
end