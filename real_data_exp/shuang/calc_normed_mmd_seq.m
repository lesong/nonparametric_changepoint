function [normed_mmd_stats, threshold, var_zb] = calc_normed_mmd_seq(X, ref_len, B, options)
    
    ref_data = X(:, 1 : ref_len);
    var_zb = get_var_zb(ref_data, B, options.t);
    ref_data = ref_data';
    
    sequence_length = size(X, 2);
    num_blocks = floor(ref_len / B);
    
    avg_kxx = 0;
    for i = 1 : num_blocks
        A = constructKernel(ref_data((i - 1) * B + 1 : i * B, :), [], options);
        A(logical(eye(size(A)))) = 0;
        avg_kxx = avg_kxx + sum(A(:)) / B / (B - 1);
    end
    avg_kxx = avg_kxx / num_blocks;
    
    % construct T
    test_data = X(:, ref_len + 1 : ref_len + 1 + B - 1)';
    T = constructKernel(test_data, [], options);
    T(logical(eye(size(T)))) = 0;
    
    % construct C
    C = cell(1, num_blocks);
    for i = 1 : num_blocks
        C{i} = constructKernel(ref_data((i - 1) * B + 1 : i * B, :), test_data, options);
    end
    
    idx = B - 1;
    threshold = get_threshold(B, B + 100, options.prob);
    normed_mmd_stats(1 : B - 1) = 0;
    
    for pos = ref_len + 1 : sequence_length - B + 1
        idx = idx + 1;

        mmd = avg_kxx + sum(T(:)) / B / (B - 1);
        
        tmp = 0;
        for i = 1 : num_blocks
            tmp = tmp - 2.0 * sum(C{i}(:)) / B / (B - 1);
        end
        mmd = mmd + tmp / num_blocks;
        
        % normalize
        normed_mmd_stats(idx) = mmd / sqrt(var_zb);
        
        if pos == sequence_length - B + 1
            break;
        end
        
        % update T
        test_data = [test_data(2 : end, :); X(:, pos + B)'];
        T(1 : end - 1, 1 : end - 1) = T(2 : end, 2 : end);
        kxx = constructKernel(test_data(end, :), test_data(1 : end - 1, :), options);
        T(end, 1 : end - 1) = kxx;
        T(1 : end - 1, end) = kxx;
        
        % update C
        for i = 1 : num_blocks
              cur_block = (i - 1) * B + 1 : i * B;

              C{i}(1 : end, 1 : end - 1) = C{i}(1 : end, 2 : end);

              kxy = constructKernel(ref_data(cur_block, :), test_data(end, :), options);
              C{i}(1 : end, end) = kxy;
        end
    end
end