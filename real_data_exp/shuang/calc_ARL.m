function arl = calc_ARL(b, M)
    t1 = b ^ 2 * exp(-0.5 * b^2);
    
    t2 = (2 * M - 1) / sqrt(2 * pi) / M / (M - 1);
    
    t3 = b * sqrt(2 * (2 * M - 1) / M / (M - 1));
    
    t3 = fv(t3);
    
    t = t1 * t2 * t3;
    
    arl = 1 / t;
end