clear;
clc;

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));


%%
data_root = '~/data/dataset/CENSREC-1-C/speechdata';
addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

ours_l = [];
ours_s = [];
baseline_l = [];
baseline_s = [];

for i = 1 : 20
    filename = sprintf('~/scratch/online_speech_params/ours_online_results_%s_%s.mat', sub_roots{i}, sub_folders{i});
    load(filename);
    s = ours_score{3};
    s = s ./ max(abs(s));
    ours_l = [ours_l, ours_labels{3}];
    ours_s = [ours_s, s];
    filename = sprintf('~/scratch/online_speech_params/baseline_online_results_%s_%s.mat', sub_roots{i}, sub_folders{i});
    load(filename);
    s = baseline_score{3};
    s = s ./ max(abs(s));
    baseline_l = [baseline_l, baseline_labels{3}];
    baseline_s = [baseline_s, s];
end