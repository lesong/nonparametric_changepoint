function [evaluated_seq, mmd_stats, break_points, thresholds] = ours_run_folder(data_root, sec, sub_folder, B, N, t, seg_idx, is_validating)
    ref_len = B * N;
    test_blocks = 6;
    
    options.KernelType = 'Gaussian';
    options.prob = 0.02;
    options.t = t;
    
    filenames = dir(fullfile(data_root, sec, sub_folder, '*.raw'));
    seg_names = cell(1, length(filenames));
    for i = 1 : length(filenames)
        t = strsplit(filenames(i).name, '.');
        seg_names{i} = t{1};
    end
    
    num_seq = 0;
    mmd_stats = {};
    break_points = [];
    thresholds = [];
    evaluated_seq = {};
    for i = 1 : length(seg_names)
        [whole_sequence, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
        
        if is_validating
            range = [seg_idx(i)];
        else
            range = [1 : seg_idx(i) - 1, seg_idx(i) + 1 : size(segments, 1)];
        end
        for j_idx = 1 : length(range)
            j = range(j_idx);
            num_seq = num_seq + 1;
            fprintf('sub_sequence %d/%d of sequence %d/%d in folder %s\n', j, size(segments, 1), i, length(seg_names), sub_folder);
            change_point = segments(j, 1);
            sequence = whole_sequence(:, change_point - (N + test_blocks) *  B : change_point + test_blocks * B);            
            break_points(num_seq) = test_blocks * B + 1;
            evaluated_seq{num_seq} = sequence;
            [mmd_stats{num_seq}, thresholds(num_seq), ~] = calc_normed_mmd_seq(sequence, ref_len, B, options);
        end
    end
end