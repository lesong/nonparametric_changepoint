%%
clear;
clc;

%%
root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));

%%
data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'close'};

for rr = 1 : length(subs)
    sub_root = subs{rr}

    tmp = dir(fullfile(data_root, sub_root));
    sub_folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            sub_folders{t} = name;
        end
    end

    B_range = [50, 100]; 
    N_range = [10, 20, 30];
    t_range = [1, 0.1, 5, 10];
    
    for i = 1 : length(sub_folders)
       %[B(i), N(i), T(i)] = tune_params(data_root, 'close', sub_folders{i}, 2, B_range, N_range, t_range);
        exp_folder_CENSREC(data_root, 'close', sub_folders{i}, 'baseline');
    end

end

% 
% for rr = 1 : length(subs)
%     sub_root = subs{rr}
% 
%     tmp = dir(fullfile(data_root, sub_root));
%     sub_folders = cell(1, length(tmp) - 2);
%     t = 0;
%     for i = 1 : length(tmp)
%         name = tmp(i).name;
%         if name(1) ~= '.'
%             t = t + 1;
%             sub_folders{t} = name;
%         end
%     end
% 
%     parfor i = 1 : length(sub_folders)
%         exp_folder_CENSREC(data_root, sub_root, sub_folders{i}, 'baseline');
%     end
% 
% end
