root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));


%%
data_root = '~/data/dataset/CENSREC-1-C/speechdata';
addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

% fid = fopen('~/Dropbox/scratch/changepoint/stats/auc_speech_online.txt', 'w');
avg_baseline = 0.0;
avg_ours = 0.0;
lw = 2.5;

for i = 1 : 20
    i
    filename = sprintf('~/scratch/online_speech_params/ours_online_results_%s_%s.mat', sub_roots{i}, sub_folders{i});
    load(filename);
    avg_ours = avg_ours + ours_auc(3);
    [tpr_ours, fpr_ours, ~] = roc(ours_labels{3}, ours_score{3});
    filename = sprintf('~/scratch/online_speech_params/baseline_online_results_%s_%s.mat', sub_roots{i}, sub_folders{i});
    load(filename);
    avg_baseline = avg_baseline + baseline_auc(3);
    [tpr_baseline, fpr_baseline, ~] = roc(baseline_labels{3}, baseline_score{3});
    
    % [baseline_auc, baseline_labels, baseline_score] = compute_auc(baseline_stats, baseline_break, 0);
    % [ours_auc, ours_labels, ours_score] = compute_auc(ours_stats, ours_break, B - 1);
    % [baseline_auc, baseline_labels, baseline_score] = auc_within_range(baseline_stats, baseline_break, 0, 50);
    % [ours_auc, ours_labels, ours_score] = auc_within_range(ours_stats, ours_break, B - 1, 50);
    [fp, tp] = get_fp_tp(ours_stats, ours_break, ours_thresholds, 49, 100);
    figure('Visible','on');
    plot(fpr_ours, tpr_ours, 'r', fpr_baseline, tpr_baseline, 'b', 'Linewidth', lw);
    hold;
    scatter(fp, tp);
    plot([fp, fp], [0, 1], '--g', 'linewidth', lw);
    h = legend('ours', 'baseline');
    set(h, 'fontsize', 20);
    xlabel('False Positive Rate', 'FontSize', 20);
    ylabel('True Positive Rate', 'FontSize', 20);
    text(fp + 0.2, 0.5, sprintf('fp=%.4f', fp));
    title(sprintf('%s/%s', sub_roots{i}, sub_folders{i}), 'Interpreter', 'none');
    % set(findall(gcf,'type','text'),'fontSize',15);
    set(findall(gcf,'type','axes'),'fontsize',15);
    set(gca,'dataaspectratio',[1 1 1]);
    set(gcf, 'PaperPositionMode', 'auto', 'PaperSize', [6, 6]);
    print('-dpdf', [sprintf('~/Dropbox/scratch/changepoint/figures/auc_%s_%s.pdf', sub_roots{i}, sub_folders{i})], '-r300');
%     
%    fprintf(fid, '%s %s ours %.4f\n', sub_roots{i}, sub_folders{i}, ours_auc(3));
end

avg_baseline = avg_baseline / 20.0
avg_ours = avg_ours / 20.0
% fclose(fid);