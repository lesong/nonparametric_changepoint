function exp_folder_CENSREC(data_root, sec, sub_folder, type)
    B = 50;
    ref_len = 30 * B;
    test_blocks = 6;
    n = 50;
    k = 50;
    alpha = 0.1;
    buf_len = 2 * n - 2 + k;
    
    root_folder = '~/Research/nonparametric_changepoint/real_data_exp';
    addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));
    mkdir(fullfile('results', sec, sub_folder));
    
    filenames = dir(fullfile(data_root, sec, sub_folder, '*.raw'));
    seg_names = cell(1, length(filenames));
    for i = 1 : length(filenames)
        t = strsplit(filenames(i).name, '.');
        seg_names{i} = t{1};
    end
    
    for i = 1 : length(seg_names)
        [whole_sequence, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
        for j = 1 : size(segments, 1)
            fprintf('sub_sequence %d/%d of sequence %d/%d in folder %s\n', j, size(segments, 1), i, length(seg_names), sub_folder);
         
            change_point = segments(j, 1);
            sequence = whole_sequence(:, change_point - (30 + test_blocks) *  B : change_point + test_blocks * B);
            if strcmp(type, 'ours')
                options.KernelType = 'Gaussian';
                ref_data = sequence(:, 1 : ref_len)';
                dist_mat = EuDist2(ref_data);
                options.t = median(dist_mat(dist_mat ~= 0)) * 10;
                options.prob = 0.02;
            
                [normed_mmd_stats, threshold, var_zb] = calc_normed_mmd_seq(sequence, ref_len, B, options);
                score = [];
            else
            % baseline 
                subseq = sequence;
                score1 = change_detection(subseq, n, k, alpha);
                score2 = change_detection(subseq(:,end:-1:1),n,k,alpha);
                score2 = score2(end:-1:1);

                score = score1 + score2;
            end
            
            filename = sprintf('results/%s/%s/%s_%s_%d.mat', sec, sub_folder, seg_names{i}, type, j);
            
            save(filename, 'options', 'score', 'normed_mmd_stats', 'threshold', 'var_zb', 'sequence', 'ref_len', 'B');
        end
    end
end
