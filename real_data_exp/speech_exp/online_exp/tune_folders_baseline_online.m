function tune_folders_baseline_online(folder_idx, k, n, alpha)

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(fullfile(root_folder, 'speech_exp'));

%%
data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

val_idx = [];
ours_name = sprintf('~/scratch/online_speech_params/online_params_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});
load(ours_name);

lambda_list = 10.^[-3:1:1];
[bestauc, sigma, lambda] = tune_baseline_params_online(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, B, N, k, n, alpha, lambda_list, val_idx, 50);

filename = sprintf('baseline_online_params_%s_%s_%d_%d.mat', sub_roots{folder_idx}, sub_folders{folder_idx}, B, N);
save(filename, 'k', 'n', 'alpha', 'sigma', 'lambda', 'val_idx', 'bestauc');

end