function [bestauc, bestT, val_idx] = tune_ours_params_online(data_root, sec, sub_folder, B, N, t_range, width)
    test_blocks = 6;
    
    filenames = dir(fullfile(data_root, sec, sub_folder, '*.raw'));
    seg_names = cell(1, length(filenames));
    for i = 1 : length(filenames)
        t = strsplit(filenames(i).name, '.');
        seg_names{i} = t{1};
    end
    
    val_idx = zeros(1, length(seg_names));
    for i = 1 : length(seg_names)
        [~, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
        
        val_idx(i) = randsample(size(segments, 1), 1);
    end
    
    bestauc = -1;
    bestT = -1;
            for t_idx = 1 : length(t_range)
                t_mul = t_range(t_idx);
                ref_len = B * N;
                ref_data = [];
                bad = 0;
                for i = 1 : length(seg_names)
                    [whole_seq, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
                    change_point = segments(val_idx(i), 1);
                    seq_range = change_point - (N + test_blocks) *  B : change_point + test_blocks * B;
                    
                    if val_idx(i) == 1
                        left = 1;
                    else
                        left = segments(val_idx(i) - 1, 2) + 1;
                    end
                    right = segments(val_idx(i), 2);
                        
                    if seq_range(end) > right || seq_range(1) < left
                        bad = 1;
                        break;
                    end
                    seq = whole_seq(:, seq_range);
                    ref_data = [ref_data; seq(:, 1 : ref_len)'];
                end
                if bad
                    continue;
                end
                
                len = size(ref_data, 1);
                ref_data = ref_data(randsample(len, min([10000, len])), :);
                dist_mat = EuDist2(ref_data);
                t = median(dist_mat(dist_mat ~= 0)) * t_mul;
                
                [~, mmd, points, ~] = ours_run_folder(data_root, sec, sub_folder, B, N, t, val_idx, 1);
                auc = auc_within_range(mmd, points, B - 1, width);
                if auc > bestauc
                    bestauc = auc;
                    bestT = t;
                    fprintf('current best: B=%d, N=%d, t=%.4f, auc=%.4f\n', B, N, t, auc);
                end
            end
end
