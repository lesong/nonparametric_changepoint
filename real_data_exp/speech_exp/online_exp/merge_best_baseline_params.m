k_range = [50, 70, 100];
n_range = [30, 50];
alpha_range = [0.0, 0.1];

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(fullfile(root_folder, 'speech_exp'));

data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

for i = 1 : num_folders
    cur_best = 0;
    cur_k = -1;
    cur_t = -1;
    cur_n = -1;
    cur_alpha = -1;
    cur_sigma = -1;
    cur_lambda = -1;
    cur_idx = [];
    for k_idx = 1 : length(k_range)
        k = k_range(k_idx);
        for n_idx = 1 : length(n_range)
            n = n_range(n_idx);
            for a_idx = 1 : length(alpha_range)
                alpha = alpha_range(a_idx);
                filename = sprintf('baseline_online_params_%s_%s_%d_%d_%.3f.mat', sub_roots{i}, sub_folders{i}, k, n, alpha);
                if ~exist(filename)
                    continue;
                end
                load(filename);
                if bestauc > cur_best
                    cur_best = bestauc;
                    cur_k = B;
                    cur_alpha = alpha;
                    cur_n = n;
                    cur_idx = val_idx;
                    cur_sigma = sigma;
                    cur_lambda = lambda;
                end
            end
        end        
    end
    k = cur_k;
    n = cur_n;
    sigma = cur_sigma;
    val_idx = cur_idx;
    alpha = cur_alpha;
    lambda = cur_lambda;
    bestauc = cur_best;
    
    filename = sprintf('~/scratch/online_speech_params/baseline_online_params_%s_%s.mat', sub_roots{i}, sub_folders{i});
    save(filename, 'k', 'n', 'alpha', 'sigma', 'lambda', 'val_idx', 'bestauc');
end