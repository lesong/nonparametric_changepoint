B_range = [50, 100, 150];
N_range = [20, 30, 40];


root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(fullfile(root_folder, 'speech_exp'));

data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

for i = 1 : num_folders
    cur_best = 0;
    cur_B = -1;
    cur_t = -1;
    cur_N = -1;
    cur_idx = [];
    for b_idx = 1 : length(B_range)
        B = B_range(b_idx);
        for n_idx = 1 : length(N_range)
            N = N_range(n_idx);
            filename = sprintf('online_params_%s_%s_%d_%d.mat', sub_roots{i}, sub_folders{i}, B, N);
            if ~exist(filename)
                continue;
            end
            load(filename);
            if bestauc > cur_best
                cur_best = bestauc;
                cur_B = B;
                cur_t = T;
                cur_N = N;
                cur_idx = val_idx;
            end
        end        
    end
    B = cur_B;
    N = cur_N;
    T = cur_t;
    val_idx = cur_idx;
    filename = sprintf('~/scratch/online_speech_params/online_params_%s_%s.mat', sub_roots{i}, sub_folders{i});
    save(filename, 'B', 'N', 'T', 'val_idx');
end