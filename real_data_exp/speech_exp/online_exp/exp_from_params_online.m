function exp_from_params_online(folder_idx, method)
    
root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(fullfile(root_folder, 'speech_exp'));
data_root = '~/data/dataset/CENSREC-1-C/speechdata';
addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end
param_file = sprintf('~/scratch/online_speech_params/online_params_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});

waited = 0;
while ~exist(param_file)
    fprintf('waited %d\n', waited);
    pause(60);
    waited = waited + 1;
end

load(param_file);

width_range = [10, 50, 100, 150, 200];

if method == 1
    [ours_seq, ours_stats, ours_break, ours_thresholds] = ours_run_folder(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, B, N, T, val_idx, 0); 
    ours_labels = cell(1, length(width_range));
    ours_score = cell(1, length(width_range));
    ours_auc = zeros(1, length(width_range));
    
    for w_idx = 1 : length(width_range)
        w = width_range(w_idx);
        [ours_auc(w_idx), ours_labels{w_idx}, ours_score{w_idx}] = auc_within_range(ours_stats, ours_break, B - 1, w);
    end

    filename = sprintf('~/scratch/online_speech_params/ours_online_results_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});
    save(filename, 'ours_seq', 'ours_stats', 'ours_break', 'ours_thresholds', 'ours_auc', 'ours_labels', 'ours_score');
else
    baseline_param_file = sprintf('~/scratch/online_speech_params/baseline_online_params_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});
    load(baseline_param_file);
    [baseline_seq, baseline_stats, baseline_break] = baseline_run_folder(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, B, N, k, n, best_alpha, best_sigma, best_lambda, val_idx, 0);
    baseline_labels = cell(1, length(width_range));
    baseline_score = cell(1, length(width_range));
    baseline_auc = zeros(1, length(width_range));
    
    for w_idx = 1 : length(width_range)
        w = width_range(w_idx);
        [baseline_auc(w_idx), baseline_labels{w_idx}, baseline_score{w_idx}] = auc_within_range(baseline_stats, baseline_break, 0, w);
    end
    
    filename = sprintf('~/scratch/online_speech_params/baseline_online_results_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});
    save(filename, 'baseline_seq', 'baseline_stats', 'baseline_break', 'baseline_auc', 'baseline_labels', 'baseline_score');
end

end