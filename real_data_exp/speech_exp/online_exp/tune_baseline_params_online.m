function [bestauc, bestSigma, bestLambda] = tune_baseline_params_online(data_root, sec, sub_folder, B, N, k, n, alpha, lambda_list, val_idx, width)
    test_blocks = 6;
    buf_len = 2 * n - 2 + k;
    
    filenames = dir(fullfile(data_root, sec, sub_folder, '*.raw'));
    seg_names = cell(1, length(filenames));
    for i = 1 : length(filenames)
        t = strsplit(filenames(i).name, '.');
        seg_names{i} = t{1};
    end
    
    bestauc = 0;
    bestSigma = -1;
    bestLambda = -1;
    for l_idx = 1 : length(lambda_list)
        lambda = lambda_list(l_idx);
        
        all_sigmas = [];
        for i = 1 : length(seg_names)
            fprintf('preprocessing sequence %d\n', i);
            [whole_seq, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
            change_point = segments(val_idx(i), 1);
            seq_range = change_point - (N + test_blocks) *  B : change_point + test_blocks * B;
            sequence = whole_seq(:, seq_range);
            subseq = sequence(end - 2 * test_blocks * B - buf_len : end);
            
            [~, sigmas, ~] = change_detection_origin(subseq, n, k, alpha);
            all_sigmas = [all_sigmas, sigmas];
        end
        
        s_list = [mean(all_sigmas), median(all_sigmas), mode(all_sigmas)];
        
        for s_idx = 1 : length(s_list)
            sigma = s_list(s_idx);
            
            [~, scores, points] = baseline_run_folder(data_root, sec, sub_folder, B, N, k, n, alpha, sigma, lambda, val_idx, 1);
            
            auc = auc_within_range(scores, points, 0, width);
            if auc > bestauc
                bestauc = auc;
                bestLambda = lambda;
                bestSigma = sigma;
                fprintf('current best: lambda=%.4f, sigma=%.4f, auc=%.4f\n', lambda, sigma, auc);
            end
        end
    end
end