function tune_folders(folder_idx)

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));


%%
data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

sub_folders

B_range = [50]; 
N_range = [30];
t_range = [1];

[B, N, T, val_idx] = tune_params(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, B_range, N_range, t_range);
filename = sprintf('~/scratch/speech_params/params_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});
save(filename, 'B', 'N', 'T', 'val_idx');

end