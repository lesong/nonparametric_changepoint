clear;
clc;

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));

data_root = '~/data/dataset/CENSREC-1-C/speechdata';
addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));

sub_roots = {'recording', 'recording', 'recording', 'recording', 'simulate', 'simulate'};

sub_folders = {'RESTAURANT_SNR_HIGH', 'RESTAURANT_SNR_LOW', 'STREET_SNR_HIGH', 'STREET_SNR_LOW', ...
    'clean1', 'N1_SNR20'};

options.KernelType = 'Gaussian';
options.prob = 0.02;

for i = 3 : 3
    filename = sprintf('~/scratch/speech_params/results_%s_%s.mat', sub_roots{i}, sub_folders{i});
    load(filename);
    
    idx = 11;
    sequence = ours_seq{idx}; 
    change_point = ours_break(idx);
    ref_len = (length(sequence) - length(ours_stats{1}));
    ref_data = sequence(:, 1 : ref_len)';
    dist_mat = EuDist2(ref_data);
    options.t = median(dist_mat(dist_mat ~= 0)) * 8
    [normed_mmd_stats, threshold] = calc_normed_mmd_seq(sequence, ref_len, 50, options);
    
    d = 500;
    left = change_point - d;
    if left <= 0
        left = 1;
    end
    right = change_point + d;
    if right > length(normed_mmd_stats)
        right = length(normed_mmd_stats);
    end
    range = [left, right];
    b_offset = -2.0;

    output = sprintf('~/Dropbox/scratch/changepoint/figures/demo_%s_%s.pdf', sub_roots{i}, sub_folders{i});
    sequence = sequence(end - length(normed_mmd_stats) + 1 : end);
    plot_example(sequence, normed_mmd_stats, threshold, change_point, range, b_offset, output);

%     figure;
%     idx = 1;
%     seq = ours_seq{idx};
%     
%     subplot(2, 1, 2);
%     plot(ours_stats{idx}, 'r', 'linewidth', 2);
%     title('Online Statistics');
%     
%     subplot(2,1,1);
%     
%     plot(seq(end - length(ours_stats{idx}) + 1 : end), 'b', 'linewidth', 2);
%     title('Original Signal');
    
%     legend('ours', 'baseline');
%     xlabel('False Positive Rate');
%     ylabel('True Positive Rate');
%     title(sprintf('ROC on %s/%s', sub_roots{i}, sub_folders{i}), 'Interpreter', 'none');
    set(findall(gcf,'type','text'),'fontSize',15);
    set(findall(gcf,'type','axes'),'fontsize',15);
    set(gcf, 'PaperPositionMode', 'auto', 'PaperSize', [6, 5]);
    print('-dpdf', [sprintf('~/Dropbox/scratch/changepoint/figures/demo_%s_%s.pdf', sub_roots{i}, sub_folders{i})], '-r300');

end