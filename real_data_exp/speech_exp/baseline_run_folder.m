function [evaluated_seq, scores, break_points] = baseline_run_folder(data_root, sec, sub_folder, B, N, k, n, alpha, sigma, lambda, seg_idx, is_validating)
    test_blocks = 6;
    buf_len = 2 * n - 2 + k;
    
    filenames = dir(fullfile(data_root, sec, sub_folder, '*.raw'));
    seg_names = cell(1, length(filenames));
    for i = 1 : length(filenames)
        t = strsplit(filenames(i).name, '.');
        seg_names{i} = t{1};
    end
    
    num_seq = 0;
    scores = {};
    break_points = [];
    evaluated_seq = {};
    
    for i = 1 : length(seg_names)
        [whole_sequence, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
        
        if is_validating
            range = [seg_idx(i)];
        else
            range = [1 : seg_idx(i) - 1, seg_idx(i) + 1 : size(segments, 1)];
        end
        for j_idx = 1 : length(range)
            j = range(j_idx);
            num_seq = num_seq + 1;
            fprintf('sub_sequence %d/%d of sequence %d/%d in folder %s\n', j, size(segments, 1), i, length(seg_names), sub_folder);
            change_point = segments(j, 1);
            sequence = whole_sequence(:, change_point - (N + test_blocks) *  B : change_point + test_blocks * B);            
            subseq = sequence(:, end - 2 * test_blocks * B - buf_len : end);
            
            scores{num_seq} = change_detection_revised(subseq, n, k, alpha, sigma, lambda);
            break_points(num_seq) = test_blocks * B + 1;
            evaluated_seq{num_seq} = subseq;
        end
    end
end