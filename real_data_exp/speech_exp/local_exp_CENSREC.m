%%
clear;
clc;

%%

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';
addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));

%%
% file_prefix = ['~/data/dataset/CENSREC-1-C', '/speechdata/recording/RESTAURANT_SNR_HIGH/FDP_9O_8629_59Z_1_8O4_4_3_O357451_1Z665_9'];
file_prefix = ['~/data/dataset/CENSREC-1-C', '/speechdata/simulate/clean1/FBW'];

[sequence, segments] = load_raw_seg(file_prefix);

%%
B = 50;
ref_len = 30 * B;

change_point = segments(9, 1);

sequence = sequence(:, change_point - 36 * B : change_point + 6 * B);

%% ours
% options.KernelType = 'Gaussian';
% 
% ref_data = sequence(:, 1 : ref_len)';
% dist_mat = EuDist2(ref_data);
% options.t = median(dist_mat(dist_mat ~= 0)) * 5
% %options.t = 1
% options.prob = 0.02;
% 
% [normed_mmd_stats, threshold] = calc_normed_mmd_seq(sequence, ref_len, B, options);
% fprintf('done\n');

%% baseline - density ratio


addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));
n = 30;
k = B;
alpha = 0.0;
buf_len = 2 * n - 2 + k;
subseq = sequence(end - 12 * B - buf_len : end);

% [score1, sigmas, lambdas] = change_detection_origin(subseq, n, k, alpha);
% score2 = change_detection(subseq(:,end:-1:1),n,k,alpha);
% score2 = score2(end:-1:1);
score2 = change_detection_revised(subseq, n, k, alpha, mode(sigmas), 0.01);
% score = score1 + score2;
score = score1;

%%
% plot(normed_mmd_stats / 10);
% hold;
% plot(sequence(ref_len + 1 : end) / 100);
% plot(score);
% 
% labels = [zeros(1, 5 * B), 10 * ones(1, 5 * B + 1)];
% plot(labels);
% 
% legend({'ours', 'signal', 'density-ratio', 'groundtruth'});

