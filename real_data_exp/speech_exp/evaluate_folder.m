function auc = evaluate_folder(foldername)
    files = dir(fullfile(foldername, '*.mat'));
    sequences = {};
    change_points = {};
    for i = 1 : length(files)
        load(fullfile(foldername, files(i).name));
        sequences{i} = normed_mmd_stats;
        change_points{i} = 300 - B + 1;
    end
    [auc, ~, ~] = compute_auc(sequences, change_points, 1);
end