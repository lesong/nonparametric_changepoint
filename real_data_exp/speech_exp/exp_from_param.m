function exp_from_param(folder_idx)

root_folder = '~/data/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));


%%
data_root = '~/data/dataset/CENSREC-1-C/speechdata';
addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end
param_file = sprintf('~/scratch/speech_params/params_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});

waited = 0;
while ~exist(param_file)
    pause(60);
    fprintf('waited %d\n', waited);
    waited = waited + 1;
end

load(param_file);
alpha = 0.0;
lambda = 0.1;

[ours_seq, ours_stats, ours_break, ours_thresholds] = ours_run_folder(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, B, N, T, val_idx, 0); 
[baseline_seq, baseline_stats, baseline_break] = baseline_run_folder(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, B, N, alpha, T, lambda, val_idx, 0); 

[ours_auc, ours_labels, ours_score] = compute_auc(ours_stats, ours_break, B - 1);
[baseline_auc, baseline_labels, baseline_score] = compute_auc(baseline_stats, baseline_break, 0);

filename = sprintf('~/scratch/speech_params/results_%s_%s.mat', sub_roots{folder_idx}, sub_folders{folder_idx});
save(filename, 'ours_seq', 'ours_stats', 'ours_break', 'ours_thresholds', 'baseline_seq', ...
    'baseline_stats', 'baseline_break', 'ours_auc', 'ours_labels', 'ours_score', 'baseline_auc', 'baseline_labels', 'baseline_score');

end
