# change point detection 

## folder structure

- **baseline**


	baseline algorithm implementation

- **bci_exp**


	experiment entry point of BCI (EGG) dataset

- **cdnet_exp**


	experiment entry point of CDNet video dataset

- **speech_exp**


	experiment entry point of speech dataset

- **dataset**


	sample data of real datasets (for debugging)

- **exploration**


	some utility functions

- **shuang**


	implementation of Shuang's algorithm


## function interface

- **shuang/calc_normed_mmd_seq.m**

	
	**Given:** sequential data (dim x length), reference length, block size, kernel options


	**Return:** normalized mmd stats; threshold


- **shuang/get_var_zb.m**


	**Given:** Reference data, block size, kernel options


	**Return:** estimated mmd variance


- **shuang/get_threshold.m**


	**Given:** block size, T (fixed to 100), prob (fixed to 0.02)

	**Return:** threshold for normalized mmd of change point

