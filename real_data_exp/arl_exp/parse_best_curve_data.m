B = 50;

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';
addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(fullfile(root_folder, 'speech_exp'));
data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

selected = cell(1, num_folders);
est_arl = cell(1, num_folders);
thresholds = cell(1, num_folders);

for i = 1 : num_folders
    
    filenames = dir(sprintf('arl_%s_%s_*.mat', sub_roots{i}, sub_folders{i}));
    filename = filenames(1).name;
    load(filename);
    thresholds{i} = b_range;
    selected{i} = arls;

    est_arl{i} = zeros(1, length(thresholds{i}));
    for j = 1 : length(thresholds{i})
        b = thresholds{i}(j);
        est_arl{i}(j) = calc_ARL(b, B);
    end
    best_rmse = Inf;

    for j = 1 : length(filenames)
        filename = filenames(j).name;
        load(filename);
        rmse = sqrt(sum((est_arl{i} - arls) .^ 2) / length(thresholds{i}));
        if rmse < best_rmse
            best_rmse = rmse;
            selected{i} = arls;
        end
    end
end

