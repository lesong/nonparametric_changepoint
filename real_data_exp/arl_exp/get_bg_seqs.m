function [bg_sequences] = get_bg_seqs(data_root, sec, sub_folder, offset)
    filenames = dir(fullfile(data_root, sec, sub_folder, '*.raw'));
    seg_names = cell(1, length(filenames));
    for i = 1 : length(filenames)
        t = strsplit(filenames(i).name, '.');
        seg_names{i} = t{1};
    end
    
    bg_sequences = {};
    num_seqs = 0;
    for i = 1 : length(seg_names)
        [whole_sequence, segments] = load_raw_seg(fullfile(data_root, sec, sub_folder, seg_names{i}));
        
        num_seqs = num_seqs + 1;
        bg_sequences{num_seqs} = whole_sequence(offset : segments(1, 1) - offset);
        for j = 2 : size(segments, 1)
            num_seqs = num_seqs + 1;
            bg_sequences{num_seqs} = whole_sequence(segments(j - 1, 2) + offset : segments(j, 1) - offset);
        end
    end
end