function arl = get_arl_hat(bg_seqs, exp_rounds, B, N, t_mul, test_len, threshold)
    ref_len = B * N;
    options.KernelType = 'Gaussian';
    options.prob = 0.02;
    
    cur_round = 0;
    seq_idxes = randperm(length(bg_seqs));
    exceed_cnt = 0;
    
    for i = 1 : length(seq_idxes)
        sequence = bg_seqs{seq_idxes(i)};
        ref_data = sequence(:, 1 : ref_len)';
        dist_mat = EuDist2(ref_data);
        options.t = median(dist_mat(dist_mat ~= 0)) * t_mul;
        
        for j = ref_len + 1 : test_len : length(sequence) - test_len + 1
            test_seq = [ref_data', sequence(:, j : j + test_len - 1)];
            [stats, ~] = calc_normed_mmd_seq(test_seq, ref_len, B, options);
            
            if ~isempty(find(stats > threshold))
                exceed_cnt = exceed_cnt + 1;
            end
            cur_round = cur_round + 1
            if cur_round >= exp_rounds
                break;
            end
        end
        if cur_round >= exp_rounds
            break;
        end
    end
    
    arl = test_len * double(exp_rounds) / double(exceed_cnt);
end