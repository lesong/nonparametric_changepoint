function arl_speech_boot(folder_idx, t_mul)

B = 50;
N = 20;
test_len = 100;
exp_num = 1000;

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';
addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(fullfile(root_folder, 'speech_exp'));
data_root = '~/data/dataset/CENSREC-1-C/speechdata';

subs = {'recording', 'simulate'};
sub_roots = {};
sub_folders = {};

num_folders = 0;
for rr = 1 : length(subs)
    sub_root = subs{rr};

    tmp = dir(fullfile(data_root, sub_root));
    folders = cell(1, length(tmp) - 2);
    t = 0;
    for i = 1 : length(tmp)
        name = tmp(i).name;
        if name(1) ~= '.'
            t = t + 1;
            folders{t} = name;
        end
    end

    for i = 1 : length(folders)
        num_folders = num_folders + 1;
        sub_roots{num_folders} = sub_root;
        sub_folders{num_folders} = folders{i};
    end
end

bg_seqs = get_bg_seqs(data_root, sub_roots{folder_idx}, sub_folders{folder_idx}, 100);

idx = 0;
arls = [];
b_range = 3 : 0.2 : 5;

for b_idx = 1 : length(b_range)
    b = b_idx;
    arls(idx) = get_arl_hat(bg_seqs, exp_num, B, N, t_mul, test_len, b);
end

filename = sprintf('arl_%s_%s_%.4f.mat', sub_roots{folder_idx}, sub_folders{folder_idx}, t_mul);
save(filename, 'arls', 'b_range', 't_mul');

end