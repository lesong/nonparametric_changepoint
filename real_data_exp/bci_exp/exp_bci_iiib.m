%%
clear;
clc;

root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));

%%
load(fullfile(root_folder, 'dataset/bci/IIIb/X11b.mat'));

sequence = s(HDR.TRIG(1) : end, :)';
sequence = sequence(:, 1 : 10000);
ref_len = 700;
B = 10;

seg_labels = HDR.Classlabel(2 : end);
seg_labels(isnan(seg_labels)) = 0;
seg_labels = seg_labels - 1;


%%
options.KernelType = 'Gaussian';

ref_data = sequence(:, 1 : ref_len)';
dist_mat = EuDist2(ref_data);
options.t = median(dist_mat(dist_mat ~= 0))
options.prob = 0.02

[normed_mmd_stats, threshold] = calc_normed_mmd_seq(sequence, ref_len, B, options);
fprintf('done\n');
