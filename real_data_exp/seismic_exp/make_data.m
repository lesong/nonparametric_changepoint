%%
clear;
clc;


%%
fid = fopen('../dataset/seismic/earthquake1.txt', 'r');

raw_signals = fscanf(fid, '%f');

fclose(fid);

idx = find(abs(raw_signals) <= 7000);

signals = raw_signals(idx);