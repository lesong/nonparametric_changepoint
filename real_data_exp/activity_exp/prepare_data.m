%%
clear;
clc;

%%
person = 'b';

raw = csvread(sprintf('../dataset/activity/%s_data.csv', person));

stamps = raw(:, 1);
sequence = sqrt(sum(raw(:, 2 : end) .^ 2, 2));

raw_label = readtable(sprintf('../dataset/activity/%s_label.csv', person), 'Delimiter', ',', 'ReadVariableNames', false);
cell_labels = table2cell(raw_label);

num_segs = 0;
segs = {};
labels = {};
ground_truth = zeros(1, length(sequence));
for i = 1 : size(cell_labels, 1)
    if isnan(cell_labels{i, 1}) || isnan(cell_labels{i, 2})
        continue;
    end
    l = find_position(stamps, cell_labels{i, 1}, 1);
    r = find_position(stamps, cell_labels{i, 2}, 0);
    num_segs = num_segs + 1;
    segs{num_segs} = sequence(l : r);
    ground_truth(l : r) = 1;
    labels{num_segs} = get_label_idx(cell_labels{i, 3});
end

sequences = {};
points = {};
num_seq = 0;
label_pairs = [];
for i = 1 : length(labels) - 1
    for j = i + 1 : length(labels)
        if labels{i} == labels{j}
            continue;
        end
        if length(segs{i}) + length(segs{j}) < 1200
            continue;
        end
        num_seq = num_seq + 1;
        if length(segs{i}) > length(segs{j})
            seq = [segs{i}', segs{j}'];
            change_point = length(segs{i}) + 1;
            label_pairs = [label_pairs; [labels{i}, labels{j}]];
        else
            seq = [segs{j}', segs{i}'];
            change_point = length(segs{j}) + 1;
            label_pairs = [label_pairs; [labels{j}, labels{i}]];
        end
        sequences{num_seq} = seq;
        points{num_seq} = change_point;
    end
end

save(sprintf('%s.mat', person), 'sequences', 'points', 'label_pairs');
