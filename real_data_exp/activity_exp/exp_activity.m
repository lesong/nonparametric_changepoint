function [ours_seqs, baseline_seqs, parsed_labels, parsed_points, ours_stats, baseline_stats, ours_reflens] = exp_activity(B, N, t_mul)
    load act.mat;
    test_blocks = 6;
    
    ours_seqs = cell(1, 6);
    baseline_seqs = cell(1, 6);
    parsed_labels = cell(1, 6);
    parsed_points = cell(1, 6);
    ours_reflens = cell(1, 6);
    options.KernelType = 'Gaussian';
    options.prob = 0.02;
    ours_stats = cell(1, 6);
    baseline_stats = cell(1, 6);
    
    for type = 1 : 1
        cur_pairs = [];
        cur_points = {};
        cur_seqs = {};
        num_seqs = 0;
        for i = 1 : length(points)
            if label_pairs(i, 1) ~= type
                continue;
            end
            num_seqs = num_seqs + 1;
            cur_pairs = [cur_pairs; label_pairs(i, :)];
            cur_points{num_seqs} = points{i};
            cur_seqs{num_seqs} = sequences{i};
        end
        
        ours_seqs{type} = {};
        baseline_seqs{type} = {};
        parsed_labels{type} = cur_pairs;
        ref_data = [];
        ours_reflens{type} = zeros(1, num_seqs);
        
        if num_seqs == 0
            continue;
        end
        
        for i = 1 : num_seqs
            ref_len = N * B;
            sequence = cur_seqs{i};
            test_before = test_blocks * B;
            if cur_points{i} - ref_len - 1 < test_before
                test_before = cur_points{i} - ref_len - 1;
            end
            test_after = 6 * B;
            if cur_points{i} + test_after > length(sequence)
                test_after = length(sequence) - cur_points{i};
            end
            ref_data = [ref_data; sequence(:, 1 : ref_len)'];
            
            ours_seqs{type}{i} = sequence(:, cur_points{i} - ref_len - test_before : cur_points{i} + test_after);
            buf_len = 2 * N - 2 + B;
            baseline_seqs{type}{i} = sequence(end - test_before - test_after - 1 - buf_len : end);
            
            cur_points{i} = test_before + 1;
            
            ours_reflens{type}(i) = ref_len;
        end
        
        parsed_points{type} = cur_points;
        
        len = size(ref_data, 1);
        ref_data = ref_data(randsample(len, min([10000, len])), :);
        dist_mat = EuDist2(ref_data);
        options.t = median(dist_mat(dist_mat ~= 0)) * t_mul;
        
        for i = 1 : num_seqs
            i
            % [ours_stats{type}{i}, threshold{type}{i}] = calc_normed_mmd_seq(ours_seqs{type}{i}, ours_reflens{type}(i), B, options);
            % [baseline_stats{type}{i}, ~, ~] = change_detection_origin(baseline_seqs{type}{i}, N, B, 0.0);
            baseline_stats{type}{i} = change_detection_origin(baseline_seqs{type}{i}, 50, 50, 0.1);
        end
    end
end