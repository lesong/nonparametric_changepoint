function idx = get_label_idx(label_string)
    class = strsplit(label_string, ';');
    class = class{1};
    idx = -1;
    if strcmp(class, 'walk')
        idx = 1;
    end
    if strcmp(class, 'jog')
        idx = 1;
    end
    if strcmp(class, 'stairUp')
        idx = 2;
    end
    if strcmp(class, 'stairDown')
        idx = 2;
    end
    if strcmp(class, 'elevatorUp')
        idx = 3;
    end
    if strcmp(class, 'elevatorDown')
        idx = 3;
    end
    if strcmp(class, 'escalatorUp')
        idx = 4;
    end
    if strcmp(class, 'escalatorDown')
        idx = 4;
    end
    if strcmp(class, 'movingWalkway')
        idx = 5;
    end
    if strcmp(class, 'stay')
        idx = 6;
    end
end