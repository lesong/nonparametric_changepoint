%%
clear;
clc;

%%
root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));

load('a.mat');

%%
t = [];
for i = 1 : size(label_pairs, 1)
    if label_pairs(i, 1) == 2 && label_pairs(i, 2) == 4
        t = [t; i];
    end
end
% t = find(label_pairs(:, 1) == 2);
idx = 139;
output = sprintf('~/Dropbox/scratch/changepoint/figures/act_demo_%d_%d.pdf', label_pairs(idx, 1), label_pairs(idx, 2));

sequence = sequences{idx};

B = 100;
ref_len = 10 * B;
test_before = 6 * B;
if points{idx} - ref_len - 1 < test_before
    test_before = points{idx} - ref_len - 1;
end

test_after = 6 * B;
if points{idx} + test_after > length(sequence)
    test_after = length(sequence) - points{idx};
end

sequence = sequence(:, points{idx} - ref_len - test_before : points{idx} + test_after);

%% ours
options.KernelType = 'Gaussian';

ref_data = sequence(:, 1 : ref_len)';
dist_mat = EuDist2(ref_data);
options.t = median(dist_mat(dist_mat ~= 0)) * 10
options.prob = 0.02;
label_pairs(idx, :)
[normed_mmd_stats, threshold] = calc_normed_mmd_seq(sequence, ref_len, B, options);
fprintf('done\n');

%%
sequence = sequence(end - length(normed_mmd_stats) + 1 : end);
change_point = test_before + 1;
d = 200;
left = change_point - d;
if left <= 0
    left = 1;
end
right = change_point + d;
if right > length(normed_mmd_stats)
    right = length(normed_mmd_stats);
end
range = [left, right];
b_offset = 300;

plot_example(sequence, normed_mmd_stats, threshold, test_before + 1, range, b_offset, output);

%%
% subplot(2,1,1);    
% plot(sequence(end - length(normed_mmd_stats) + 1 : end), 'b', 'linewidth', 2);
% hold;
% plot([test_before, test_before], ylim, '-.g', 'linewidth', 2);
% title('Original Signal');
% range = xlim;
% xlim(range);
% 
% subplot(2, 1, 2);
% point = find(normed_mmd_stats > threshold);
% if length(point) > 0
%     point = point(1);
% else
%     point = length(normed_mmd_stats);
% end
% plot(normed_mmd_stats(1 : end), 'm', 'linewidth', 2);
% hold;
% xlim(range);
% plot(xlim, [threshold, threshold], '--r', 'linewidth', 2);
% title('Online Statistics');
% text(0, threshold + 2.0, sprintf('b=%.2f', threshold));
% 

    
%     legend('ours', 'baseline');
%     xlabel('False Positive Rate');
%     ylabel('True Positive Rate');
%     title(sprintf('ROC on %s/%s', sub_roots{i}, sub_folders{i}), 'Interpreter', 'none');
%     set(findall(gcf,'type','text'),'fontSize',15);
%     set(findall(gcf,'type','axes'),'fontsize',15);
% 
% 
%     set(gcf, 'PaperPositionMode', 'auto', 'PaperSize', [6, 5]);
%     print('-dpdf', [], '-r300');

