function pos = find_position(sequence, val, left)
    if left == 1
        pos = find(sequence > val);
        pos = pos(1);
    else
        pos = find(sequence > val);
        pos = pos(1) - 1;
    end
end