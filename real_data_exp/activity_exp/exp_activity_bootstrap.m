%%
clear;
clc;

%%
root_folder = '~/Research/nonparametric_changepoint/real_data_exp';

addpath(fullfile(root_folder, 'shuang'));
addpath(fullfile(root_folder, 'exploration'));
addpath(genpath(fullfile(root_folder, 'baseline/density_ratio')));

[ours_seqs, baseline_seqs, parsed_labels, parsed_points, ours_stats, baseline_stats, ours_reflens] = exp_activity(100, 10, 2);