function [rPE, g_nu, g_re] = RelULSIF_revised( x_de, x_nu, x_re, x_ce, alpha, sigma_chosen, lambda_chosen)
rng default
[~,n_nu] = size(x_nu);
[~,n_de] = size(x_de);

% Parameter Initialization Section
if nargin < 4 || isempty(x_ce)
    b = min(100,n_nu);
    idx = randperm(n_nu);
    x_ce = x_nu(:,idx(1:b));
end

if nargin < 5
    alpha = 0.5;
end

% construct gaussian centers
[~,n_ce] = size(x_ce);
% get sigma candidates
x = [x_de,x_nu];
[dist2_de] = comp_dist(x_de,x_ce);
%n_de * n_ce
[dist2_nu] = comp_dist(x_nu,x_ce);
%n_nu * n_ce

%compute the final result
k_de = kernel_gau(dist2_de',sigma_chosen);
k_nu = kernel_gau(dist2_nu',sigma_chosen);

H = ((1-alpha)/n_de)*k_de*k_de' + ...
    (alpha/n_nu)*k_nu*k_nu';
h = mean(k_nu,2);

theta = (H + eye(n_ce)*lambda_chosen)\h;

g_nu = theta'*k_nu;
g_de = theta'*k_de;
g_re = [];
if ~isempty(x_re)
    dist2_re = comp_dist(x_re,x_ce);
    k_re = kernel_gau(dist2_re', sigma_chosen);
    g_re = theta'*k_re;
end
rPE = mean(g_nu) - 1/2*(alpha*mean(g_nu.^2) + ...
    (1-alpha)*mean(g_de.^2)) - 1/2;

end