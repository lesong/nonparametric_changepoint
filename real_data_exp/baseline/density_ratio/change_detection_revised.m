function [SCORE] = change_detection_revised(X, n, k, alpha, sigma, lambda)

SCORE=[];

WIN = sliding_window(X, k, 1);
nSamples = size(WIN,2);
t = n +1;
SCORE=zeros(1, nSamples + 1 - 2 * n);

while(t + n -1 <= nSamples)
    
    YTest = WIN(:, t : n + t -1 );
    YRef = WIN(:, t-n: t-1);
    [SCORE(t - n), ~, ~]=RelULSIF_revised(YRef,YTest,[],[],alpha, sigma, lambda);
    
    t = t + 1;
end

end

