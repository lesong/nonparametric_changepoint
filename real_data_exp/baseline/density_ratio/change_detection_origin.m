function [SCORE, bandwidths, lambdas] = change_detection_origin(X, n, k, alpha)


WIN = sliding_window(X, k, 1);
nSamples = size(WIN,2);
t = n +1;

SCORE=zeros(1, nSamples + 1 - 2 * n);
bandwidths = zeros(1, nSamples + 1 - n - n);
lambdas = zeros(1, nSamples + 1 - n - n);

while(t + n -1 <= nSamples)
    
    YTest = WIN(:, t : n + t -1 );
    YRef = WIN(:, t-n: t-1);
    [SCORE(t - n), ~, ~, bandwidths(t - n), lambdas(t - n)]=RelULSIF_origin(YRef,YTest,[],[],alpha);
    
    t = t + 1;
    
end

end

