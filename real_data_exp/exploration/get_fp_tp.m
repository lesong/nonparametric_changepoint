function [fpr, tpr] = get_fp_tp(sequences, break_points, thresholds, offset, width)
    tp = 0.0;
    fp = 0.0;
    len = length(sequences) + 0.0;
    for i = 1 : length(sequences)
        seq = real(sequences{i});
        s1 = max(seq(offset + 1 : break_points(i) - width - 1));
        s2 = max(seq(break_points(i) - width : break_points(i) + width));
        
        if s1 > thresholds(i)
            fp = fp + 1.0;
        end
        if s2 > thresholds(i)
            tp = tp + 1.0;
        end
    end
    
    fpr = double(fp) / len;
    tpr = double(tp) / len;
end