function ccc = mfcc( x )
    addpath('~/Software/voicebox/');
    % 24 filters, 256 frames, f = 8000, lower = 0, higher = 0.5, hamming
	bank = melbankm(24, 256, 8000, 0, 0.5, 'm');																		
    %normalize
    bank = full(bank);		
    bank=bank/max(bank(:));
    
    %DCTϵ��12*24
    for k = 1 : 12
        n = 0 : 23;
        dctcoef(k,: ) = cos((2*n+1) * k * pi / (2 * 24));
    end;
    
    % normalized cesptrum window
    w = 1+6*sin(pi * [1:12] ./12);
    w = w/max(w);
    
    % pre-weighted 
    xx = double(x);
    xx = filter([1 -0.9375],1,xx);
    
    % divide into frames
    xx = enframe(xx, 256, 1);

    % compute MFCC for each frame
    for i = 1 : size(xx,1)
        y = xx(i, :);				
        s = y' .* hamming(256);	
        t = abs(fft(s));	
        t = t.^2;
        c1 = dctcoef * log(bank * t(1:129));
        c2 = c1.*w';
        m(i,:)=c2';
    end
    size(xx)
    size(m)
    % compute delta features
    dtm = zeros(size(m));
    for i = 3 : size(m, 1) - 2
        dtm(i, :) = -2 * m(i - 2, :) - m(i - 1, :) + m(i + 1, :) + 2 * m(i + 2, :);
    end
    dtm = dtm / 3;
    
    % combine two kinds of features
    ccc = [m dtm];
    ccc = ccc(3 : size(m, 1) - 2, :);
end

