function plot_example(signal, stats, threshold, changepoint, range, b_offset, filename)
    lw = 2.5;
    
    subplot(2,1,1);    
    plot(signal, 'b', 'linewidth', lw);
    hold;
    % yl = ylim;
    % ylim(yl);
    plot([changepoint, changepoint], ylim, '-.r', 'linewidth', lw);
    % plot([changepoint, changepoint], [5000, 5000], '-.r', 'linewidth', lw);
    title('Original Signal');
    xlim(range);

    subplot(2, 1, 2);
    point = find(stats(range(1) : end) > threshold);
    if length(point) > 0
        point = point(1) + range(1) - 1;
    else
        point = length(stats);
    end
    plot(stats(1 : point), 'm', 'linewidth', lw);
    hold;
    xlim(range);
    plot(xlim, [threshold, threshold], '--r', 'linewidth', lw);
    
    % plot([point, point], ylim, '-.g', 'linewidth', lw);
    plot([point, point], [-10, 5], '-.g', 'linewidth', lw);
    title('Online Statistics');
    text(range(1), threshold + b_offset, sprintf('b=%.2f', threshold));
    set(findall(gcf,'type','text'),'fontSize',18);
    set(findall(gcf,'type','axes'),'fontsize',18);
    
    set(gcf, 'PaperPositionMode', 'auto', 'PaperSize', [6, 5]);
    print('-dpdf', filename, '-r300');
end