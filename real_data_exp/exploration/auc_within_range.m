function [auc, labels, score] = auc_within_range(sequences, break_points, offset, width)
    labels = zeros(1, 2 * length(sequences));
    labels(2 : 2 : end) = 1;
    
    score = zeros(1, 2 * length(sequences));
    
    for i = 1 : length(sequences)
        seq = real(sequences{i});
        if break_points(i) < 1
            continue;
        end
        if offset + 1 > break_points(i) - width - 1
            score(i * 2 - 1) = -Inf;
        else
            score(i * 2 - 1) = max(seq(offset + 1 : break_points(i) - width - 1));
        end
        if break_points(i) - width <= 0
            left = 1;
        else
            left = break_points(i) - width;
        end
        
        if break_points(i) + width > length(seq)
            right = length(seq);
        else
            right = break_points(i) + width;
        end
        % score(i * 2) = max(seq(break_points(i) - width : break_points(i) + width));
        score(i * 2) = max(seq(left : right));
    end
    
    class1 = find(labels==1);
    class0 = find(labels==0);

    thresh = unique(sort(score));
    Nthresh = length(thresh);
    hitRate = zeros(1, Nthresh); 
    faRate = zeros(1, Nthresh);
    for thi=1:length(thresh)
        th = thresh(thi);
        % hit rate = TP/P
        hitRate(thi) = sum(score(class1) >= th) / length(class1);
        % fa rate = FP/N
        faRate(thi) = sum(score(class0) >= th) / length(class0);
    end
    auc = sum(abs(faRate(2:end) - faRate(1:end-1)) .* hitRate(2:end));
end